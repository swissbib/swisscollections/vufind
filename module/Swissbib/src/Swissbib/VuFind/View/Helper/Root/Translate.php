<?php
/**
 * Translate View Helper
 *
 * PHP version 7
 *
 * Copyright (C) project swissbib, University Library Basel, Switzerland
 * http://www.swissbib.org  / http://www.swissbib.ch / http://www.ub.unibas.ch
 *
 * Date: 9/12/13
 * Time: 11:46 AM
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category Swissbib_VuFind
 * @package  VuFind_View_Helper_Root
 * @author   Guenter Hipler <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://www.swissbib.org
 */
namespace Swissbib\VuFind\View\Helper\Root;

use Laminas\I18n\Exception\RuntimeException;
use VuFind\View\Helper\Root\Translate as VFTranslate;

/**
 * Translate view helper
 *
 * @category Swissbib_VuFind
 * @package  VuFind_View_Helper_Root
 * @author   Guenter Hipler <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://www.swissbib.org  Main Page
 */
class Translate extends VFTranslate
{
    /**
     * Translate a string
     *
     * @param string $str     String to translate
     * @param array  $tokens  Tokens to inject into the translated string
     * @param string $default Default value to use if no translation is found (null
     *                        for no default).
     *
     * @return string
     */
    public function __invoke($str, $tokens = [], $default = null)
    {
        $msg = $this->processTranslation($str, $default);

        // Do we need to perform substitutions?
        if (!empty($tokens)) {
            $in = $out = [];
            foreach ($tokens as $key => $value) {
                $in[] = $key;
                $out[] = $value;
            }
            $msg = str_replace($in, $out, $msg);
        }

        return $msg;
    }

    /**
     * Extract text-domain from label. Use text-domain "default" if none given.
     *
     * Pattern is textDomain::labelKey
     *
     * @param String $target String to detect the text-domain from
     *
     * @return array
     */
    protected function extractTextDomain($target): array
    {
        $parts = explode('::', $target);

        if (count($parts) === 2) {
            return $parts;
        }

        return ['default', $target];
    }

    /**
     * TranslateFacet
     *
     * @param String $facetName  FacetName
     * @param String $facetValue FacetValue
     *
     * @return null|string
     */
    public function translateFacet(string $facetName, string $facetValue): ?string
    {
        if (in_array($facetName, $this->translatedFacets)) {
            $fieldToTranslateInArray =  array_filter(
                $this->translatedFacets, function ($passedValue) use ($facetName) {
                    return ($passedValue === $facetName)
                    || (count(preg_grep("/" . $facetName . ":" . "/", [$passedValue])) > 0);
                }
            );

            $translate = count($fieldToTranslateInArray) > 0;
            $fieldToEvaluate = $translate ? current($fieldToTranslateInArray) : null;

            if (!str_contains($fieldToEvaluate, ':') && $translate) {
                return $this->processTranslation($facetValue);
            } elseif ($translate) {
                return $this->processTranslation(
                    $facetValue . '::' .
                    substr($fieldToEvaluate, strpos($fieldToEvaluate, ':') + 1)
                );
            }
        }
        return $facetValue;
    }

    /**
     * ProcessTranslation
     *
     * @param String      $str     String to translate
     * @param String|null $default Default value if translation fails
     *
     * @return string
     */
    protected function processTranslation(string $str, string $default = null): string
    {
        try {
            $translator = $this->getTranslator();
            if (!is_object($translator)) {
                throw new RuntimeException();
            }

            list($textDomain, $str) = $this->extractTextDomain($str);

            $msg = $translator->translate($str, $textDomain);
        } catch (RuntimeException $e) {
            // If we get called before the translator is set up, it will throw an
            // exception, but we should still try to display some text!
            $msg = $str;
        }

        // Did the translation fail to change anything?  If so, use default:
        if (null !== $default && $msg == $str) {
            $msg = $default;
        }

        return $msg;
    }
}
