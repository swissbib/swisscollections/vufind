<?php
/**
 * SolrMarcTestCase
 *
 * PHP version 7
 *
 * Copyright (C) project swissbib, University Library Basel, Switzerland
 * http://www.swissbib.org  / http://www.swissbib.ch / http://www.ub.unibas.ch
 *
 * Date: 1/2/13
 * Time: 4:09 PM
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category Swissbib_VuFind
 * @package  SwissbibTest_RecordDriver
 * @author   Guenter Hipler  <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://www.swissbib.org
 */
namespace SwissbibTest\RecordDriver;

use Laminas\Config\Config;
use PHPUnit\Framework\TestCase;
use Swissbib\RecordDriver\Helper\Availability;
use Swissbib\RecordDriver\Helper\Holdings;
use Swissbib\RecordDriver\SolrDefaultAdapter;
use Swissbib\RecordDriver\SolrMarc as SolrMarcDriver;

/**
 * SolrMarcTestCase
 *
 * @category Swissbib_VuFind
 * @package  SwissbibTest_RecordDriver
 * @author   Guenter Hipler  <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org
 */
class SolrMarcTestCase extends TestCase
{
    /**
     * SolrMarcDriver
     *
     * @var SolrMarcDriver
     */
    protected SolrMarcDriver $driver;

    /**
     * Initialize driver with fixture
     *
     * @param String $file File
     *
     * @return void
     */
    public function initialize(string $file): void
    {
//        if (!$this->driver) {
        $this->driver = new SolrMarcDriver(
            null,
            null,
            null,
            $this->createStub(Holdings::class),
            $this->createStub(SolrDefaultAdapter::class),
            $this->createStub(Availability::class),
            null
        );
        $fixture = $this->getFixtureData($file);

        $this->driver->setRawData($fixture);
//        }
    }

    /**
     * Get record fixture
     *
     * @param String $file File
     *
     * @return array
     */
    protected function getFixtureData(string $file): array
    {
        return json_decode(
            file_get_contents(realpath(SWISSBIB_TEST_FIXTURES . '/' . $file)),
            true
        );
    }
}
