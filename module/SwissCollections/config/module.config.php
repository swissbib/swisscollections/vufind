<?php

namespace SwissCollections\Module\Configuration;

use SwissCollections\AjaxHandler\GetBibliographiesData;
use SwissCollections\AjaxHandler\GetBusinessDocsData;
use SwissCollections\AjaxHandler\GetFacetData;
use SwissCollections\AjaxHandler\GetTectonicsData;
use SwissCollections\AjaxHandler\GetTopicsTectonicsData;
use SwissCollections\Controller\AlphabrowseController;
use SwissCollections\Controller\BrowseController;
use SwissCollections\Controller\HierarchyController;
use SwissCollections\Controller\SearchController;
use SwissCollections\Formatter\FieldFormatter\DocLinkLine;
use SwissCollections\Formatter\FieldFormatter\DoiLink;
use SwissCollections\Formatter\FieldFormatter\Inline;
use SwissCollections\Formatter\FieldFormatter\LabelField;
use SwissCollections\Formatter\FieldFormatter\Legal;
use SwissCollections\Formatter\FieldFormatter\Line;
use SwissCollections\Formatter\FieldFormatter\Link;
use SwissCollections\Formatter\FieldFormatter\NormalizedTime;
use SwissCollections\Formatter\FieldFormatter\PhysicalConstitution;
use SwissCollections\Formatter\FieldFormatter\PhysicalDescription;
use SwissCollections\Formatter\FieldFormatter\SearchGndLink;
use SwissCollections\Formatter\FieldFormatter\SearchLink;
use SwissCollections\Formatter\FieldFormatter\Signature852;
use SwissCollections\Formatter\FieldFormatter\Signature949;
use SwissCollections\Formatter\FieldFormatter\SourceLinks;
use SwissCollections\Formatter\FieldFormatter\Title;
use VuFind\Controller\AbstractBaseFactory;
use VuFind\Controller\AbstractBaseWithConfigFactory;

$config = [
    'router' => [
        'routes' => [
            'tektonik' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/Tektonik/[:action]',
                    'constraints' => [
                        'action'     => 'Home|Tektonik|Topics|BusinessDocs',
                    ],
                    'defaults' => [
                        'controller' => 'tektonik',
                        'action' => 'home',
                    ]
                ],
            ],
            'bibliographies' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/Bibliographien/[:action]',
                    'constraints' => [
                        'action'     => 'Home|Bibliographies|BibliographiesSpecial',
                    ],
                    'defaults' => [
                        'controller' => 'bibliographies',
                        'action' => 'home',
                    ]
                ],
            ],
            'browse-action' =>
            [
                'type' => 'Laminas\Router\Http\Segment',
                'options' =>
                    [
                        'route' => '/Browse[/:action]',
                        'constraints' => [
                            'action'     => '.*',
                        ],
                        'defaults' =>
                            [
                                'controller' => 'Search',
                                'action' => 'Home',
                            ],
                    ],
            ],
            'alphabrowse-home' =>
            [
                'type' => 'Laminas\Router\Http\Literal',
                'options' =>
                [
                    'route' => '/Alphabrowse/Home',
                    'defaults' =>
                    [
                        'controller' => 'Alphabrowse',
                        'action' => 'Home',
                    ],
                ],
            ],
            'about' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/About',
                    'defaults' => [
                        'controller' => 'about',
                        'action' => 'home',
                    ]
                ],
            ],
            'contact' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/Contact',
                    'defaults' => [
                        'controller' => 'contact',
                        'action' => 'home',
                    ]
                ],
            ],
            'impress' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/Impress',
                    'defaults' => [
                        'controller' => 'impress',
                        'action' => 'home',
                    ]
                ],
            ],
            'data-privacy' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/Privacy',
                    'defaults' => [
                        'controller' => 'data-privacy',
                        'action' => 'home',
                    ]
                ],
            ],
            'partners' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/Partners',
                    'defaults' => [
                        'controller' => 'partners',
                        'action' => 'home',
                    ]
                ],
            ],
            'site-policy' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/SitePolicy',
                    'defaults' => [
                        'controller' => 'site-policy',
                        'action' => 'home',
                    ]
                ],
            ],
            'data-export' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/DataExport',
                    'defaults' => [
                        'controller' => 'data-export',
                        'action' => 'home',
                    ]
                ],
            ],
            'data-export-info' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/data-export-info',
                    'defaults' => [
                        'controller' => 'data-export',
                        'action' => 'info',
                    ]
                ],
            ],
            'data-export/download' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/DataExport',
                    'defaults' => [
                        'controller' => 'data-export',
                        'action' => 'result',
                    ]
                ],
            ],
            'data-export/download-start-info' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/DataExport',
                    'defaults' => [
                        'controller' => 'data-export',
                        'action' => 'downloadStartInfo',
                    ]
                ],
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            'tektonik' => 'SwissCollections\Controller\Factory::getTektonikController',
            'bibliographies' => 'SwissCollections\Controller\Factory::getBibliographiesController',
            BrowseController::class => AbstractBaseWithConfigFactory::class,
            AlphabrowseController::class => AbstractBaseWithConfigFactory::class,
            HierarchyController::class => AbstractBaseWithConfigFactory::class,
            'about' => 'SwissCollections\Controller\Factory::getAboutController',
            'contact' => 'SwissCollections\Controller\Factory::getContactController',
            'impress' => 'SwissCollections\Controller\Factory::getImpressController',
            'data-privacy' => 'SwissCollections\Controller\Factory::getPrivacyController',
            'partners' => 'SwissCollections\Controller\Factory::getPartnersController',
            'site-policy' => 'SwissCollections\Controller\Factory::getSitePolicyController',
            'data-export' => 'SwissCollections\Controller\Factory::getDataExportController',
            SearchController::class => AbstractBaseFactory::class,
        ],
        'aliases' => [
            'Browse' => BrowseController::class,
            'browse' => BrowseController::class,
            'Alphabrowse' => AlphabrowseController::class,
            'alphabrowse' => AlphabrowseController::class,
            'Hierarchy' => HierarchyController::class,
            'hierarchy' => HierarchyController::class,
            \VuFind\Controller\SearchController::class => SearchController::class,
        ]
    ],
    'service_manager' => [
        'factories' => [
            'SwissCollections\VuFind\Search\Memory' => 'VuFind\Search\MemoryFactory',
            'SwissCollections\Csv\CsvReader' => 'SwissCollections\Csv\CsvReaderFactory',
            'SwissCollections\VuFind\Search\Solr\Results' => 'SwissCollections\VuFind\Search\Solr\ResultsFactory',
            'SwissCollections\ExtendedSolrFactoryHelper' =>  'SwissCollections\VuFind\Search\Helper\Factory::getExtendedSolrFactoryHelper',
            \SwissCollections\VuFind\Search\Solr\HierarchicalFacetHelper::class => \SwissCollections\VuFind\Search\Solr\HierarchicalFacetHelperFactory::class,
            'VuFind\Search\Solr\HierarchicalFacetHelper' => \SwissCollections\VuFind\Search\Solr\HierarchicalFacetHelperFactory::class,
        ],
        'aliases' => [
        'VuFind\HierarchicalFacetHelper' => \SwissCollections\VuFind\Search\Solr\HierarchicalFacetHelper::class,
        ],
    ],
    'formatters' => [
        'inline' => Inline::class,
        'link' => Link::class,
        'search-link' => SearchLink::class,
        'search-gnd-link' => SearchGndLink::class,
        'line' => Line::class,
        'doclinkline' => DocLinkLine::class,
        'doi-link' => DoiLink::class,
        'title' => Title::class,
        'legal' => Legal::class,
        'signature-949' => Signature949::class,
        'signature-852' => Signature852::class,
        'physical-constitution' => PhysicalConstitution::class,
        'physical-description' => PhysicalDescription::class,
        'source-links' => SourceLinks::class,
        'normalized-time' => NormalizedTime::class,
        'label-from-field' => LabelField::class,
    ],
    'vufind' => [
        'recorddriver_tabs' => [
            'SwissCollections\RecordDriver\SolrMarc' => [
                'tabs' => [
                    'OrderTab' => 'OrderTab',
                    'CiteTab' => 'CiteTab',
                    'Details' => 'StaffViewMARC',
                    'HierarchyTreeArchival' => 'HierarchyTreeArchival',
                    'ExportTab' => 'ExportTab',
                ],
                'defaultTab' => null,
            ],
        ],
        'plugin_managers' => [
            'ajaxhandler' => [
                'factories' => [
                    GetFacetData::class => AbstractBaseWithConfigFactory::class,
                    GetTectonicsData::class => AbstractBaseWithConfigFactory::class,
                    GetBibliographiesData::class => AbstractBaseWithConfigFactory::class,
                    GetBusinessDocsData::class => AbstractBaseWithConfigFactory::class,
                    GetTopicsTectonicsData::class => AbstractBaseWithConfigFactory::class,
                ],
                'aliases' => [
                    'getFacetData' => GetFacetData::class,
                    'getTectonicsData' => GetTectonicsData::class,
                    'getBibliographiesData' => GetBibliographiesData::class,
                    'getBusinessDocsData' => GetBusinessDocsData::class,
                    'getTopicsTectonicsData' => GetTopicsTectonicsData::class,
                ]
            ],
            'recorddriver' => [
                'factories' => [
                    'SwissCollections\RecordDriver\SolrMarc' => 'SwissCollections\RecordDriver\Factory::getSolrMarcRecordDriver',
                ],
                'aliases' => [
                    'solrmarc' => 'SwissCollections\RecordDriver\SolrMarc',
                ],
            ],
            'recordtab' => [
                'factories' => [
                    'SwissCollections\RecordTab\CiteTab' => 'SwissCollections\RecordTab\Factory::getCite',
                    'SwissCollections\RecordTab\OrderTab' => 'SwissCollections\RecordTab\Factory::getOrder',
                    'SwissCollections\RecordTab\ExportTab' => 'SwissCollections\RecordTab\Factory::getExport',
                ],
                'aliases' => [
                    'CiteTab' => 'SwissCollections\RecordTab\CiteTab',
                    'OrderTab' => 'SwissCollections\RecordTab\OrderTab',
                    'ExportTab' => 'SwissCollections\RecordTab\ExportTab',
                ],
            ],
            'recommend' => [
                'factories' => [
                    'VuFind\Recommend\SideFacets' => 'SwissCollections\Recommend\Factory::getSideFacets',
                ],
            ],
            'vufind_search_results' => [
                'factories' => [
                    \SwissCollections\VuFind\Search\Solr\Results::class => \SwissCollections\VuFind\Search\Solr\ResultsFactory::class,
                ],
                'aliases' => [
                    'solr' => \SwissCollections\VuFind\Search\Solr\Results::class,
                ],
            ],
        ],
    ],
    'swissbib' => [
        'ignore_js_assets' => [
            '|Accordion.js|',
        ],
        'plugin_managers' => [
            'vufind_search_results' => [
                'factories' => [
                    'Swissbib\VuFind\Search\Solr\Results\Solr' => 'SwissCollections\VuFind\Search\Results\Factory::getSolr',
                ],
            ],
        ]
    ]
];
return $config;
