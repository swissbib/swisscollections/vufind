<?php
/**
 * SwissCollections: ResultListViewFieldInfo.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RecordDriver;

use Laminas\Log\LoggerInterface;
use Laminas\View\Helper\AbstractHelper;
use SwissCollections\RenderConfig\AbstractFieldCondition;
use SwissCollections\RenderConfig\AbstractRenderConfigEntry;
use SwissCollections\RenderConfig\CompoundEntry;
use SwissCollections\RenderConfig\FormatterConfig;
use SwissCollections\RenderConfig\MarcFieldValueProvider;
use SwissCollections\RenderConfig\RenderGroupConfig;

/**
 * Represents all information read from "result-list-entry-fields.yaml"
 * which is used in the result list view for every search hit.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class ResultListViewFieldInfo extends AbstractHelper implements
    MarcFieldValueProvider
{
    /**
     * Raw content of "result-list-entry-fields.yaml".
     *
     * @var mixed
     */
    protected mixed $resultListViewFieldInfo;

    /**
     * The parsed info from {@link ResultListViewFieldInfo::$resultListViewFieldInfo}
     *
     * @var array array of doc type string to {@link ResultListGroupInfo}
     */
    protected array $renderInfo;

    public static string $RENDER_INFO_GROUP_FIELDS = 'fields';

    public static string $RENDER_INFO_MARC_INDEX = 'marcIndex';
    public static string $RENDER_INFO_MARC_SHOW_LABEL = 'showLabel';
    public static string $RENDER_INFO_MARC_FORMATTER = 'formatter';
    public static string $RENDER_INFO_MARC_CONDITION = 'condition';
    public static string $RENDER_INFO_MARC_CONDITION_CREATOR = 'creator';
    public static string $RENDER_INFO_MARC_VALUE_PROVIDER = 'provider';
    public static string $RENDER_INFO_MARC_FORMATTER_NAME = 'name';
    public static string $RENDER_INFO_MARC_FORMATTER_SEPARATOR = 'separator';
    public static string $RENDER_INFO_MARC_FORMATTER_REPEATED = 'repeated';
    public static string $RENDER_INFO_MARC_FORMATTER_SUBFIELDS = 'entries';
    public static string $RENDER_INFO_MARC_FORMATTER_MARC_SUBFIELD = 'marcSubfield';

    public static string $FIELD_TITEL = 'Titel';
    public static string $FIELD_PERSONS = 'Personen';
    public static string $FIELD_TIME = 'Zeiten';
    public static string $FIELD_IMPORTANT = 'Wichtig';
    public static string $FIELD_SIGNATURE = 'Signatur';

    /**
     * ViewFieldInfo constructor.
     *
     * @param mixed           $resultListViewFieldInfo the read in information
     * @param LoggerInterface $logger                  a logger instance
     */
    public function __construct(mixed $resultListViewFieldInfo, LoggerInterface $logger)
    {
        $this->resultListViewFieldInfo = $resultListViewFieldInfo;
        $this->renderInfo = $this->parse($logger);
    }

    /**
     * Get a group's configuration.
     *
     * @param string $name the document type category's name
     *
     * @return mixed | null
     */
    protected function getDocTypeInfo(string $name): mixed
    {
        return $this->getDocumentTypesInfo()[$name];
    }

    /**
     * Get the field info from a doc type info.
     * Read from {@link ViewFieldInfo::$RENDER_INFO_FIELDS}.
     *
     * @param array  $docTypeInfo data returned by getDocTypeInfo()
     * @param string $fieldName   the field's name is either {@link ResultListViewFieldInfo::FIELD_TITEL},
     *                            {@link ResultListViewFieldInfo::FIELD_PERSONS},
     *                            {@link ResultListViewFieldInfo::FIELD_TIME},
     *                            {@link ResultListViewFieldInfo::FIELD_IMPORTANT} or
     *                            {@link ResultListViewFieldInfo::FIELD_SIGNATURE}
     *
     * @return array an array of arrays with "index" etc.
     */
    protected function getMarcFieldInfo(array $docTypeInfo, string $fieldName): array
    {
        $marcInfo = [];
        if (!empty($docTypeInfo)) {
            $fieldViewInfo = $docTypeInfo[$fieldName] ?? null;
            if (!empty($fieldViewInfo)) {
                $marcInfo = $fieldViewInfo;
            }
        }
        return $marcInfo;
    }

    /**
     * Returns a field formatter's config (from
     * {@link ResultListViewFieldInfo::$RENDER_INFO_MARC_FORMATTER}).
     *
     * @param array|null $marcFieldInfo one element returned by getMarcFieldInfo()
     *
     * @return FormatterConfig
     */
    protected function getMarcFieldFormatter(array|null $marcFieldInfo): FormatterConfig
    {
        $config = null;
        if (!empty($marcFieldInfo)) {
            $config = $marcFieldInfo[self::$RENDER_INFO_MARC_FORMATTER] ?? [];
        }
        if (empty($config)) {
            $config = [];
        }
        return new FormatterConfig('inline', $config);
    }

    /**
     * Returns a field's condition (from
     * {@link ResultListViewFieldInfo::$RENDER_INFO_MARC_CONDITION}).
     *
     * @param array|null      $marcFieldInfo one element returned by getMarcFieldInfo()
     * @param LoggerInterface $logger        a logger instance
     *
     * @return AbstractFieldCondition|null
     */
    protected function getMarcFieldCondition(?array $marcFieldInfo, LoggerInterface $logger): AbstractFieldCondition|null
    {
        $condition = null;
        if (array_key_exists(self::$RENDER_INFO_MARC_CONDITION, $marcFieldInfo)) {
            $config = $marcFieldInfo[self::$RENDER_INFO_MARC_CONDITION] ?? [];
            $creator = $config[self::$RENDER_INFO_MARC_CONDITION_CREATOR] ?? null;
            $condition = call_user_func_array($creator, array(&$config, $logger));
        }
        return $condition;
    }

    /**
     * Returns a field marc's index (from
     * {@link ResultListViewFieldInfo::$$RENDER_INFO_MARC_INDEX}).
     *
     * @param array|null $marcFieldInfo one element returned by getMarcFieldInfo()
     *
     * @return int
     */
    protected function getMarcFieldIndex(?array $marcFieldInfo): int
    {
        return $marcFieldInfo[self::$RENDER_INFO_MARC_INDEX] ?? 0;
    }

    /**
     * Returns a marc field's value provider (from
     * {@link ResultListViewFieldInfo::$$RENDER_INFO_MARC_VALUE_PROVIDER}).
     *
     * @param array|null $marcFieldInfo one element returned by getMarcFieldInfo()
     *
     * @return string
     */
    protected function getValueProviderConfig(?array $marcFieldInfo): string
    {
        return $marcFieldInfo[self::$RENDER_INFO_MARC_VALUE_PROVIDER] ?? '';
    }

    /**
     * Get string representation of this instance.
     *
     * @return string
     */
    public function __toString()
    {
        return 'ResultListViewFieldInfo{' . print_r(
            $this->renderInfo,
            true
        ) . "}";
    }

    /**
     * Parse the yaml file to groups of render config entries suitable for a
     * call to {@link FieldGroupFormatterRegistry::applyFormatter}
     *
     * @param LoggerInterface $logger a logger instance
     *
     * @return array array of doc type to {@link ResultListGroupInfo}[]
     */
    protected function parse(LoggerInterface $logger): array
    {
        $docTypeRenderGroupMap = [];
        foreach ($this->getDocumentTypesInfo() as $docType => $groupEntries) {
            $renderGroups = [];
            $dtInfo = $groupEntries[self::$RENDER_INFO_GROUP_FIELDS] ?? [];
            foreach ($dtInfo as $fieldName => $fieldConfig) {
                $renderGroupConfig = new RenderGroupConfig($fieldName);
                $marcFieldsInfo = $fieldConfig;
                if (!empty($marcFieldsInfo)) {
                    foreach ($marcFieldsInfo as $pos => $marcInfo) {
                        $fieldFormatter = $this->getMarcFieldFormatter(
                            $marcInfo
                        );
                        $condition = $this->getMarcFieldCondition(
                            $marcInfo,
                            $logger
                        );
                        // echo "<!-- SFCL1: " . print_r($marcInfo, true) . " -->";
                        $marcIndex = $this->getMarcFieldIndex($marcInfo);
                        $valueProvider = $this->getValueProviderConfig(
                            $marcInfo
                        );
                        $subfieldName = $fieldName . ($pos + 1);
                        $renderConfig = new CompoundEntry(
                            $docType,
                            $fieldName,
                            $subfieldName,
                            $marcIndex,
                            $fieldFormatter,
                            $valueProvider,
                            RenderGroupConfig::$IGNORE_INDICATOR,
                            RenderGroupConfig::$IGNORE_INDICATOR,
                            $condition
                        );
                        $subfieldFormatterConfigList
                            = $fieldFormatter->getEntryOrder("simple");
                        // echo "<!-- SFCL2: " . print_r($subfieldFormatterConfigList, true) . " -->";
                        foreach ($subfieldFormatterConfigList as $sc) {
                            $renderConfig->addElement(
                                $subfieldName,
                                $sc->config[self::$RENDER_INFO_MARC_FORMATTER_MARC_SUBFIELD] ?? ''
                            );
                        }
                        $renderGroupConfig->addCompound($renderConfig);
                    }
                }
                $renderGroups[] = $renderGroupConfig;
            }
            $docTypeRenderGroupMap[$docType] = new ResultListGroupInfo(
                $renderGroups
            );
        }
        // echo "<!-- RLCONF: " . print_r($docTypeRenderGroupMap, true) . " -->";
        return $docTypeRenderGroupMap;
    }

    /**
     * Get the configuration of all document types.
     *
     * @return mixed
     */
    protected function getDocumentTypesInfo(): mixed
    {
        return $this->resultListViewFieldInfo['document-types'] ?? false;
    }

    /**
     * Get the parsed render info.
     *
     * @param string $groupName a group's name
     *
     * @return ResultListGroupInfo array of doc type string to {@link ResultListGroupInfo}
     */
    public function getRenderInfo(string $groupName): ResultListGroupInfo
    {
        return $this->renderInfo[$groupName] ?? new ResultListGroupInfo([]);
    }

    /**
     * Get a field's value provider.
     *
     * @param AbstractRenderConfigEntry $renderElem the render element
     *
     * @return null|string
     */
    public function getValueProvider(AbstractRenderConfigEntry $renderElem): ?string
    {
        if ($renderElem instanceof CompoundEntry) {
            return $renderElem->getValueProvider();
        }
        return null;
    }
}
