<?php
/**
 * SwissCollections: SolrMarc.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */


namespace SwissCollections\RecordDriver;

use Swissbib\RecordDriver\SolrMarc as SwissbibSolrMarc;
use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\Formatter\SubfieldFormatter\Simple;
use SwissCollections\RenderConfig\AbstractFieldCondition;
use SwissCollections\RenderConfig\AbstractRenderConfigEntry;
use SwissCollections\RenderConfig\FormatterConfig;
use SwissCollections\RenderConfig\IndicatorCondition;
use SwissCollections\RenderConfig\MarcFieldValueProvider;
use SwissCollections\RenderConfig\SingleEntry;

/**
 * Enhanced record driver.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class SolrMarc extends SwissbibSolrMarc
{
    /**
     * The optional archival tab.
     *
     * @var mixed
     */
    protected mixed $archivalTab;

    /**
     * Delegates to parent's method.
     *
     * @param int $index the marc index
     *
     * @return mixed
     */
    public function getMarcSubfieldsRaw(int $index): array
    {
        return parent::getMarcSubfieldsRaw($index);
    }

    /**
     * Delegates to parent's method.
     *
     * Get items of a field (which exists multiple times) as named map (array)
     * Use this method if the field is (R)epeatable
     *
     * @param Integer $index             Index
     * @param array   $fieldMap          FieldMap
     * @param Boolean $includeIndicators IncludeIndicators
     *
     * @return array
     */
    public function getMarcSubFieldMaps(
        int $index,
        array $fieldMap,
        bool $includeIndicators = true
    ): array {
        return parent::getMarcSubFieldMaps($index, $fieldMap, $includeIndicators);
    }

    /**
     * Delegates to parent's method.
     *
     * @param mixed $index the marc index
     *
     */
    public function getMarcFields(mixed $index): array
    {
        if (is_numeric($index)) {
            // parent method uses sprintf('%03d', $index) which fails for strings
            $fields = parent::getMarcFields($index);
        } else {
            $fields = $this->getMarcReader()->getFields($index);
        }

        // Marc Control field
        if (count($fields) === 1 && is_string($fields[0])) {
            $fields = [['tag' => $index, 'i1' => '', 'i2' => '',
                'subfields' => [['code' => 'a', 'data' => $fields[0]]]
            ]];
        }

        return $fields;
    }

    /**
     * Delegates to parent's method.
     *
     * @param int    $index        the marc index
     * @param string $subFieldCode the name of the subfield
     *
     * @return bool|String
     */
    public function getSimpleMarcSubFieldValue(int $index, string $subFieldCode): bool|string
    {
        return parent::getSimpleMarcSubFieldValue($index, $subFieldCode);
    }

    /**
     * Returns a map of subfield values.
     *
     * @param int|string                  $index               the marc index
     * @param AbstractFieldCondition|null $fieldCondition      field's condition
     * @param string[]                    $hiddenMarcSubfields all hidden marc subfields
     *
     * @return array array of maps of marc subfield names to values
     */
    public function getMarcFieldsRawMap(
        int|string $index,
        AbstractFieldCondition|null $fieldCondition,
        array $hiddenMarcSubfields
    ): array {
        if (is_int($index)) {
            if ($index < 100) {
                $indexStr = '0' . $index;
            } else {
                $indexStr = '' . $index;
            }
        } else {
            $indexStr = $index;
        }
        $fields = $this->getMarcReader()->getFields($indexStr);
        $fieldsData = [];

        foreach ($fields as $field) {
            $tempFieldData = $this->getMarcFieldRawMap(
                $field,
                $fieldCondition,
                $hiddenMarcSubfields
            );
            if (count($tempFieldData) > 0) {
                $fieldsData[] = $tempFieldData;
            }
        }

        return $fieldsData;
    }

    /**
     * Get the required data from the given marc field. Values are only used
     * if the element's condition is fulfilled.
     *
     * Be aware that this method may return a list of values if the marc field
     * contains several values.
     *
     * @param array       $field the marc field
     * @param SingleEntry $elem  contains the required field names
     *
     * @return null|SubfieldRenderData
     */
    public function getRenderFieldData(array $field, SingleEntry $elem): SubfieldRenderData|null
    {
        try {
            if ($elem->checkCondition($field, $this)) {
                $ind1 = IndicatorCondition::parse(
                    $field['i1'] ?? null,
                    IndicatorCondition::$EMPTY_INDICATOR
                );
                $ind2 = IndicatorCondition::parse(
                    $field['i2'] ?? null,
                    IndicatorCondition::$EMPTY_INDICATOR
                );
                $fieldMap = $elem->buildMap();
                $fieldData = $this->getMappedFieldData(
                    $field,
                    $fieldMap
                );
                $subfieldRenderData = new SubfieldRenderData(
                    $fieldData['value'] ?? null,
                    true,
                    $ind1,
                    $ind2
                );

                // Marc Control field
                if ($subfieldRenderData->emptyValue()
                    && count($field) === 1
                    && is_string($field[0])) {
                    $subfieldRenderData = new SubfieldRenderData(
                        $field[0],
                        true,
                        $ind1,
                        $ind2
                    );
                }

                $this->logger->debug(
                    'GRFD: ' . $elem->marcIndex . ' ' . $elem->getMarcSubfieldName() . ' '
                    . print_r($subfieldRenderData, true)
                );
                if (!$subfieldRenderData->emptyValue()) {
                    return $subfieldRenderData;
                }
            }
            return null;
        } catch (\Throwable $exception) {
            $this->logger->err(
                'Caught Exception ' . $exception->getMessage() . "\n"
                . $exception->getTraceAsString()
            );
        }
        return null;
    }

    /**
     * Factory method to build a new {@link SubfieldRenderData} instance
     * without indicator limitations.
     *
     * @param string $value      a subfield's value
     * @param bool   $escapeHtml if html escaping is required (if false the value is already html escaped)
     *
     * @return SubfieldRenderData
     */
    public function buildGenericSubMap(
        string $value,
        bool $escapeHtml
    ): SubfieldRenderData {
        return new SubfieldRenderData(
            $value,
            $escapeHtml,
            IndicatorCondition::$UNKNOWN_INDICATOR,
            IndicatorCondition::$UNKNOWN_INDICATOR
        );
    }

    /**
     * Checks if the given "value" is empty.
     *
     * @param SubfieldRenderData $subfieldRenderData the value to check
     *
     * @return bool
     */
    protected function isEmptyValue(
        SubfieldRenderData $subfieldRenderData
    ): bool {
        return $subfieldRenderData->emptyValue();
    }

    /**
     * Returns a map of subfield names to their values if the condition is
     * fulfilled.
     *
     * @param array                       $field               the marc field
     * @param AbstractFieldCondition|null $fieldCondition      the field's conditions
     * @param string[]                    $hiddenMarcSubfields hidden marc subfields
     *
     * @return array array of subfield names to array of values
     */
    public function getMarcFieldRawMap(
        array $field,
        AbstractFieldCondition|null $fieldCondition,
        array $hiddenMarcSubfields
    ): array {
        $tempFieldData = [];

        if ($fieldCondition === null
            || $fieldCondition->assertTrue($field, $this)
        ) {
            $subfields = $field['subfields'] ?? [];
            foreach ($subfields as $marcSubfield) {
                if (!in_array($marcSubfield['code'] ?? '', $hiddenMarcSubfields, true)) {
                    $data = trim($marcSubfield['data'] ?? '');
                    $code = '' . ($marcSubfield['code'] ?? '');
                    if (array_key_exists($code, $tempFieldData)) {
                        $tempFieldData[$code][] = $data;
                    } else {
                        $tempFieldData[$code] = [$data];
                    }
                }
            }
            // Marc Control field
            if (empty($subfields) && count($field) === 1 && is_string($field[0])) {
                $tempFieldData['a'] = $field;
            }
        }
        return $tempFieldData;
    }

    /**
     * Returns a list of subfield names to their values if the condition is
     * fulfilled.
     *
     * @param array                       $field               the marc field
     * @param AbstractFieldCondition|null $fieldCondition      the field's conditions
     * @param string[]                    $hiddenMarcSubfields hidden marc subfields
     *
     * @return array array of subfield names to array of values
     */
    public function getMarcFieldRawSequence(
        array $field,
        AbstractFieldCondition|null $fieldCondition,
        array $hiddenMarcSubfields
    ): array {
        $tempFieldData = [];

        if ($fieldCondition === null
            || $fieldCondition->assertTrue($field, $this)
        ) {
            $subfields = $field['subfields'] ?? [];
            foreach ($subfields as $marcSubfield) {
                if (!in_array(
                    $marcSubfield['code'] ?? '',
                    $hiddenMarcSubfields,
                    true
                )
                ) {
                    $v = trim($marcSubfield['data'] ?? '');
                    $k = '' . ($marcSubfield['code'] ?? '');
                    $tempFieldData[] = [$k, $v];
                }
            }
            if (empty($subfields)) {
                $tempFieldData['a'] = $field;
            }
        }
        return $tempFieldData;
    }

    /**
     * Get all types of the document.
     *
     * @return string[]
     */
    public function getDocumentTypes(): array
    {
        return $this->fields['format_str_mv'] ?? [];
    }

    public function getDocumentIconType(): string
    {
        $types = $this->fields['icon_code_str_mv'] ?? $this->fields['format_parent_str_mv'];
        switch ($types) {
            case in_array('textcsv', $types):
                return 'CSV';
            case in_array('bild', $types):
                return 'IMAGE';
            case in_array('film', $types):
                return 'VIDEO';
            case in_array('textpdf', $types):
                return 'PDF';
            default:
                return isset($types[0]) ? $types[0] : '';
        }
    }

    public function getDefaultDocType(): string
    {
        return 'Handschriften';
    }

    /**
     * Get the document type category of the current document.
     *
     * @return string[] | null
     */
    public function getTopDocumentType(): array|null
    {
        return $this->fields['format_parent_str_mv'] ?? [];
    }

    /**
     * Get a solr field's value.
     *
     * @param string $name the solr field's name
     *
     * @return array
     */
    public function getSolrFieldValue(string $name): array
    {
        $v = $this->fields[$name] ?? null;
        if ($v === null) {
            return [];
        }
        if (is_array($v)) {
            return $v;
        }
        return [$v];
    }

    /**
     * Abstract method to get field values for marc fields and other value
     * providers.
     *
     * @param AbstractRenderConfigEntry $renderElem    the render element
     * @param MarcFieldValueProvider    $valueProvider contains the provider information
     *
     * @return array
     */
    public function getFieldValues(AbstractRenderConfigEntry $renderElem, MarcFieldValueProvider $valueProvider): array
    {
        $valueProvider = $valueProvider->getValueProvider($renderElem);
        if (empty($valueProvider)) {
            $fields = $this->getMarcFields($renderElem->marcIndex);
        } else {
            $fields = call_user_func_array(
                $valueProvider,
                array(&$renderElem, &$this)
            );
        }
        return $fields;
    }

    /**
     * Get the document type of a given render element. Fakes a marc field
     * with a marc subfield "a".
     *
     * @param AbstractRenderConfigEntry $renderElem the render element
     * @param SolrMarc                  $solrMarc   the render context
     *
     * @return array
     */
    public static function documentTypeProvider(
        AbstractRenderConfigEntry $renderElem,
        self $solrMarc
    ): array {
        $docTypes = [];
        $types = $solrMarc->getDocumentTypes();
        if (!empty($types)) {
            foreach ($types as $t) {
                $docTypes[] = $t;
            }
        }
        return [self::fakeMarcfield('a', $docTypes)];
    }

    /**
     * Dummy marc value provider to avoid empty INTERNAL fields.
     *
     * @param AbstractRenderConfigEntry $renderElem the render element
     * @param SolrMarc                  $solrMarc   the render context
     *
     * @return array
     */
    public static function dummyProvider(AbstractRenderConfigEntry $renderElem, self $solrMarc): array
    {
        return self::fakeMarcfield('a', ['XYZ42']);
    }

    /**
     * Returns dummy value if the archival tab exists.
     *
     * @param AbstractRenderConfigEntry $renderElem the render element
     * @param SolrMarc                  $solrMarc   the render context
     *
     * @return array
     */
    public static function hasArchivalTab(AbstractRenderConfigEntry $renderElem, self $solrMarc): array
    {
        $result = [];
        if (!empty($solrMarc->archivalTab)) {
            $result[] = self::fakeMarcfield('a', ['XYZ42']);
        }
        return $result;
    }

    /**
     * Fake a marc field with the given subfield name and values.
     *
     * @param string   $marcSubfield the marc subfield's name
     * @param string[] $values       the marc subfield's values
     *
     * @return array
     */
    public static function fakeMarcfield(string $marcSubfield, array $values): array
    {
        $subfieldValues = [];
        foreach ($values as $v) {
            $subfieldValues[] = ['code' => $marcSubfield, 'data' => $v];
        }

        return ['tag' => '000', 'i1' => '', 'i2' => '', 'subfields' => $subfieldValues];
    }

    /**
     * Get a document's human readable publication date (based on swissbib's
     * implementation).
     * Marc index: 008
     *
     * @param AbstractRenderConfigEntry $renderElem the render element
     * @param SolrMarc                  $solrMarc   the render context
     *
     * @return array
     */
    public static function humanReadablePublicationDatesProvider(
        AbstractRenderConfigEntry $renderElem,
        self $solrMarc
    ): array {
        $dates = $solrMarc->getHumanReadablePublicationDates();
        return [self::fakeMarcfield('a', $dates)];
    }

    /**
     * Get a document's primary authors (based on swissbib's implementation).
     * Marc index: 100
     *
     * @param AbstractRenderConfigEntry $renderElem the render element
     * @param SolrMarc                  $solrMarc   the render context
     *
     * @return array
     */
    public static function primaryAuthorsProvider(
        AbstractRenderConfigEntry $renderElem,
        self $solrMarc
    ): array {
        $authors = $solrMarc->getPrimaryAuthors();
        return [self::fakeMarcfield('a', $authors)];
    }

    /**
     * Get a document's languages.
     * Either Marc index 546 or solr multi value solr field 'language'.
     *
     * @param AbstractRenderConfigEntry $renderElem Not used, but required for call from {@see self::getFieldValues()}
     * @param SolrMarc                  $solrMarc the render context
     *
     * @return array
     */
    public static function languagesProvider(
        AbstractRenderConfigEntry $renderElem,
        self $solrMarc
    ): array {
        $languages = [];

        $languages546 = $solrMarc->getLangData();
        if (!empty($languages546)) {
            $languages = $languages546;
        } else {
            $languagesSolr = $solrMarc->getLanguages();
            if (!empty($languagesSolr)) {
                foreach ($languagesSolr as $l) {
                    $languages[] = $solrMarc->getTranslator()->translate(
                        $l,
                        'languagecodes'
                    );
                }
            }
        }

        return [self::fakeMarcfield('a', $languages)];
    }

    /**
     * Get primary author
     *
     * @param Boolean $asString AsString
     *
     * @return array|String
     */
    public function getPrimaryAuthor(bool $asString = true): string|array
    {
        $s = parent::getPrimaryAuthor($asString);
        if ($asString) {
            $s = Simple::stripQuoting(Simple::stripHtmlQuoting($s));
        }
        return $s;
    }

    /**
     * GetSubtitle
     *
     * @return string
     */
    public function getSubtitle(): string
    {
        return Simple::stripQuoting(
            Simple::stripHtmlQuoting(parent::getSubtitle())
        );
    }

    /**
     * Get short title.
     *
     * @return string
     */
    public function getShortTitle(): string
    {
        return Simple::stripQuoting(
            Simple::stripHtmlQuoting(parent::getShortTitle())
        );
    }

    /**
     * Get thumbnail link from e-rara-DOI (see wiki documentation)
     * 128 = small
     * 304 = medium
     * 504 = large
     *
     * @param string $size small, medium, large
     *
     * @return bool|string
     */
    protected function getThumbnailEraraBySize(string $size): bool|string
    {
        $dois = $this->getDOIs();
        if (!empty($dois)) {
            return $this->thumbnailUrlFromDoi($dois['0'], $size);
        }

        return false;
    }

    /**
     * Get thumbnail link from e-rara-DOI (see wiki documentation)
     * 128 = small
     * 304 = medium
     * 504 = large
     *
     * @param string $doi  the e-rara url
     * @param string $size small, medium, large
     *
     * @return bool|string
     */
    protected function thumbnailUrlFromDoi(string $doi, string $size): bool|string
    {
        $width = 128;
        if ($size === 'medium') {
            $width = 304;
        } elseif ($size === 'large') {
            $width = 504;
        }

        if (!empty($doi) && preg_match('/^.*e-rara/', $doi)) {
            return 'https://www.e-rara.ch/titlepage/doi/' . $doi . '/' . $width;
        }

        return false;
    }

    /**
     * Get thumbnail link from e-rara-DOI (see wiki documentation)
     * 128 = small
     * 304 = medium
     * 504 = large
     *
     * @param string $size small, medium, large
     *
     * @return bool|string
     */
    protected function getThumbnailErara856BySize(string $size): bool|string
    {
        $urls = $this->getMarcReader()->getFields('856');
        if ($urls) {
            foreach ($urls as $url) {
                $link = $this->getSubfield($url, 'u');
                if (!empty($link)
                    && (preg_match('@^[^/]+://doi.org/(10.3931.+)$@', $link, $matches) === 1)
                    && $path = $this->thumbnailUrlFromDoi($matches[1], $size)
                ) {
                    return $path;
                }
                if (!empty($link)
                    && (preg_match('@^[^/]+://e-codices.ch/loris/.+$@', $link, $matches) === 1)
                ) {
                    // complete url with size: .../full/,150/0/default/jpg
                    return $this->buildImageUrlBySize($link, $size);
                }

                if ($this->getSubfield($url, '3') === 'thumbnail iiif') {
                    return $this->buildImageUrlBySize($link, $size);
                }

                if ($this->getSubfield($url, '3') === 'thumbnail') {
                    return $link;
                }
            }
        }
        return false;
    }

    /**
     * Get thumbnail link from e-manuscripta-DOI (see wiki documentation).
     * 128 = small
     * 304 = medium
     * 800 = large
     *
     * @param string $size small, medium, large
     *
     * @return bool|string
     */
    protected function getThumbnailEmanuscriptaBySize(string $size): bool|string
    {
        $width = 128;
        if ($size === 'medium') {
            $width = 304;
        } elseif ($size === 'large') {
            $width = 504;
        }

        $field = $this->getDOIs();
        if (!empty($field) && preg_match('/^.*e-manuscripta/', $field['0'])) {
            $URL_thumb = 'https://www.e-manuscripta.ch/titlepage/doi/'
                . $field['0']
                . '/' . $width;
            return $URL_thumb;
        }
        return false;
    }

    /**
     * Get thumbnail link from 856 u and convert it to IIIF url.
     * 128 = small
     * 420 = medium
     * 800 = large
     *
     * @param string $size small, medium, large
     *
     * @return bool|string
     */
    protected function getThumbnailPortraitUbBaselBySize(string $size): bool|string
    {
        $urls = $this->getMarcReader()->getFields('856');
        if ($urls) {
            foreach ($urls as $url) {
                $address = $this->getSubfield($url, 'u');
                if ($address
                    && preg_match('/.*digi\/a100\/portraet\//', $address) === 1
                    && preg_match('@^.*/([^./]+)\.[^/]+$@', $address, $groups) === 1
                ) {
                    $thumbId = $groups[1];
                    $sizeInPx = 128;
                    if ($size === 'medium') {
                        $sizeInPx = 420;
                    } elseif ($size === 'large') {
                        $sizeInPx = 800;
                    }
                    return $this->buildImageUrlBySize("https://ub-sipi.ub.unibas.ch/portraets/{$thumbId}.jpx", $size);
                }
            }
        }
        return false;
    }

    /**
     * @param string $prefixUrl first part of image url without size part
     * @param string $size small, medium, large
     *
     * @return string complete url
     */
    protected function buildImageUrlBySize(string $prefixUrl, string $size): string
    {
        $sizeInPx = 128;
        if ($size === 'medium') {
            $sizeInPx = 420;
        } elseif ($size === 'large') {
            $sizeInPx = 800;
        }
        return $prefixUrl . "/full/{$sizeInPx},/0/default.jpg";
    }

    /**
     * Get the url from the AVD marc field.
     *
     * @param string $size small, medium, large
     *
     * @return bool|string
     */
    protected function getThumbnailAvdBySize(string $size): bool|string
    {
        $smallUrl = null;
        $avdValues = $this->getMarcFieldsRawMap('AVD', null, []);
        if (!empty($avdValues)) {
            foreach ($avdValues as $value) {
                $d = $value['d'];
                $u = $value['u'];
                $s = $value['s'];
                if (!$this->isImage($s)) {
                    continue;
                }
                if (($size === 'small') && !empty($d)) {
                    return $d[0];
                }
                // else medium, large
                if (!empty($u)) {
                    return $u[0];
                }

                if (!empty($d)) {
                    $smallUrl = $d[0];
                }
            }
            // better than nothing for medium/large
            if (!empty($smallUrl)) {
                return $smallUrl;
            }
        }
        return false;
    }

    /**
     * Get the IZ ID from Basel IZ
     *
     * @return bool|string
     */
    public function getBaselIzID(): bool|string
    {
        $originValues = $this->getMarcFieldsRawMap('035', null, []);
        if (!empty($originValues)) {
            foreach ($originValues as $value) {
                $a = $value['a'] ?? null;
                if (!empty($a) && stripos($a[0], '(41SLSP_UBS)') === 0) {
                    return substr($a[0], 12);
                }
            }
        }
        return false;
    }

    /**
     * Get the IZ ID from Bern IZ
     *
     * @return bool|string
     */
    public function getBernIzID(): bool|string
    {
        $originValues = $this->getMarcFieldsRawMap('035', null, []);
        if (!empty($originValues)) {
            foreach ($originValues as $value) {
                $a = $value['a'] ?? null;
                if (!empty($a) && stripos($a[0], '(41SLSP_UBE)') === 0) {
                    return substr($a[0], 12);
                }
            }
        }
        return false;
    }

    /**
     * Get the IZ ID from Basel IZ
     *
     * @return bool|string
     */
    public function getZBcollectionsID()
    {
        try {
            $originValues = $this->getMarcFieldsRawMap('035', null, []);

            if (!empty($originValues)) {
                foreach ($originValues as $value) {
                    if (isset($value['a'])) {
                        $a = $value['a'];

                        if (!empty($a)) {
                            if (stripos($a[0], "(ZBcollections)") === 0) {
                                return substr($a[0], 15);
                            }
                        }
                    }
                }
            }
        } catch (\File_MARC_Exception $exception) {
            // NOP
        }

        return false;
    }


    /**
     * Describes the marc field value an image?
     *
     * @param string[] $texts a text containing a value from marc field AVD $s.
     *
     * @return bool
     */
    protected function isImage(array $texts): bool
    {
        if (!empty($texts)) {
            $images = [
                'jpeg', 'jpg', 'gif', 'png', 'tiff', 'tif', 'svg', 'bmp',
                'webp', 'avif'
            ];
            foreach ($texts as $text) {
                foreach ($images as $img) {
                    if (is_int(stripos($text, $img))) {
                        return true;
                    }
                }
            }
            return false;
        }
        // nothing known, give it a try
        return true;
    }

    /**
     * Returns one of two things:
     * a full URL to a thumbnail preview of the record
     * if an image is available in an external system; an array of parameters to
     * send to VuFind's internal cover generator if no fixed URL exists
     *
     * Extended from SolrDefault
     *
     * See documentation to swissbib-specific functions:
     * http://www.swissbib.org/wiki/index.php?title=Staff:Thumbnail_locations
     *
     * @param string $size Size of thumbnail (small, medium or large -- small is
     *                     default).
     *
     * @return bool|array|string
     */
    public function getThumbnail($size = 'small'): bool|array|string
    {
        $useMostSpecificState = $this->getUseMostSpecificFormat();
        $format = $this->getMostSpecificFormat();
        $this->setUseMostSpecificFormat($useMostSpecificState);

        if ($path = $this->getThumbnailEraraBySize($size)) {
            return $path;
        }
        if ($path = $this->getThumbnailEmanuscriptaBySize($size)) {
            return $path;
        }
        if ($path = $this->getThumbnailAvdBySize($size)) {
            return $path;
        }
        if ($path = $this->getThumbnailPortraitUbBaselBySize($size)) {
            return $path;
        }
        if ($path = $this->getThumbnailErara856BySize($size)) {
            return $path;
        }
        if (isset($format) && is_array($format) && !empty($format)) {
            return [
                'size' => $size, 'format' => $format[0],
                'contenttype' => $format[0]
            ];
        }

        return [];
    }

    /**
     * Get the archival tab object.
     *
     * @return mixed|null
     */
    public function getArchivalTab(): mixed
    {
        return $this->archivalTab;
    }

    /**
     * Set the archival tab object.
     *
     * @param mixed $archivalTab
     */
    public function setArchivalTab(mixed $archivalTab): void
    {
        $this->archivalTab = $archivalTab;
    }

    /**
     * Generic search text provider for "Sucheinstiege". Excludes synthetic
     * subfield {@link AbstractRenderConfigEntry::DECORATOR_MARC_SUBFIELD_HTML}.
     * Return a searchable consisting of all field values.
     *
     * @param FieldFormatterData[] $fieldDataList   the field values
     * @param FormatterConfig      $formatterConfig the formatter's config
     * @param FieldRenderContext   $context         the context
     *
     * @return string
     */
    public static function allSubfieldsSearchText(
        array $fieldDataList,
        FormatterConfig $formatterConfig,
        FieldRenderContext $context
    ): string {
        $text = '';
        foreach ($fieldDataList as $fieldData) {
            if ($fieldData->renderConfig->getMarcSubfieldName()
                !== AbstractRenderConfigEntry::DECORATOR_MARC_SUBFIELD_HTML
            ) {
                $text .= ' ' . $fieldData->subfieldRenderData->value;
            }
        }
        return trim($text);
    }
}
