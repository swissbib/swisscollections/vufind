<?php
/**
 * SwissCollections: ViewFieldInfo.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RecordDriver;

use Laminas\Log\LoggerInterface;
use SwissCollections\RenderConfig\AbstractFieldCondition;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Represents all information read from "detail-view-field-structure.yaml"
 * which is used in the detail view.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class ViewFieldInfo
{
    protected mixed $detailViewFieldInfo;

    public static string $RENDER_INFO_FIELDS = "fields";
    public static string $RENDER_INFO_GROUPS = "groups";

    public static string $RENDER_INFO_FIELD_TYPE = "type";
    public static string $RENDER_INFO_FIELD_MODE = "formatter";
    public static string $RENDER_INFO_FIELD_SUBFIELD_SEQUENCES = "sequences";
    public static string $RENDER_INFO_FIELD_VALUE_PROVIDER = "provider";
    public static string $RENDER_INFO_FIELD_CONDITION = "condition";
    public static string $RENDER_INFO_FIELD_CONDITION_CREATOR = "creator";

    /**
     * ViewFieldInfo constructor.
     *
     * @param mixed $detailViewFieldInfo the read in information
     */
    public function __construct(mixed $detailViewFieldInfo)
    {
        $this->detailViewFieldInfo = $detailViewFieldInfo;
    }

    /**
     * Checks if a {@link ViewFieldInfo::$RENDER_INFO_FIELD_TYPE} is specified.
     *
     * @param array $fieldViewInfo data returned by getField()
     *
     * @return bool
     */
    public function hasType(array $fieldViewInfo): bool
    {
        return array_key_exists(self::$RENDER_INFO_FIELD_TYPE, $fieldViewInfo);
    }

    /**
     * Returns the value of {@link ViewFieldInfo::$RENDER_INFO_FIELD_TYPE}.
     *
     * @param array $fieldViewInfo data returned by getField()
     *
     * @return String|null either single | compound | sequences
     */
    public function getType(array $fieldViewInfo): ?string
    {
        return $fieldViewInfo[self::$RENDER_INFO_FIELD_TYPE] ?? null;
    }

    /**
     * Get a group's configuration.
     *
     * @param string $name the group's name
     *
     * @return mixed | null
     */
    public function getGroup(string $name): mixed
    {
        return $this->detailViewFieldInfo['structure'][$name] ?? null;
    }

    /**
     * Field info with marc index postfix is preferred to info without.
     * Read from {@link ViewFieldInfo::$RENDER_INFO_FIELDS}.
     *
     * @param array      $groupViewInfo data returned by getGroup()
     * @param string     $name          the field's name
     * @param string    $marcIndex     optional marc index
     *
     * @return mixed | null
     */
    public function getField(array $groupViewInfo, string $name, string $marcIndex = ''): mixed
    {
        $fields = $groupViewInfo[self::$RENDER_INFO_FIELDS] ?? [];
        return $fields[$name . '-' . $marcIndex] ?? ($fields[$name] ?? '');
    }

    /**
     * Returns a formatter's config (from
     * {@link ViewFieldInfo::$RENDER_INFO_FIELD_MODE}).
     *
     * @param array|null $groupViewInfo data returned by getGroup()
     * @param string     $fieldName     the field's name
     *
     * @return FormatterConfig
     */
    public function getFieldGroupFormatter(?array $groupViewInfo, string $fieldName): FormatterConfig
    {
        $cfg = $this->groupInfo($groupViewInfo, $fieldName) ?? [];
        $config = $cfg[self::$RENDER_INFO_FIELD_MODE] ?? [];
        return new FormatterConfig('default', $config);
    }

    /**
     * Returns a group's view config. Read from
     * {@link ViewFieldInfo::$RENDER_INFO_GROUPS}.
     *
     * @param array|null $groupViewInfo data returned by getGroup()
     * @param string     $fieldName     the field's name
     *
     * @return mixed | null
     */
    protected function groupInfo(?array $groupViewInfo, string $fieldName): mixed
    {
        $config = [];
        if ($groupViewInfo) {
            $fieldGroups = $groupViewInfo[self::$RENDER_INFO_GROUPS] ?? null;
            if ($fieldGroups) {
                return $fieldGroups[$fieldName] ?? [];
            }
        }
        return $config;
    }

    /**
     * Belongs a field to group of fields (with different conditions, marc
     * indexes)?
     * Checks {@link ViewFieldInfo::$RENDER_INFO_GROUPS}.
     *
     * @param string $groupName the group's name
     * @param string $fieldName the field's name
     *
     * @return bool
     */
    public function isMultiMarcField(string $groupName, string $fieldName): bool
    {
        $groupViewInfo = $this->getGroup($groupName);
        if ($groupViewInfo) {
            $fieldGroups = $groupViewInfo[self::$RENDER_INFO_GROUPS] ?? [];
            return key_exists($fieldName, $fieldGroups);
        }
        return false;
    }

    /**
     * Get a formatter's config. Read from
     * {@link ViewFieldInfo::$RENDER_INFO_FIELD_MODE}.
     *
     * @param string $defaultFormatterName the default formatter to use
     * @param array  $fieldViewInfo        data returned by getField()
     *
     * @return FormatterConfig
     */
    public function getFormatterConfig(string $defaultFormatterName, array $fieldViewInfo
    ): FormatterConfig {
        $config = $fieldViewInfo[self::$RENDER_INFO_FIELD_MODE] ?? [];
        return new FormatterConfig($defaultFormatterName, $config);
    }

    /**
     * Get a formatter's condition. Read from
     * {@link ViewFieldInfo::$RENDER_INFO_FIELD_CONDITION}.
     *
     * @param array           $fieldViewInfo data returned by getField()
     * @param LoggerInterface $logger        a logger instance
     *
     * @return AbstractFieldCondition|null
     */
    public function getCondition(array $fieldViewInfo, LoggerInterface $logger): ?AbstractFieldCondition
    {
        $c = null;
        if (array_key_exists(self::$RENDER_INFO_FIELD_CONDITION, $fieldViewInfo))
        {
            $config = $fieldViewInfo[self::$RENDER_INFO_FIELD_CONDITION];
            $creator = $config[self::$RENDER_INFO_FIELD_CONDITION_CREATOR];
            $c = call_user_func_array($creator, array(&$config, $logger));
        }
        return $c;
    }

    /**
     * Get the value provider. Read from
     * {@link ViewFieldInfo::$RENDER_INFO_FIELD_VALUE_PROVIDER}.
     *
     * @param array $fieldViewInfo data returned by getField()
     *
     * @return string
     */
    public function getValueProvider(array $fieldViewInfo): string
    {
        return $fieldViewInfo[self::$RENDER_INFO_FIELD_VALUE_PROVIDER] ?? "";
    }

    /**
     * Especially for sequence view configs.
     * Read from {@link ViewFieldInfo::$RENDER_INFO_FIELD_SUBFIELD_SEQUENCES}.
     *
     * @param array $fieldViewInfo data returned by getField()
     *
     * @return string[][]
     */
    public function getSubfieldSequences(array $fieldViewInfo): array
    {
        return $fieldViewInfo[self::$RENDER_INFO_FIELD_SUBFIELD_SEQUENCES] ?? [[]];
    }

    /**
     * Get all configured group names.
     *
     * @return string[]
     */
    public function groupNames(): array
    {
        return array_keys($this->detailViewFieldInfo['structure']);
    }

    /**
     * Gets field names for a given group.
     * Removes optional marc index postfix from field names infos and
     * prevents duplicates (first occurrence is used).
     * Reads from {@link ViewFieldInfo::$RENDER_INFO_FIELDS}.
     *
     * @param string $groupName the group's name
     *
     * @return string[]
     */
    public function fieldNames(string $groupName): array
    {
        $fieldNames = [];
        $groupViewInfo = $this->getGroup($groupName);
        if ($groupViewInfo) {
            $fieldNameCandidates = array_keys(
                $groupViewInfo[self::$RENDER_INFO_FIELDS]
            );
            if ($fieldNameCandidates) {
                foreach ($fieldNameCandidates as $s) {
                    if (preg_match("/(.+)-[0-9]+$/", $s, $matches) === 1) {
                        $fn = $matches[1];
                    } else {
                        $fn = $s;
                    }
                    if (!array_search($fn, $fieldNames)) {
                        $fieldNames[] = $s;
                    }
                }
            }
        }
        return $fieldNames;
    }
}