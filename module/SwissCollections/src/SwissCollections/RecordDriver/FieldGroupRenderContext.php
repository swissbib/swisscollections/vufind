<?php
/**
 * SwissCollections: FieldGroupRenderContext.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RecordDriver;

use Laminas\Log\LoggerInterface;
use Laminas\View\Renderer\PhpRenderer;
use SwissCollections\Formatter\FieldFormatterRegistry;
use SwissCollections\Formatter\SubfieldFormatterRegistry;
use SwissCollections\RenderConfig\FormatterConfig;
use SwissCollections\RenderConfig\MarcFieldValueProvider;
use SwissCollections\RenderConfig\RenderConfig;

/**
 * Render context for field groups.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class FieldGroupRenderContext
{
    /**
     * The prefix to use for translations.
     *
     * @var string
     */
    public string $i18nKeyPrefix;

    /**
     * "vufind"'s renderer.
     *
     * @var PhpRenderer
     */
    public PhpRenderer $phpRenderer;

    /**
     * The registry of all field formatters.
     *
     * @var FieldFormatterRegistry
     */
    public FieldFormatterRegistry $fieldFormatterRegistry;

    /**
     * The registry of all subfield formatters.
     *
     * @var SubfieldFormatterRegistry
     */
    public SubfieldFormatterRegistry $subfieldFormatterRegistry;

    /**
     * The marc record.
     *
     * @var SolrMarc
     */
    public SolrMarc $solrMarc;

    /**
     * The formatter configuration of the group.
     *
     * @var FormatterConfig|null
     */
    public FormatterConfig|null $formatterConfig;

    /**
     * The marc field data provider.
     *
     * @var MarcFieldValueProvider
     */
    public MarcFieldValueProvider $marcFieldValueProvider;

    /**
     * A logger instance.
     *
     * @var LoggerInterface
     */
    public LoggerInterface $logger;

    /**
     * @var array
     */
    private array $processedSubMaps;

    /**
     * FieldRenderContext constructor.
     *
     * @param FieldFormatterRegistry    $fieldFormatterRegistry    the field registry
     * @param SubfieldFormatterRegistry $subfieldFormatterRegistry the subfield registry
     * @param SolrMarc                  $solrMarc                  the marc record
     * @param PhpRenderer               $phpRenderer               vufind's renderer
     * @param string                    $i18nKeyPrefix             the key prefix for translations
     * @param MarcFieldValueProvider    $marcFieldValueProvider    the marc field data provider
     */
    public function __construct(
        FieldFormatterRegistry $fieldFormatterRegistry,
        SubfieldFormatterRegistry $subfieldFormatterRegistry,
        SolrMarc $solrMarc,
        PhpRenderer $phpRenderer,
        string $i18nKeyPrefix,
        MarcFieldValueProvider $marcFieldValueProvider,
        LoggerInterface $logger
    ) {
        $this->solrMarc = $solrMarc;
        $this->fieldFormatterRegistry = $fieldFormatterRegistry;
        $this->subfieldFormatterRegistry = $subfieldFormatterRegistry;
        $this->processedSubMaps = [];
        $this->phpRenderer = $phpRenderer;
        $this->i18nKeyPrefix = $i18nKeyPrefix;
        $this->marcFieldValueProvider = $marcFieldValueProvider;
        $this->logger = $logger;
    }

    /**
     * Forget already rendered values.
     *
     * @return void
     */
    public function clearProcessed(): void
    {
        $this->processedSubMaps = [];
    }
}
