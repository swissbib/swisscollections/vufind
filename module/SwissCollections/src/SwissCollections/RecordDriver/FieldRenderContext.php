<?php
/**
 * SwissCollections: FieldRenderContext.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RecordDriver;

use Laminas\Log\LoggerInterface;
use Laminas\View\Renderer\PhpRenderer;
use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\Formatter\FieldFormatterRegistry;
use SwissCollections\Formatter\SubfieldFormatterRegistry;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Context for field formatters.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordDriver
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class FieldRenderContext
{
    /**
     * "vufind"'s renderer.
     *
     * @var PhpRenderer
     */
    public PhpRenderer $phpRenderer;

    // any already processed values
    public array $processedSubMaps;

    /**
     * The registry of all field formatters.
     *
     * @var FieldFormatterRegistry
     */
    protected FieldFormatterRegistry $fieldFormatterRegistry;

    /**
     * The registry of all subfield formatters.
     *
     * @var SubfieldFormatterRegistry
     */
    protected SubfieldFormatterRegistry $subfieldFormatterRegistry;

    /**
     * The marc record.
     *
     * @var SolrMarc
     */
    public SolrMarc $solrMarc;

    /**
     * The prefix to use for translations.
     *
     * @var string
     */
    public string $i18nKeyPrefix;

    /**
     * A logger instance.
     *
     * @var LoggerInterface
     */
    public LoggerInterface $logger;

    /**
     * The index if multiple marc values exist.
     *
     * @var int
     */
    public int $valuePos;

    /**
     * FieldRenderContext constructor.
     *
     * @param FieldFormatterRegistry    $fieldFormatterRegistry    the field formatter registry
     * @param SolrMarc                  $solrMarc                  the marc record
     * @param SubfieldFormatterRegistry $subfieldFormatterRegistry the subfield formatter registry
     * @param PhpRenderer               $phpRenderer               vufind's renderer
     * @param string                    $i18nKeyPrefix             the key prefix for translations
     * @param LoggerInterface           $logger                    a logger instance
     * @param int                       $valuePos                  the index if multiple marc values exist.
     */
    public function __construct(
        FieldFormatterRegistry $fieldFormatterRegistry,
        SolrMarc $solrMarc,
        SubfieldFormatterRegistry $subfieldFormatterRegistry,
        PhpRenderer $phpRenderer,
        string $i18nKeyPrefix,
        LoggerInterface $logger,
        int $valuePos
    ) {
        $this->solrMarc = $solrMarc;
        $this->fieldFormatterRegistry = $fieldFormatterRegistry;
        $this->subfieldFormatterRegistry = $subfieldFormatterRegistry;
        $this->processedSubMaps = [];
        $this->phpRenderer = $phpRenderer;
        $this->i18nKeyPrefix = $i18nKeyPrefix;
        $this->logger = $logger;
        $this->valuePos = $valuePos;
    }

    /**
     * Checks, whether a value was already rendered.
     *
     * @param string $candidate a lookup key build from values to render
     *
     * @return bool
     */
    public function alreadyProcessed(string $candidate): bool
    {
        return ($this->processedSubMaps[$candidate] ?? false) === true;
    }

    /**
     * Remember an already rendered value.
     *
     * @param string $candidate the value's lookup key
     *
     * @return void
     */
    public function addProcessed(string $candidate): void
    {
        $this->processedSubMaps[$candidate] = true;
    }

    /**
     * Forget already rendered values.
     *
     * @return void
     */
    public function clearProcessed(): void
    {
        $this->processedSubMaps = [];
    }

    /**
     * Apply a field formatter.
     *
     * @param string               $lookupKey       a value's lookup key
     * @param FieldFormatterData[] $data            the values to render
     * @param FormatterConfig      $formatterConfig the field formatter's config
     * @param string               $labelKey        the field's translation key
     *
     * @return void
     */
    public function applyFieldFormatter(
        string $lookupKey,
        array $data,
        FormatterConfig $formatterConfig,
        string $labelKey
    ): void {
        $this->fieldFormatterRegistry->applyFormatter(
            $formatterConfig,
            $labelKey,
            $data,
            $this
        );
        if (!empty($lookupKey)) {
            $this->addProcessed($lookupKey);
        }
    }

    /**
     * Apply a subfield formatter.
     *
     * @param String             $lookupKey       a value's lookup key
     * @param FieldFormatterData $data            the value to render
     * @param FormatterConfig    $formatterConfig the field formatter's config
     * @param String             $labelKey        the field's translation key
     *
     * @return void
     */
    public function applySubfieldFormatter(
        string $lookupKey,
        FieldFormatterData $data,
        FormatterConfig $formatterConfig,
        string $labelKey
    ): void
    {
        $this->subfieldFormatterRegistry->applyFormatter(
            $formatterConfig,
            $labelKey,
            $data,
            $this
        );
        if (!empty($lookupKey)) {
            $this->addProcessed($lookupKey);
        }
    }
}
