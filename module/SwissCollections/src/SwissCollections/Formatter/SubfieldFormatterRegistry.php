<?php
/**
 * SwissCollections: SubfieldFormatterRegistry.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter;

use Laminas\Log\LoggerInterface;
use Laminas\View\Helper\AbstractHelper;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Registry of all subfield formatters.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class SubfieldFormatterRegistry extends AbstractHelper
{
    /**
     * The map of subfield formatters.
     *
     * @var array<string,SubfieldFormatter>
     */
    protected $registry;

    /**
     * A logger instance.
     *
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * FieldFormatterRegistry constructor.
     *
     * @param LoggerInterface $logger a logger instance
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Register a given subfield formatter.
     *
     * @param string            $name the formatter's name in detail-view-field-structure.yaml
     * @param SubfieldFormatter $ff   the formatter to register
     *
     * @return void
     */
    public function register(string $name, SubfieldFormatter $ff): void
    {
        $this->registry[$name] = $ff;
    }

    /**
     * Get a subfield formatter by name.
     *
     * @param string $name the formatter's name
     *
     * @return null|SubfieldFormatter
     */
    public function get(string $name): ?SubfieldFormatter
    {
        return $this->registry[$name] ?? null;
    }

    /**
     * Apply a field formatter.
     *
     * @param FormatterConfig    $formatterConfig the formatter to apply
     * @param string             $fieldName       the field's name (not the name of the marc subfield)
     * @param FieldFormatterData $fieldData       the value to render
     * @param FieldRenderContext $context         the render context
     *
     * @return void
     */
    public function applyFormatter(
        FormatterConfig $formatterConfig, string $fieldName, FieldFormatterData $fieldData, FieldRenderContext $context
    ): void
    {
        $formatterKey = $formatterConfig->getFormatterName();
        $ff = $this->get($formatterKey);
        if (!empty($ff)) {
            $ff->render($fieldName, $fieldData, $formatterConfig, $context);
        } else {
            $this->logger->err("Unknown subfield formatter: '$formatterKey'");
        }
    }
}
