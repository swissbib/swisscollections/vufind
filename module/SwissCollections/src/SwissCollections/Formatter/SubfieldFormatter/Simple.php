<?php
/**
 * SwissCollections: Simple.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\SubfieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter\SubfieldFormatter;

use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\Formatter\SubfieldFormatter;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Render subfield value as inline html.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\SubfieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Simple extends SubfieldFormatter
{
    /**
     * Html tag to render.
     *
     * @string
     */
    protected string $tag = "span";

    /**
     * Render a subfield value to html.
     *
     * @param string             $fieldName       the field's name
     * @param FieldFormatterData $fieldData       the subfield's value
     * @param FormatterConfig    $formatterConfig the formatter config to apply
     * @param FieldRenderContext $context         the render context
     *
     * @return void
     */
    public function render(string $fieldName, FieldFormatterData $fieldData, FormatterConfig $formatterConfig, FieldRenderContext $context): void
    {
        echo $context->phpRenderer->render(
            '/RecordDriver/SolrMarc/SubfieldFormatter/Simple',
            [
                'fieldData' => &$fieldData,
                'fieldName' => $fieldName,
                'formatter' => $this,
                'context' => $context,
                'formatterConfig' => $formatterConfig,
            ]
        );
    }

    /**
     * Get text html escaped.
     *
     * @param string             $text            the plain text for html output
     * @param FormatterConfig    $formatterConfig the formatter config to apply
     * @param FieldRenderContext $context         the render context
     * @param string             $fieldName       the field's name
     *
     * @return string
     */
    public function getHtml(
        string $text,
        FormatterConfig $formatterConfig,
        FieldRenderContext $context,
        string $fieldName
    ): string {
        $text = self::stripQuoting($text);
        return $context->phpRenderer->escapeHtml($text);
    }

    /**
     * Get the html tag's name.
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * Removes leading/trailing "<<...>>".
     *
     * @param string|null $text non html escaped text
     *
     * @return string|null
     */
    public static function stripQuoting(?string $text): ?string
    {
        if ($text !== null) {
            // remove leading/trailing <<...>>
            $matches = null;
            if (preg_match("/^<<([^>]+)>>(.*)/", $text, $matches) === 1) {
                $part1 = trim($matches[1]);
                $part2 = trim($matches[2]);
                if (strlen($part1) > 0 && strlen($part2) > 0) {
                    $part2 = " " . $part2;
                }
                $text = $part1 . $part2;
            }
            if (preg_match("/^(.*)<<(.+)>>$/", $text, $matches) === 1) {
                $part1 = trim($matches[1]);
                $part2 = trim($matches[2]);
                if (strlen($part1) > 0 && strlen($part2) > 0) {
                    $part1 = $part1 . " ";
                }
                $text = $part1 . $part2;
            }
        }
        return $text;
    }

    /**
     * Removes leading/trailing "&amp;lt;&amp;lt;...&amp;gt;&amp;gt;".
     *
     * @param string|null $html html escaped text
     *
     * @return string|null
     */
    public static function stripHtmlQuoting(?string $html): ?string
    {
        if ($html !== null) {
            $html = trim($html);
            // can't decode given html easily
            // (see vendor/laminas/laminas-escaper/src/Escaper.php)
            $start = "&lt;&lt;";
            $end = "&gt;&gt;";
            $startLen = strlen($start);
            $endLen = strlen($end);

            // eliminate leading "<<...>>"
            if (preg_match("/^" . $start . "/", $html) === 1) {
                $endAt = strpos($html, $end, $startLen);
                if ($endAt !== false) {
                    $part1 = trim(substr($html, $startLen, $endAt - $startLen));
                    $part2 = trim(substr($html, $endAt + $endLen));
                    if (strlen($part1) > 0 && strlen($part2) > 0) {
                        $part2 = " " . $part2;
                    }
                    $html = $part1 . $part2;
                }
            }

            // eliminate trailing "<<...>>"
            if (preg_match("/" . $end . "$/", $html) === 1) {
                $startAt = strrpos($html, $start);
                if ($startAt !== false) {
                    $part1 = trim(substr($html, 0, $startAt));
                    $part2 = trim(
                        substr(
                            $html,
                            $startAt + $startLen,
                            strlen($html) - $startAt - $startLen - $endLen
                        )
                    );
                    if (strlen($part1) > 0 && strlen($part2) > 0) {
                        $part1 = $part1 . " ";
                    }
                    $html = $part1 . $part2;
                }
            }
        }
        return $html;
    }
}
