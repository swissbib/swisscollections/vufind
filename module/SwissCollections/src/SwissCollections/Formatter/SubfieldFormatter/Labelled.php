<?php

/**
 * SwissCollections: Labelled.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\SubfieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter\SubfieldFormatter;

use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Adds a translated label.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\SubfieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Labelled extends Simple
{
    /**
     * Get the text for html.
     *
     * @param string             $value            plain text for output
     * @param FormatterConfig    $formatterConfig the formatter config to apply
     * @param FieldRenderContext $context         the render context
     * @param string             $fieldName       the field's name
     *
     * @return string
     */
    public function getHtml(
        string $value,
        FormatterConfig $formatterConfig,
        FieldRenderContext $context,
        string $fieldName
    ): string {
        // textDomain is optional
        $textDomain = $formatterConfig->optionalConfigEntry(
            'textDomain',
            'default'
        );
        // text is required!
        $text = $formatterConfig->optionalConfigEntry(
            'text',
            ''
        );
        $text = $context->solrMarc->getTranslator()->translate(
            $context->i18nKeyPrefix . '.' . $fieldName . '.' . $text,
            $textDomain
        );
        $formattedText = $text . ': ' . $value;
        return parent::getHtml($formattedText, $formatterConfig, $context, $fieldName);
    }
}
