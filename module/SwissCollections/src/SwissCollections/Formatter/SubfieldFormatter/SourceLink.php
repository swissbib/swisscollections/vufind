<?php
/**
 * SwissCollections: SourceLink.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 8/7/21
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\SubfieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter\SubfieldFormatter;

use SwissCollections\Formatter\SwisscollectionsMarcFormatter;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Creates source links.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\SubfieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class SourceLink extends Simple
{

    /**
     * Gets known key from source text.
     *
     * @param string $text eg. (EXLNZ-41SLSP_NETWORK)991147926609705501
     *
     * @return string the key eg. EXLNZ-41SLSP_NETWORK or empty string
     */
    public static function checkKey(string $text): string
    {
        $key = SwisscollectionsMarcFormatter::getInstitutionFromNodeText($text);
        if (empty($key)) {
            return "";
        }
        return $key;
    }

    /**
     * Get the text for html.
     *
     * @param string             $text            plain text for output
     * @param FormatterConfig    $formatterConfig the formatter config to apply
     * @param FieldRenderContext $context         the render context
     * @param string             $fieldName       the field's name
     *
     * @return string
     */
    public function getHtml(string $text, FormatterConfig $formatterConfig, FieldRenderContext $context, string $fieldName
    ): string {
        $text = trim($text);
        $key = self::checkKey($text);
        $text = trim(substr($text, strlen($key) + 2));
        $url = SwisscollectionsMarcFormatter::getUrl($key, $text);
        $label = $context->solrMarc->translate(
            'page.detail.field.label.Identifikatoren.Quelle.' . $key
        );
        return "<a target='_blank' class='link-external source-link' href='$url'>"
            . $context->phpRenderer->escapeHtml($label) . "</a>";
    }
}