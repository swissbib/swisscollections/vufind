<?php
/**
 * SwissCollections: FieldFormatter.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter;

use Laminas\View\Model\ModelInterface;
use Laminas\View\Renderer\PhpRenderer;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Abstract top class of all field formatters.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
abstract class FieldFormatter
{
    /**
     * Renders given values to html.
     *
     * @param string               $fieldName       the field's name
     * @param FieldFormatterData[] $fieldDataList   the field's values
     * @param FormatterConfig      $formatterConfig the field formatter's config
     * @param FieldRenderContext   $context         the render context
     *
     * @return void
     */
    public function render(
        string $fieldName,
        array $fieldDataList,
        FormatterConfig $formatterConfig,
        FieldRenderContext $context
    ): void {
        $nameOrModel = $this->getTemplateName();
        echo $context->phpRenderer->render(
            $nameOrModel,
            $this->getTemplateArguments(
                $fieldName,
                $fieldDataList,
                $formatterConfig,
                $context
            )
        );
    }

    /**
     * Get the the template's name.
     *
     * @return string|null
     */
    public function getTemplateName(): ?string
    {
        return null;
    }

    /**
     * Helper method to render one subfield to html.
     *
     * @param FieldFormatterData $fd      the information to render
     * @param FieldRenderContext $context the render context
     *
     * @return void
     */
    public function outputValue(
        FieldFormatterData $fd,
        FieldRenderContext $context
    ): void {
        $formatterConfig = $fd->renderConfig->getFormatterConfig();
        // "null" for lookupKey should be OK, because non-sequence fields (see SequencesEntry) should not contain duplicates
        $context->applySubfieldFormatter(
            "",
            $fd,
            $formatterConfig,
            $fd->renderConfig->labelKey
        );
    }

    /**
     * Returns a formatter's name.
     *
     * @return string
     */
    public function __toString()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * Returns translated text for a given key. If no translation exists, the
     * key is returned, where all "." are replaced with "-" (allows html to
     * wrap).
     *
     * @param string             $labelKey the key to translate
     * @param FieldRenderContext $context  the render context
     *
     * @return string
     */
    public function translateLabelKey(string $labelKey, FieldRenderContext $context): string
    {
        $label = $context->phpRenderer->translate(
            $context->i18nKeyPrefix . '.' . $labelKey
        );
        if (str_contains($label, $context->i18nKeyPrefix)) {
            $label = preg_replace("/[.]/", "-", $label);
        }
        return $label;
    }

    /**
     * Build the arguments for the template call.
     *
     * @param string               $fieldName       the field's name
     * @param FieldFormatterData[] $fieldDataList   the field's values
     * @param FormatterConfig      $formatterConfig the field formatter's config
     * @param FieldRenderContext   $context         the render context
     *
     * @return array
     */
    public function getTemplateArguments(
        string $fieldName,
        array $fieldDataList,
        FormatterConfig $formatterConfig,
        FieldRenderContext $context
    ): array {
        return [
            'fieldDataList' => &$fieldDataList,
            'fieldName' => $fieldName,
            'formatter' => $this,
            'context' => $context,
            'formatterConfig' => $formatterConfig,
        ];
    }
}
