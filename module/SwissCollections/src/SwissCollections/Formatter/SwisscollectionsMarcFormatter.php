<?php
/**
 * SwissCollections: SwisscollectionsMarcFormatter.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 8/7/21
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter;

use Swissbib\XSLT\MARCFormatter;

/**
 * Helper class to reuse XSLT MARCFormatter class.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class SwisscollectionsMarcFormatter extends MARCFormatter
{

    // @codingStandardsIgnoreStart
    protected static array $institutionURLs = [
        //Here you can add new source (CMI for example when it is coming)
        "EXLNZ-41SLSP_NETWORK" => "https://swisscovery.slsp.ch/discovery/search?query=any,contains,%s&tab=41SLSP_NETWORK&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION",
        "ZBcollections" => "https://zbcollections.ch/home/#/content/%s",
    ];
    // @codingStandardsIgnoreEnd

    public static function getInstitutionFromNodeText(string $nodeText): string
    {
        return parent::getInstitutionFromNodeText($nodeText);
    }

    /**
     * Use getInstitutionFromNodeText() before to get a valid key from a
     * 035 value of subfield "a".
     *
     * @param string $key a key used in 035
     * @param        $docId
     *
     * @return null|string
     */
    public static function getUrl(string $key, $docId): ?string
    {
        return str_replace(
            '%s', urlencode($docId), self::$institutionURLs[$key]
        );
    }
}