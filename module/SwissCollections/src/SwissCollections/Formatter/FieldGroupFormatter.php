<?php
/**
 * SwissCollections: FieldGroupFormatter.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter;

use SwissCollections\RecordDriver\FieldGroupRenderContext;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\AbstractRenderConfigEntry;
use SwissCollections\Trait\OrderFieldsTrait;

/**
 * Abstract top class of all field group formatters.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
abstract class FieldGroupFormatter
{
    use OrderFieldsTrait;
    /**
     * Renders given values to html.
     *
     * @param AbstractRenderConfigEntry[] $fieldDataList the values to render
     * @param FieldGroupRenderContext     $context       the render context
     *
     * @return void
     */
    abstract public function render(array &$fieldDataList, FieldGroupRenderContext &$context): void;

    /**
     * Helper method to render one field to html.
     *
     * @param AbstractRenderConfigEntry $renderElem      the field's render configuration
     * @param FieldGroupRenderContext   $context         the render context
     * @param String                    $repeatStartHtml html to be output before list items
     * @param String                    $repeatEndHtml   html to be output after list item
     *
     * @return void
     */
    public function outputField(
        AbstractRenderConfigEntry $renderElem,
        FieldGroupRenderContext $context,
        string $repeatStartHtml,
        string $repeatEndHtml
    ): void {
        $fields = $context->solrMarc->getFieldValues(
            $renderElem,
            $context->marcFieldValueProvider
        );
        if (!empty($fields)) {
            if ($renderElem->getFormatterConfig()->isRepeated()) {
                echo $repeatStartHtml;
            }
            $fieldContext = new FieldRenderContext(
                $context->fieldFormatterRegistry,
                $context->solrMarc,
                $context->subfieldFormatterRegistry,
                $context->phpRenderer,
                $context->i18nKeyPrefix,
                $context->logger,
                0
            );
            
            if($orderConfig = $renderElem->getFormatterConfig()->optionalConfigEntry('order', false)) {
                $fields = $this->sortFields($fields, $orderConfig);
            }
            
            foreach ($fields as $pos => $field) {
                $fieldContext->valuePos = $pos;
                $renderElem->render($field, $fieldContext);
            }
            if ($renderElem->getFormatterConfig()->isRepeated()) {
                echo $repeatEndHtml;
            }
        }
    }

    /**
     * Converts a translate key to a well formed css class.
     *
     * @param String $labelKey the key to convert
     *
     * @return string
     */
    public static function labelKeyAsCssClass(String $labelKey): string
    {
        return preg_replace('/[. \\/"§$%&()!=?+*~#\':,;]/', "_", $labelKey);
    }

    /**
     * Returns translated text for a given key. If no translation exists, the
     * key is returned, where all "." are replaced with "-" (allows html to
     * wrap).
     *
     * @param string                  $labelKey     the key to translate
     * @param FieldGroupRenderContext $context      the render context
     * @param array                   $replacements tokens to replace (htlm escape them if needed)
     *
     * @return string
     */
    public function translateLabelKey(
        string $labelKey,
        FieldGroupRenderContext $context,
        array $replacements = []
    ): string {
        $label = $context->phpRenderer->translate(
            $this->i18nKey($labelKey, $context),
            $replacements
        );
        if (str_contains($label, $context->i18nKeyPrefix)) {
            $label = preg_replace("/[.]/", "-", $label);
        }
        return $label;
    }

    /**
     * Builds page specific i18n key of given postfix.
     *
     * @param string                  $labelKey the remaining part of the i18n key
     * @param FieldGroupRenderContext $context  the render context
     *
     * @return string
     */
    public function i18nKey(string $labelKey, FieldGroupRenderContext $context): string
    {
        return $context->i18nKeyPrefix . '.' . $labelKey;
    }
}
