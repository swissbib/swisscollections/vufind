<?php
/**
 * SwissCollections: FieldFormatterRegistry.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter;

use Laminas\Log\LoggerInterface;
use Laminas\View\Helper\AbstractHelper;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Registry of all field formatters.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Formatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class FieldFormatterRegistry extends AbstractHelper
{
    /**
     * The map of field formatters.
     *
     * @var array<string,FieldFormatter>
     */
    protected $registry;

    /**
     * A logger instance.
     *
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * FieldFormatterRegistry constructor.
     *
     * @param LoggerInterface $logger a logger instance
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Register a given field formatter.
     *
     * @param string         $name the formatter's name
     * @param FieldFormatter $ff   the formatter to register
     *
     * @return void
     */
    public function register(string $name, FieldFormatter $ff): void
    {
        $this->registry[$name] = $ff;
    }

    /**
     * Get a field formatter by name.
     *
     * @param string $name a formatter's name
     *
     * @return null|FieldFormatter
     */
    public function get(string $name): ?FieldFormatter
    {
        return $this->registry[$name];
    }

    /**
     * Apply a field formatter.
     *
     * @param FormatterConfig      $formatterConfig the formatter's config
     * @param string               $fieldName       the field's name to render
     * @param FieldFormatterData[] $data            the field's values
     * @param FieldRenderContext   $context         the render context
     *
     * @return void
     */
    public function applyFormatter(
        FormatterConfig $formatterConfig, string $fieldName, array $data, FieldRenderContext &$context
    ): void
    {
        $formatterKey = $formatterConfig->getFormatterName();
        $ff = $this->get($formatterKey);
        if (!empty($ff)) {
            $ff->render($fieldName, $data, $formatterConfig, $context);
        } else {
            $this->logger->err("Unknown field formatter: '$formatterKey'");
        }
    }

    /**
     * @param mixed $registry
     *
     * @return FieldFormatterRegistry
     */
    public function setRegistry(mixed $registry): static
    {
        $this->registry = $registry;
        return $this;
}
}
