<?php
/**
 * SwissCollections: SearchLink.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\FieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter\FieldFormatter;

use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RenderConfig\FormatterConfig;

/**
 * Render subfield values as inline search link to html.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\FieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class SearchLink extends Inline
{
    /**
     * Get the the template's name.
     *
     * @return string
     */
    public function getTemplateName(): string
    {
        return '/RecordDriver/SolrMarc/FieldFormatter/SearchLink';
    }

    /**
     * Formats the field data list to include specific labels based on the given criteria.
     *
     * This method filters the provided field data list to find entries where the
     * MARC subfield name is 'g' and the value does not start with 'yr:' or 'no:'.
     * It then extracts the values of these entries and returns them as a
     * comma-separated string prefixed with a comma. If no matching entries are found,
     * an empty string is returned.
     *
     * @param array &$fieldDataList The list of field data to be filtered and formatted.
     * @param FormatterConfig $formatterConfig The configuration for the formatter (not used in this method).
     * @param FieldRenderContext $context The context for rendering the field (not used in this method).
     *
     * @return string A comma-separated string of filtered label values, or an empty string if none are found.
     */
    public static function enthaltenInFormatter(
        array &$fieldDataList,
        FormatterConfig $formatterConfig,
        FieldRenderContext $context
    ) : string
    {
        $additionalLabels = '';
        foreach ($fieldDataList as $data) {
            if ($data->renderConfig->getMarcSubfieldName() === 'g' &&
                !preg_match('/^(yr:|no:)/', $data->subfieldRenderData->value)) {
                $additionalLabels = ', ' . $data->subfieldRenderData->value;
            }
        }
        return  $additionalLabels;
    }

    /**
     * Special search word formatter for "Sucheinstiege > Besetzung"
     *
     * @param FieldFormatterData[] $fieldDataList   the data to format
     * @param FormatterConfig      $formatterConfig the render configuration
     * @param FieldRenderContext   $context         the render context
     */
    public static function besetzungFormatter(
        array &$fieldDataList, FormatterConfig $formatterConfig, FieldRenderContext $context
    ): string
    {
        $searchString = '';
        // subfield sequence a,n; n is treated optionally
        $at = 0;
        while ($at < count($fieldDataList)) {
            $fd = $fieldDataList[$at];
            if ($fd->renderConfig->getMarcSubfieldName() !== 'a') {
                break;
            }
            if ($at > 0) {
                $searchString .= ',';
            }
            $searchString .= $fd->subfieldRenderData->value;
            $at++;
            if ($at < count($fieldDataList)
                && $fieldDataList[$at]->renderConfig->getMarcSubfieldName()
                === 'n'
            ) {
                $fd = $fieldDataList[$at];
                $searchString .= ' (' . $fd->subfieldRenderData->value . ')';
                $at++;
            }
        }
        return '"' . $searchString . '"';
    }
}
