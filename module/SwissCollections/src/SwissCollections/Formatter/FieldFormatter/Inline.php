<?php
/**
 * SwissCollections: Inline.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\FieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Formatter\FieldFormatter;

use Laminas\View\Model\ModelInterface;
use SwissCollections\Formatter\FieldFormatter;
use SwissCollections\Formatter\FieldFormatterData;

/**
 * Render subfield values inline to html.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\templates\RecordDriver\SolrMarc\FieldFormatter
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Inline extends FieldFormatter
{
    /**
     * Get the the template's name.
     *
     * @return string
     */
    public function getTemplateName(): string
    {
        return '/RecordDriver/SolrMarc/FieldFormatter/Inline';
    }

    /**
     * Get the configured subfield value separator from the render config.
     *
     * @param FieldFormatterData $fd the field's values and render config
     *
     * @return string
     */
    public function getSeparator(FieldFormatterData $fd): string
    {
        return $fd->renderConfig->getFormatterConfig()->getSeparator();
    }
}
