<?php
/**
 * SwissCollections: FieldCondition.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RenderConfig;

use Laminas\Log\LoggerInterface;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Special field condition to compare a given marc subfield's value to a given
 * string.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class RegExpSubfieldCondition extends ConstSubfieldCondition
{

    public static string $conditionPattern = '/[$]([^~]+)~(\\/.+\\/.*)/';

    /**
     * Checks the given field. Returns true if the condition is fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    protected function check(array $field, SolrMarc $solrMarc): bool
    {
        // indicators are checked too, so no need to do it twice
        $marcSubfieldMap = $solrMarc->getMarcFieldRawMap($field, null, []);
        $marcSubfieldValues = $marcSubfieldMap[$this->marcSubfieldName] ?? null;
        if (!empty($marcSubfieldValues)) {
            foreach ($marcSubfieldValues as $v) {
                if (preg_match($this->expectedValue, $v) === 1) {
                    return true;
                }
            }
        }
        $marcSubfieldValuesStr = "";
        if (!empty($marcSubfieldValues)) {
            $marcSubfieldValuesStr = implode(
                ", ", $marcSubfieldValues
            );
        }
        $this->logger->debug(
            $field["tag"]
            . " REGEXP MARC CONDITION FAILED FOR " . $field['tag']
            . ": Constant $this, got [" . $marcSubfieldValuesStr . "]"
        );
        return false;
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return "$" . $this->marcSubfieldName . "~" . $this->expectedValue;
    }

    /**
     * Creates a new instance from the given text.
     *
     * @param string          $text               the text has the format: $SubfieldName=text
     * @param int             $expectedIndicator1 the expected first indicator
     * @param int             $expectedIndicator2 the expected second indicator
     * @param LoggerInterface $logger             a logger instance
     *
     * @return RegExpSubfieldCondition|null
     */
    public static function parse(
        string $text, int $expectedIndicator1, int $expectedIndicator2, LoggerInterface $logger
    ): ?ConstSubfieldCondition
    {
        list($subfieldName, $expectedValue) = self::parseImpl(
            self::$conditionPattern, $text, $expectedIndicator1,
            $expectedIndicator2, $logger
        );
        if (!empty($subfieldName) && !empty($expectedValue)) {
            return new RegExpSubfieldCondition(
                $subfieldName, $expectedValue, $expectedIndicator1,
                $expectedIndicator2, $logger
            );
        }
        return null;
    }
}