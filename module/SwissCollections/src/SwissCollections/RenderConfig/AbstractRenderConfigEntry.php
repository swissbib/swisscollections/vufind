<?php
/**
 * SwissCollections: AbstractRenderConfigEntry.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RenderConfig;

use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RecordDriver\SolrMarc;
use SwissCollections\RecordDriver\SubfieldRenderData;

/**
 * Class AbstractRenderConfigEntry.
 *
 * This class represents all common configuration options of
 * a marc field. The class fields correspond to the columns in
 * detail-fields.csv.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
abstract class AbstractRenderConfigEntry
{
    /**
     * The group's name from detail-fields.csv, column "Gruppierungsname / Oberbegriff".
     *
     * @var String
     */
    public string $groupName;
    /**
     * The field's name from detail-fields.csv, column "Bezeichnung".
     *
     * @var String
     */
    public string $fieldName;
    /**
     * The subfield's name from detail-fields.csv, column "Unterbezeichnung".
     *
     * @var String
     */
    public string $subfieldName;
    /**
     * A synthetic key used especially for translations of this field's name.
     *
     * @var String
     */
    public string $labelKey;
    /**
     * The marc index from from detail-fields.csv, column "datafield tag".
     *
     * @var string
     */
    public string $marcIndex;
    /**
     * The first indicator from from detail-fields.csv, column "datafield ind1".
     *
     * @var int
     */
    public int $indicator1;
    /**
     * The second indicator from from detail-fields.csv, column "datafield ind2".
     *
     * @var int
     */
    public int $indicator2;
    /**
     * The condition  from from detail-fields.csv, column "subfield match condition".
     *
     * @var string|AbstractFieldCondition|null
     */
    public AbstractFieldCondition|string|null $subfieldCondition;

    /**
     * The field formatter to apply.
     *
     * @var FormatterConfig
     */
    protected FormatterConfig $formatterConfig;

    /**
     * The group formatter to apply.
     *
     * @var ?FormatterConfig
     */
    protected FormatterConfig|null $fieldGroupFormatter = null;

    /**
     * Synthetic marc subfield name used by "decorator:" implementation.
     */
    public const DECORATOR_MARC_SUBFIELD_HTML = 'HTML';

    /**
     * AbstractRenderConfigEntry constructor.
     *
     * @param string                      $groupName       the group's name from detail-fields.csv, column "Gruppierungsname / Oberbegriff"
     * @param string                      $fieldName       the field's name from detail-fields.csv, column "Bezeichnung"
     * @param string                      $subfieldName    the subfield's name from detail-fields.csv, column "Unterbezeichnung"
     * @param string                      $marcIndex       the marc index from from detail-fields.csv, column "datafield tag"
     * @param FormatterConfig             $formatterConfig the formatter to apply
     * @param int                         $indicator1      the first indicator from from detail-fields.csv, column "datafield ind1"
     * @param int                         $indicator2      the second indicator from from detail-fields.csv, column "datafield ind2"
     * @param AbstractFieldCondition|null|string $condition       the condition  from from detail-fields.csv, column "subfield match condition"
     */
    public function __construct(
        string $groupName,
        string $fieldName,
        string $subfieldName,
        string $marcIndex,
        FormatterConfig $formatterConfig,
        int $indicator1,
        int $indicator2,
        AbstractFieldCondition|string|null $condition
    ) {
        $this->groupName = $groupName;
        $this->fieldName = $fieldName;
        $this->subfieldName = $subfieldName;
        $this->marcIndex = $marcIndex;
        $this->indicator1 = $indicator1;
        $this->indicator2 = $indicator2;
        $this->formatterConfig = $formatterConfig;
        $this->subfieldCondition = $condition;
        $this->labelKey = self::buildLabelKey(
            $groupName,
            $subfieldName
        );
    }

    /**
     * Build the translation lookup key.
     *
     * @param string $groupName    the group's name
     * @param string $subfieldName the subfield's name
     *
     * @return string
     */
    public static function buildLabelKey(
        string $groupName,
        string $subfieldName
    ): string {
        return $groupName . '.' . $subfieldName;
    }

    /**
     * Returns the formatter's name.
     *
     * @return string
     */
    public function getRenderMode(): string
    {
        return $this->formatterConfig->getFormatterName();
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return 'AbstractRenderConfigEntry{'
            . $this->labelKey . ','
            . $this->groupName . ','
            . $this->fieldName . ','
            . $this->subfieldName . ','
            . $this->marcIndex . ','
            . $this->indicator1 . ','
            . $this->indicator2 . ','
            . $this->conditionsToString() . ','
            . $this->formatterConfig . ','
            . $this->fieldGroupFormatter . '}';
    }

    /**
     * Sort "elements".
     *
     * @return void
     */
    public function orderEntries(): void
    {
        // NOP
    }

    /**
     * Returns all subfield values to render to html which fit this field
     * configuration.
     *
     * @param array              $field   all available marc subfield values
     * @param FieldRenderContext $context the render context
     *
     * @return FieldFormatterData[]
     */
    public function getAllRenderData(array &$field, FieldRenderContext $context): array
    {
        return [];
    }

    /**
     * Contains the given marc field subfields to render to html?
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    public function hasRenderData(array &$field, SolrMarc $solrMarc): bool
    {
        return true;
    }

    /**
     * Exist values to render to html for this configuration?
     *
     * @param SolrMarc               $solrMarc      the marc record
     * @param MarcFieldValueProvider $valueProvider the value provider
     *
     * @return bool
     */
    public function isEmpty(
        SolrMarc $solrMarc,
        MarcFieldValueProvider $valueProvider
    ): bool {
        $fields = $solrMarc->getFieldValues($this, $valueProvider);
        if (!empty($fields)) {
            foreach ($fields as $field) {
                if ($this->hasRenderData($field, $solrMarc)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Create a lookup key to avoid the output of duplicate values to html.
     *
     * @param FieldFormatterData[] $fieldFormatterDataList the values to render to html
     *
     * @return string
     */
    public function calculateRenderDataLookupKey(array $fieldFormatterDataList): string
    {
        $key = '';
        foreach ($fieldFormatterDataList as $ffd) {
            $key .= '{}' . $ffd->subfieldRenderData->asLookupKey();
        }
        return $key;
    }

    /**
     * Set the html code to output around all list items.
     *
     * @param string $start contains the html to render before all list items
     * @param string $end   contains the html to render after all list items
     *
     * @return void
     */
    public function setListHtml(string $start, string $end): void
    {
        $this->formatterConfig->setListHtml($start, $end);
    }

    /**
     * Render the given field values to html.
     *
     * @param FieldFormatterData[] $values  the field values to rendern
     * @param FieldRenderContext   $context the render context
     *
     * @return void
     */
    public function renderImpl(array &$values, FieldRenderContext $context): void
    {
        $lookupKey = $this->calculateRenderDataLookupKey($values);
        if (!empty($values) && !$context->alreadyProcessed($lookupKey)) {
            if ($this->formatterConfig->isRepeated()) {
                echo $this->formatterConfig->listStartHml;
            }
            $this->applyFormatter($lookupKey, $values, $context);
            if ($this->formatterConfig->isRepeated()) {
                echo $this->formatterConfig->listEndHml;
            }
        } elseif (!empty($values) && $context->alreadyProcessed($lookupKey)) {
            $context->logger->debug(
                'Prevented duplicate value output: '
                . print_r($values, true)
            );
        }
    }

    /**
     * Apply the configured formatter to given field values.
     *
     * @param String               $lookupKey a hash key of the values for quick lookup
     * @param FieldFormatterData[] $values    the field values to render to html
     * @param FieldRenderContext   $context   the render context
     *
     * @return void
     */
    public function applyFormatter(string $lookupKey, array &$values, FieldRenderContext $context): void
    {
        $context->applyFieldFormatter(
            $lookupKey,
            $values,
            $this->formatterConfig,
            $this->labelKey
        );
    }

    /**
     * Render the given field values to html.
     *
     * @param array              $field   the marc field to render
     * @param FieldRenderContext $context the render context
     *
     * @return void
     */
    public function render(array &$field, FieldRenderContext $context): void
    {
        $values = $this->getAllRenderData($field, $context);
        $this->renderWithDecorator($values, $context);
    }

    /**
     * Render the given field values to html and apply decorator.
     *
     * @param FieldFormatterData[] $values  the field values to rendern
     * @param FieldRenderContext   $context the render context
     *
     * @return void
     */
    public function renderWithDecorator(array &$values, FieldRenderContext $context): void
    {
        ob_start();
        $this->renderImpl($values, $context);
        $html = ob_get_clean();
        $this->applyDecorator($html, $values, $context);
    }

    /**
     * Apply an optional decorator to given html. Decorators are formatters
     * which support the synthetic marc subfield 'HTML' too.
     *
     * @param string               $html    the already formatted child html
     * @param FieldFormatterData[] $values  the field's data
     * @param FieldRenderContext   $context the render context
     *
     * @return void
     */
    public function applyDecorator(string $html, array $values, FieldRenderContext $context): void
    {
        $decorator = $this->formatterConfig->getDecorator();
        if (!empty($decorator)) {
            $specialConfig = new FormatterConfig(
                $decorator['name'],
                $decorator
            );
            $compoundConfig = new CompoundEntry(
                $this->groupName,
                $this->fieldName,
                $this->subfieldName,
                $this->marcIndex,
                $specialConfig,
                '',
                $this->indicator1,
                $this->indicator2
            );
            $singleConfigHtml = $compoundConfig->addElement(
                $this->subfieldName,
                self::DECORATOR_MARC_SUBFIELD_HTML
            );
            $renderHtmlData = new SubfieldRenderData(
                $html,
                false,
                $this->indicator1,
                $this->indicator2
            );
            $values = [
                ...$values,
                new FieldFormatterData($singleConfigHtml, $renderHtmlData)
            ];
            $context->applyFieldFormatter(
                '',
                $values,
                $specialConfig,
                $this->labelKey
            );
        } else {
            echo $html;
        }
    }

    /**
     * Returns an object of config options from detail-view-field-structure.yaml for this
     * marc field.
     *
     * @return FormatterConfig
     */
    public function getFormatterConfig(): FormatterConfig
    {
        return $this->formatterConfig;
    }

    /**
     * Get the configured field group formatter.
     *
     * @return FormatterConfig|null
     */
    public function getFieldGroupFormatter(): FormatterConfig|null
    {
        return $this->fieldGroupFormatter;
    }

    /**
     * Set the field group formatter.
     *
     * @param FormatterConfig|null $fieldGroupFormatter an instance
     *
     * @return void
     */
    public function setFieldGroupFormatter(
        FormatterConfig|null $fieldGroupFormatter
    ): void {
        // keep default formatter object
        if ($fieldGroupFormatter !== null) {
            $this->fieldGroupFormatter = $fieldGroupFormatter;
        }
    }

    /**
     * Checks the given field and all and'ed conditions. Returns true if all
     * conditions are fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    public function checkCondition(array $field, SolrMarc $solrMarc): bool
    {
        if (empty($this->subfieldCondition)) {
            return true;
        }
        return $this->subfieldCondition->assertTrue($field, $solrMarc);
    }

    /**
     * Create string representation of conditions.
     *
     * @return string
     */
    public function conditionsToString(): string
    {
        $allConditionsStr = '';
        if (!empty($this->subfieldCondition)) {
            $allConditionsStr = $this->subfieldCondition->allConditionsToString(
            );
        }
        return $allConditionsStr;
    }
}
