<?php
/**
 * SwissCollections: FieldCondition.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RenderConfig;

use Laminas\Log\LoggerInterface;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Special field condition to compare a given marc subfield's value to a given
 * string.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class ConstSubfieldCondition extends AbstractFieldCondition
{
    public static string $conditionPattern = '/[$]([^=]+)=(.+)/';

    /**
     * This marc subfield contains the expected value.
     *
     * @var string
     */
    public string $marcSubfieldName;

    /**
     * The expected value.
     *
     * @var string
     */
    protected string $expectedValue;

    /**
     * Expected value of indicator 1
     * ({@link IndicatorCondition::$UNKNOWN_INDICATOR} if unset)
     *
     * @var int
     */
    protected int $expectedIndicator1;

    /**
     * Expected value of indicator 2
     * ({@link IndicatorCondition::$UNKNOWN_INDICATOR} if unset)
     *
     * @var int
     */
    protected int $expectedIndicator2;

    /**
     * ConstSubfieldCondition constructor.
     *
     * @param string          $marcSubfieldName   the subfield's name to check
     * @param string          $expectedValue      the expected value
     * @param int             $expectedIndicator1 the expected first indicator
     * @param int             $expectedIndicator2 the expected second indicator
     * @param LoggerInterface $logger             a logger instance
     */
    public function __construct(
        string $marcSubfieldName,
        string $expectedValue,
        int $expectedIndicator1,
        int $expectedIndicator2,
        LoggerInterface $logger
    ) {
        parent::__construct($logger);
        $this->marcSubfieldName = $marcSubfieldName;
        $this->expectedValue = $expectedValue;
        $this->expectedIndicator1 = $expectedIndicator1;
        $this->expectedIndicator2 = $expectedIndicator2;
    }

    /**
     * Checks the given field. Returns true if the condition is fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    protected function check(array $field, SolrMarc $solrMarc): bool
    {
        // indicators are checked too, so no need to do it twice
        $marcSubfieldMap = $solrMarc->getMarcFieldRawMap($field, null, []);
        $marcSubfieldValues = $marcSubfieldMap[$this->marcSubfieldName] ?? null;
        if (!empty($marcSubfieldValues)) {
            if (in_array($this->expectedValue, $marcSubfieldValues)) {
                return true;
            }
        }
        $marcSubfieldValuesStr = "";
        if (!empty($marcSubfieldValues)) {
            $marcSubfieldValuesStr = implode(
                ", ",
                $marcSubfieldValues
            );
        }
        $this->logger->debug(
            $field["tag"]
            . " CONST MARC CONDITION FAILED FOR " . $field["tag"]
            . ": Constant $this, got [" . $marcSubfieldValuesStr . "]"
        );
        return false;
    }

    /**
     * Creates a new instance from the given text.
     *
     * @param string          $pattern            the pattern to check
     * @param string          $text               the text has the format: $SubfieldName=text
     * @param int             $expectedIndicator1 the expected first indicator
     * @param int             $expectedIndicator2 the expected second indicator
     * @param LoggerInterface $logger             a logger instance
     *
     * @return array
     */
    protected static function parseImpl(
        string $pattern,
        string $text,
        int $expectedIndicator1,
        int $expectedIndicator2,
        LoggerInterface $logger
    ): array
    {
        $text = trim($text);
        if (preg_match($pattern, $text, $matches) === 1) {
            $subfieldName = trim($matches[1]);
            $expectedValue = trim($matches[2]);
            if (strlen($subfieldName) > 0 && strlen($expectedValue) > 0) {
                // "???" is used in csv to mark unknown subfield in condition
                if (!str_contains($subfieldName, "?")) {
                    return [$subfieldName, $expectedValue];
                }
            }
        }
        return [null, null];
    }

    /**
     * Creates a new instance from the given text.
     *
     * @param string          $text               the text has the format: $SubfieldName=text
     * @param int             $expectedIndicator1 the expected first indicator
     * @param int             $expectedIndicator2 the expected second indicator
     * @param LoggerInterface $logger             a logger instance
     *
     * @return ConstSubfieldCondition|null
     */
    public static function parse(
        string $text,
        int $expectedIndicator1,
        int $expectedIndicator2,
        LoggerInterface $logger
    ): ?ConstSubfieldCondition
    {
        list($subfieldName, $expectedValue) = self::parseImpl(
            self::$conditionPattern,
            $text,
            $expectedIndicator1,
            $expectedIndicator2,
            $logger
        );
        if (!empty($subfieldName) && !empty($expectedValue)) {
            return new ConstSubfieldCondition(
                $subfieldName,
                $expectedValue,
                $expectedIndicator1,
                $expectedIndicator2,
                $logger
            );
        }
        return null;
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return "$" . $this->marcSubfieldName . "=" . $this->expectedValue;
    }
}
