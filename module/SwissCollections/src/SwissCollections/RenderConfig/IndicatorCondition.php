<?php
/**
 * SwissCollections: IndicatorCondition.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RenderConfig;

use Laminas\Log\LoggerInterface;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Special field condition to compare a given marc indicator's value to a given
 * int.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class IndicatorCondition extends AbstractFieldCondition
{
    public static int $UNKNOWN_INDICATOR = -1;
    public static int $EMPTY_INDICATOR = -2;

    /**
     * The indicator to check. Either 1 or 2.
     *
     * @var int
     */
    protected int $indicatorId;

    /**
     * The expected value.
     *
     * @var int
     */
    public int $expectedValue;

    /**
     * IndicatorCondition constructor.
     *
     * @param LoggerInterface $logger        a logger instance
     * @param int             $indicatorId   the indicator to check
     * @param int             $expectedValue the expected value
     */
    protected function __construct(
        LoggerInterface $logger,
        int $indicatorId,
        int $expectedValue
    ) {
        parent::__construct($logger);
        $this->indicatorId = $indicatorId;
        $this->expectedValue = $expectedValue;
    }

    /**
     * Creates an {@link IndicatorCondition} from the given text if the text
     * is valid. Otherwise null is returned.
     * This method is only used by the code which parses "detail-fields.csv",
     * where a missing indicator value means "any" value. "#" is used to
     * express, that an indicator's value is expected to be unset.
     *
     * @param int             $indicatorId either 1 or 2
     * @param string          $text        the indicator's expected value
     * @param LoggerInterface $logger      a logger instance
     *
     * @return IndicatorCondition|null
     */
    protected static function buildIndicatorCondition(
        int $indicatorId,
        string $text,
        LoggerInterface $logger
    ): self|null {
        $ind = self::parse($text, self::$UNKNOWN_INDICATOR);
        if ($ind === self::$UNKNOWN_INDICATOR) {
            return null;
        }
        return new self($logger, $indicatorId, $ind);
    }

    /**
     * Creates an {@link IndicatorCondition} from the given text for the first
     * indicator if the text is valid. Otherwise null is returned.
     *
     * @param string          $text   the indicator's expected value
     * @param LoggerInterface $logger a logger instance
     *
     * @return IndicatorCondition|null
     */
    public static function buildIndicator1Condition(string $text, LoggerInterface $logger): self|null
    {
        return self::buildIndicatorCondition(1, $text, $logger);
    }

    /**
     * Creates an {@link IndicatorCondition} from the given text for the second
     * indicator if the text is valid. Otherwise null is returned.
     *
     * @param string          $text   the indicator's expected value
     * @param LoggerInterface $logger a logger instance
     *
     * @return IndicatorCondition|null
     */
    public static function buildIndicator2Condition(string $text, LoggerInterface $logger): self|null
    {
        return self::buildIndicatorCondition(2, $text, $logger);
    }

    /**
     * Parses indicator from text. Returns
     * {@link IndicatorCondition::$UNKNOWN_INDICATOR} for unknown/bad
     * indicator value.
     *
     * @param string|null $text         a positive int or empty string or null
     * @param int         $defaultValue returned if $text is null or an empty string
     *
     * @return int
     */
    public static function parse(string|null $text, int $defaultValue): int
    {
        if ($text === null) {
            return $defaultValue;
        }
        $text = trim($text);
        // note: "0" is a valid indicator value, so don't use empty()!
        if ($text === '') {
            return $defaultValue;
        }
        // "#" means unset indicator value
        if ($text === '#') {
            return self::$EMPTY_INDICATOR;
        }
        if (!ctype_digit($text)) {
            return self::$UNKNOWN_INDICATOR;
        }
        return (int)$text;
    }

    /**
     * Checks the given field. Returns true if the condition is fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    protected function check(array $field, SolrMarc $solrMarc): bool
    {
        try {
            $ind = self::parse(
                $field['i' . $this->indicatorId],
                self::$EMPTY_INDICATOR
            );
            if (!$this->matches($ind)) {
                $this->logger->debug(
                    $field['tag']
                    . ' INDICATOR CONDITION FAILED FOR ' . $field['tag']
                    . ":  {$this}, got " . $ind
                );
                return false;
            }
        } catch (\Throwable $exception) {
            $this->logger->err(
                'Caught Exception ' . $exception->getMessage() . "\n"
                . $exception->getTraceAsString()
            );
            // return true in order not to hide a field's value
        }
        return true;
    }

    /**
     * Matches a given indicator to the expected one.
     * Note: For an expected {@link IndicatorCondition::$UNKNOWN_INDICATOR}
     * no IndicatorCondition is created at all (see {@link IndicatorCondition::buildIndicatorCondition}),
     * so that this method doesn't have to consider this case.
     *
     * @param $currentInd
     *
     * @return bool
     */
    protected function matches($currentInd): bool
    {
        // assert: $this->expectedValue !== self::$UNKNOWN_INDICATOR
        return $currentInd === $this->expectedValue;
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        $expectedValue = $this->expectedValue;
        if ($expectedValue === self::$EMPTY_INDICATOR) {
            $expectedValue = '#';
        }
        return '|' . $this->indicatorId . '|=' . $expectedValue;
    }
}
