<?php
/**
 * SwissCollections: Optional596Condition.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */


namespace SwissCollections\RenderConfig;

use Laminas\Log\LoggerInterface;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * 596 shall only be used if not only subfield $3 exists.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class MoreThan596S3Condition extends AbstractFieldCondition
{
    /**
     * The subfield's name containing the value to display.
     *
     * @var string
     */
    protected string $subfieldName;

    /**
     * MoreThan596S3Condition constructor.
     *
     * @param string          $subfieldName the solr subfield's name
     * @param LoggerInterface $logger       a logger instance
     */
    public function __construct(string $subfieldName, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->subfieldName = $subfieldName;
    }

    /**
     * Checks the given field. Returns true if the condition is fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    protected function check(array $field, SolrMarc $solrMarc): bool
    {
        $marcSubfieldMap = $solrMarc->getMarcFieldRawMap($field, null, []);
        if (!empty($this->subfieldName)) {
            $marcSubfieldValues = $marcSubfieldMap[$this->subfieldName] ?? null;
            return !empty($marcSubfieldValues);
        } else {
            foreach ($marcSubfieldMap as $subfieldName => $values) {
                if ($subfieldName !== "3") {
                    return !empty($values);
                }
            }
            return false;
        }
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return "&MoreThan596$3";
    }

    /**
     * Creates a new ConstSolrFieldCondition instance from a given configuration
     * (defined in "detail-view-field-structure.yaml").
     *
     * @param array           $config the configuration from "condition:".
     * @param LoggerInterface $logger a logger instance
     *
     * @return MoreThan596S3Condition
     */
    public static function creator(array $config, LoggerInterface $logger): MoreThan596S3Condition
    {
        $solrField = $config['subfield'] ?? '';
        return new MoreThan596S3Condition($solrField, $logger);
    }
}