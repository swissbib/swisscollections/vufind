<?php
/**
 * SwissCollections: ConstSolrFieldCondition.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */


namespace SwissCollections\RenderConfig;

use Laminas\Log\LoggerInterface;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Special field condition to compare a given list of values to a given
 * solr field. If one value matches the solr field's value the condition is
 * true.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class ConstSolrFieldCondition extends AbstractFieldCondition
{
    /**
     * The name of the solr field.
     *
     * @var string
     */
    protected string $solrField;

    /**
     * All allowed values.
     *
     * @var string[]
     */
    protected array $allowedValues;

    /**
     * ConstSolrFieldCondition constructor.
     *
     * @param string          $solrField     the solr field's name
     * @param string[]        $allowedValues the allowed values
     * @param LoggerInterface $logger        a logger instance
     */
    public function __construct(string $solrField, array $allowedValues, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->solrField = $solrField;
        $this->allowedValues = $allowedValues;
    }

    /**
     * Checks the given field. Returns true if the condition is fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    protected function check(array $field, SolrMarc $solrMarc): bool
    {
        $values = $solrMarc->getSolrFieldValue($this->solrField);
        foreach ($values as $v) {
            if (in_array($v, $this->allowedValues)) {
                $this->logger->debug(
                    $field["tag"]
                    . " CONST SOLR CONDITION SUCCEEDED FOR " . $this->solrField
                    . ": Constant $this, got [" . implode(",", $values) . "]"
                );
                return true;
            }
        }
        $this->logger->debug(
            message: $field['tag']
            . ' CONST SOLR CONDITION FAILED FOR ' . $this->solrField
            . ": Constant {$this}, got [" . implode(',', $values) . ']'
        );
        return false;
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return '&' . $this->solrField . '=' . implode(
            '|',
            $this->allowedValues
        );
    }

    /**
     * Creates a new ConstSolrFieldCondition instance from a given configuration
     * (defined in "detail-view-field-structure.yaml").
     *
     * @param array           $config the configuration from "condition:".
     * @param LoggerInterface $logger a logger instance
     *
     * @return ConstSolrFieldCondition
     */
    public static function creator(array $config, LoggerInterface $logger): ConstSolrFieldCondition
    {
        $solrField = $config['field'] ?? '';
        $allowedValues = $config['values'] ?? [];
        return new self($solrField, $allowedValues, $logger);
    }
}
