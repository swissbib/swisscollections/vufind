<?php
/**
 * SwissCollections: Signature852Condition.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */


namespace SwissCollections\RenderConfig;

use Laminas\Log\LoggerInterface;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * 852 shall only be used if 949 isn't present.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class Signature852Condition extends AbstractFieldCondition
{
    /**
     * Checks the given field. Returns true if the condition is fulfilled.
     *
     * @param array    $field    the marc field
     * @param SolrMarc $solrMarc the marc record
     *
     * @return bool
     */
    protected function check(array $field, SolrMarc $solrMarc): bool
    {
        // use 852 value only if no 949 entry with same b field value exists
        $values852 = $solrMarc->getMarcFieldsRawMap(852, null, []);
        $values949 = $solrMarc->getMarcFieldsRawMap(949, null, []);

        foreach ($values852 as $v852) {
            if (!empty($v852['b'])) {
                $b = $v852['b'][0];
                foreach ($values949 as $v949) {
                    if (!empty($v949['b']) && $v949['b'][0] === $b) {
                        continue 2;
                    }
                }
                // 852 contains a 'b' not present in 949!
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return "&NotEmpty852Signature";
    }

    /**
     * Creates a new ConstSolrFieldCondition instance from a given configuration
     * (defined in "detail-view-field-structure.yaml").
     *
     * @param array           $config the configuration from "condition:".
     * @param LoggerInterface $logger a logger instance
     *
     * @return Signature852Condition
     */
    public static function creator(array $config, LoggerInterface $logger): Signature852Condition
    {
        return new Signature852Condition($logger);
    }
}