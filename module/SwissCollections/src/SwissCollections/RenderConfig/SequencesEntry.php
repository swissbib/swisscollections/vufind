<?php
/**
 * SwissCollections: SequencesEntry.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RenderConfig;

use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RecordDriver\SolrMarc;
use SwissCollections\RecordDriver\SubfieldRenderData;

/**
 * Class SequencesEntry.
 *
 * Represents a stream of repeating marc subfield sequences of one marc record.
 * A sequence is a defined order of marc subfields.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RenderConfig
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class SequencesEntry extends CompoundEntry
{
    /**
     * All allowed subfield name sequences.
     * Note: SequencesEntry objects always work on marc values, so it is not
     * possible to configure a value provider.
     *
     * @var String[][]
     */
    protected array $sequences;

    /**
     * SequencesEntry constructor.
     *
     * @param string                      $groupName       the group's name from detail-fields.csv, column "Gruppierungsname / Oberbegriff"
     * @param string                      $fieldName       the field's name from detail-fields.csv, column "Bezeichnung"
     * @param string                      $subfieldName    the subfield's name from detail-fields.csv, column "Unterbezeichnung"
     * @param string                      $marcIndex       the marc index from from detail-fields.csv, column "datafield tag"
     * @param FormatterConfig             $formatterConfig the formatter to apply
     * @param int                         $indicator1      the first indicator from from detail-fields.csv, column "datafield ind1"
     * @param int                         $indicator2      the second indicator from from detail-fields.csv, column "datafield ind2"
     * @param AbstractFieldCondition|null $condition       the condition  from from detail-fields.csv, column "subfield match condition"
     */
    public function __construct(
        string $groupName,
        string $fieldName,
        string $subfieldName,
        string $marcIndex,
        FormatterConfig $formatterConfig,
        int $indicator1 = -1,
        int $indicator2 = -1,
        AbstractFieldCondition|null $condition = null
    ) {
        parent::__construct(
            $groupName,
            $fieldName,
            $subfieldName,
            $marcIndex,
            $formatterConfig,
            "",
            $indicator1,
            $indicator2,
            $condition
        );
        $this->formatterConfig->setRepeatedDefault(true);
        if (empty($this->formatterConfig->formatterNameDefault)) {
            $this->formatterConfig->formatterNameDefault = "inline";
        }
    }

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        $seqStr = "[";
        foreach ($this->sequences as $index => $seq) {
            if ($index > 0) {
                $seqStr .= ",";
            }
            $seqStr .= "[" . implode(",", $seq) . "]";
        }
        $seqStr .= "]";
        return "SequencesEntry{" . parent::__toString() . "," . $seqStr . "}";
    }

    /**
     * Calls addElement() for every not already added subfield.
     *
     * @param String[][] $sequences the values from detail-view-field-structure.yaml at "sequences:"
     *
     * @return void
     */
    public function setSequences(array $sequences): void
    {
        $this->sequences = $sequences;
    }

    /**
     * Build a lookup key.
     *
     * @param string $fieldSubfieldName the fields subfield name (column "Unterbezeichnung" in detail-fields.csv)
     * @param string $marcSubfieldName  the marc subfield name (e.g. "a")
     *
     * @return string
     */
    protected function buildSubfieldName(
        string $fieldSubfieldName,
        string $marcSubfieldName
    ): string {
        return $fieldSubfieldName . "-" . $marcSubfieldName;
    }

    /**
     * Searches a given subfield name in the given values and returns the containing value or null.
     *
     * @param String               $subfieldName the subfield's name to find
     * @param FieldFormatterData[] $values       search subfield's name in these values
     *
     * @return null | FieldFormatterData
     */
    protected function inValues(string $subfieldName, array $values): ?FieldFormatterData
    {
        foreach ($values as $ffd) {
            $sn = $this->buildSubfieldName(
                $ffd->renderConfig->subfieldName,
                $ffd->renderConfig->getMarcSubfieldName()
            );
            if ($sn === $subfieldName) {
                return $ffd;
            }
        }
        return null;
    }

    /**
     * Similar to {@link AbstractRenderConfigEntry::orderEntries}, but works on FieldFormatterData[] instead of SingleEntry[].
     *
     * @param FieldFormatterData[] $values sort values by the values returned by {@link SequencesEntry::getEntryOrder}
     *
     * @return FieldFormatterData[]
     */
    protected function orderValues(array $values): array
    {
        $newEntries = [];
        $entryOrder = $this->getEntryOrder();
        if (empty($entryOrder)) {
            $newEntries = $values;
        } else {
            $fieldNames = [];
            foreach ($entryOrder as $fieldFormatter) {
                $fieldName = $fieldFormatter->fieldName;
                $fieldNames[] = $fieldName;
                $ffd = $this->inValues($fieldName, $values);
                if ($ffd) {
                    $newEntries[] = $ffd;
                }
            }
            foreach ($values as $v) {
                $sn = $this->buildSubfieldName(
                    $v->renderConfig->subfieldName,
                    $v->renderConfig->getMarcSubfieldName()
                );
                if (!in_array($sn, $fieldNames)) {
                    $newEntries[] = $v;
                }
            }
        }
        return $newEntries;
    }

    /**
     * Checks whether a subfield sequence matches the current field values.
     *
     * @param array    $rawData  list of tuples (marc subfield names x value)
     *                           (returned by {@link SolrMarc::getMarcFieldRawSequence})
     * @param SolrMarc $solrMarc the marc record
     * @param int      $startPos where to start in $rawData
     *
     * @return array an array with 'endPos' (int) and 'values' (FieldFormatterData[])
     * @throws \Exception
     */
    protected function matchesSubfieldSequence(array $rawData, SolrMarc &$solrMarc, int $startPos): array
    {
        $rawDataLen = count($rawData);
        foreach ($this->sequences as $seq) {
            $pos = $startPos;
            foreach ($seq as $marcSubfieldName) {
                if ($pos >= $rawDataLen) {
                    continue 2;
                }
                if ($rawData[$pos][0] !== $marcSubfieldName) {
                    continue 2;
                }
                $pos++;
            }
            // sequence matched ...
            $seqValues = [];
            $pos = $startPos;
            foreach ($seq as $marcSubfieldName) {
                $text = $rawData[$pos][1];
                $fieldFormatterData = $this->buildFieldFormatterData(
                    $marcSubfieldName,
                    $text,
                    $solrMarc
                );
                $seqValues[] = $fieldFormatterData;
                $pos++;
            }
            // $this->elements are already ordered, but the $values are built from "sequences:" which
            // has to use its own sorting! so, re-sort $values too ...
            $seqValues = $this->orderValues($seqValues);
            return ['endPos' => $pos, 'values' => $seqValues];
        }
        return [];
    }

    /**
     * Add not specified subfields from sequences.
     *
     * @return void
     */
    public function addSubfieldsFromSequences(): void
    {
        foreach ($this->sequences as $seq) {
            foreach ($seq as $marcSubfieldName) {
                if (!$this->knowsSubfield($marcSubfieldName)) {
                    $this->addElement($this->subfieldName, $marcSubfieldName);
                }
            }
        }
    }

    /**
     * Render one given marc field to html. The subfields are filtered by
     * the given {@link SequencesEntry::sequences}.
     *
     * @param array              $field   the current field to render
     * @param FieldRenderContext $context the render context
     *
     * @return void
     * @throws \Exception
     */
    public function render(array &$field, FieldRenderContext $context): void
    {
        $rawData = $context->solrMarc->getMarcFieldRawSequence(
            $field,
            $this->subfieldCondition,
            $this->getHiddenMarcSubfields()
        );
        $pos = 0;
        $isFirst = true;
        $usedValues = [];

        ob_start();
        $count = count($rawData);
        while ($pos < $count) {
            $match = $this->matchesSubfieldSequence(
                $rawData,
                $context->solrMarc,
                $pos
            );
            if (!empty($match)) {
                if (!$isFirst) {
                    $sequenceSeparator
                        = $this->formatterConfig->getSequencesSeparator();
                    if (!empty($sequenceSeparator)) {
                        echo $sequenceSeparator;
                    }
                }
                $this->renderImpl($match['values'], $context);
                // don't prevent single subfield group if occurring in another field value too!
                // in fact, the call of clearProcessed() deactivates the depluction for sequences
                $context->clearProcessed();
                $pos = $match['endPos'];
                $isFirst = false;
                $usedValues = array_merge($usedValues, $match['values']);
            } else {
                $pos++;
            }
        }
        $html = ob_get_clean();
        $this->applyDecorator($html, $usedValues, $context);
    }
}
