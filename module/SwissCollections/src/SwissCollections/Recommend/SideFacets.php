<?php

/**
 * SideFacets Recommendations Module
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind2
 * @package  Recommendations
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Recommend;

use Swissbib\Recommend\SideFacets as ParentSideFacets;

/**
 * SideFacets Recommendations Module
 *
 * This class provides recommendations displaying facets beside search results
 *
 * @category VuFind2
 * @package  Recommendations
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */
class SideFacets extends ParentSideFacets
{
    /**
     * Get facet information from the search results.
     * Core facets merged with mylibraries (favoriten)
     *
     * @return array
     * @throws \Exception
     */
    public function getFacetSet(): array
    {
        $facetSet = parent::getFacetSet();

        /**
         * @var \SwissCollections\VuFind\Search\Solr\Results $results
         */
        $results = $this->results;
        foreach ($facetSet as $key => $value) {
            if ($results->usesIndexSort($key)
                && $results->shallManualSort($key)
            ) {
                $results->sortFacetValues($facetSet[$key]);
            }
        }

        $myLibraries = $this->getMyLibraries();
        return array_merge($myLibraries, $facetSet);
    }
}
