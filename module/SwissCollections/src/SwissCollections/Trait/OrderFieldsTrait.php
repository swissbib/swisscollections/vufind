<?php

namespace SwissCollections\Trait;

trait OrderFieldsTrait
{
    private function sortFields(array $fields, array $orderConfig): array
    {
        if (count($fields) <= 1) {
            return $fields;
        }

        $order = $orderConfig['sort'];
        $fieldKey = array_key_first($orderConfig);
        $fieldFindCriteria = $orderConfig[$fieldKey];
        $values = array_column($fields, $fieldKey);

        $orderPositions = array_map(function($item) use ($order, $fieldFindCriteria) {
            return $this->getFieldPosition($item, $fieldFindCriteria, $order);
        }, $values);

        array_multisort($orderPositions, SORT_ASC, $fields);

        return $fields;
    }

    private function getFieldPosition(array $item, array $fieldFindCriteria, array $order): int
    {
        $matchingValue = array_column($item, $fieldFindCriteria['sortkey'], $fieldFindCriteria['key'])[$fieldFindCriteria['value']] ?? null;
        return ($matchingValue !== null) ? array_search($matchingValue, $order) ?? PHP_INT_MAX : PHP_INT_MAX;
    }
}