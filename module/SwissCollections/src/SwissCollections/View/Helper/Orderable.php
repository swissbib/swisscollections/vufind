<?php
/**
 * SwissCollections: Orderable.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 9/7/21
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Show order tab?
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Orderable extends AbstractHelper
{

    /**
     * Check a given record.
     *
     * @param SolrMarc $record  a record instance
     * @param string[] $docType the record's doc type
     *
     * @return bool
     */
    public function __invoke(SolrMarc $record, array $docType): bool
    {
        $marc949 = $record->getMarcFieldsRawMap(949, null, []);
        $zbCollectionsId = $record->getZBcollectionsID();

        if ($zbCollectionsId) {
            return true;
        }
        // case: Archiv Material without 949 values
        if (!empty($docType) && in_array("AR", $docType) && empty($marc949)) {
            return false;
        }
        // case: check $p values of 949
        $found = 0;
        if (!empty($marc949)) {
            $valuesToHide = ["60", "67", "68", "69", "70"];
            foreach ($marc949 as $v) {
                if (!empty($v['p'])) {
                    foreach ($v['p'] as $sv) {
                        if (in_array($sv, $valuesToHide)) {
                            $found++;
                            break;
                        }
                    }
                }
            }
            if ($found === count($marc949)) {
                return false;
            }
        }

        return true;
    }
}