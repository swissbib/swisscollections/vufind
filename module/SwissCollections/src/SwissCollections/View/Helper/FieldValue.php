<?php
/**
 * SwissCollections:: FieldValue.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use SwissCollections\Formatter\FieldFormatter;
use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\RecordDriver\FieldRenderContext;

/**
 * Enhanced HeadTitle class to remove &lt;&lt;...&gt;&gt;.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class FieldValue extends AbstractHelper
{
    /**
     * Render a given field value.
     *
     * @param FieldFormatterData|null $fd              the value to render
     * @param FieldFormatter          $formatter       the formatter to use for rendering
     * @param FieldRenderContext      $context         an instance
     * @param string|null             $extraCssClasses optional css classes
     *
     * @return string
     */
    public function __invoke(
        ?FieldFormatterData $fd,
        FieldFormatter $formatter,
        FieldRenderContext $context,
        string $extraCssClasses = null
    ): string
    {
        if ($fd !== null) {
            return $this->getView()->render(
                '/RecordDriver/SolrMarc/FieldFormatter/value',
                [
                    'fd' => $fd,
                    'formatter' => $formatter,
                    'context' => $context,
                    'extraCssClasses' => $extraCssClasses
                ]
            );
        }
        return "";
    }
}
