<?php
/**
 * SwissCollections:: LayoutClass.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper;

/**
 * Main layout CSS classes provider.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class LayoutClass extends \VuFind\View\Helper\Bootstrap3\LayoutClass
{
    /**
     * Invoke LayoutClass
     *
     * @param String $class Class
     *
     * @return String
     */
    public function __invoke($class): string
    {
        $classString = '';

        switch ($class) {
        case 'mainbody':
            $classString .= $this->sidebarOnLeft ?
                'col-md-8 col-md-push-3 col-table-fix-md right-md-search-results-gutter' :
                'col-md-8 col-table-fix-md right-md-search-results-gutter';
            break;
        case 'sidebar':
            $classString .= $this->sidebarOnLeft
                ? 'sidebar col-md-4 col-md-pull-9 col-table-fix-md hidden-print'
                : 'sidebar col-md-4 col-table-fix-md hidden-print';
        }

        $htmlLayoutClass = $this->getView()->htmlLayoutClass;

        return isset($htmlLayoutClass) ? $classString . ' ' .
            $htmlLayoutClass : $classString;
    }
}
