<?php
/**
 * SwissCollections:: SwisscoveryOrderLink.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Create an URL for swisscovery ordering.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class SwisscoveryOrderLink extends AbstractHelper
{

    /**
     * Create the URL.
     *
     * @param SolrMarc $record a record instance
     *
     * @return string
     * @throws \Exception
     */
    public function __invoke(SolrMarc $record): string
    {
        $docId = $record->getUniqueID();
        $docIdUrlParm = urlencode($docId);

        $valueList852 = $record->getMarcSubFieldMaps(852, ['9' => 'bib'], false);
        $providers = [];
        if (!empty($valueList852)) {
            foreach ($valueList852 as $bib) {
                $b = $bib['bib'] ?? "";
                $provider = "";
                if (strlen($b) >= 4) {
                    $provider = substr($b, -4, 4);
                }
                if (!empty($provider) && !in_array($provider, $providers)) {
                    $providers[] = $provider;
                }
            }
        }
        $singleProvider = count($providers) === 1;
        if ($singleProvider) {
            $provider = $providers[0];
        } else {
            $provider = "multiple";
        }

        $url = match ($provider) {
            '5504' => "https://basel.swisscovery.org/discovery/fulldisplay?docid=alma"
                . $docIdUrlParm . "&search_scope=DN_and_CI&vid=41SLSP_UBS:live",
            '5505' => "https://rzs.swisscovery.slsp.ch/discovery/fulldisplay?docid=alma"
                . $docIdUrlParm . "&vid=41SLSP_RZS:VU15",
            '5508' => "https://uzb.swisscovery.slsp.ch/discovery/fulldisplay?docid=alma"
                . $docIdUrlParm . "&vid=41SLSP_UZB:UZB",
            '5511' => "https://ubbern.swisscovery.slsp.ch/discovery/fulldisplay?docid=alma"
                . $docIdUrlParm . "&vid=41SLSP_UBE:UBE",
            '5528' => "https://zbs.swisscovery.slsp.ch/discovery/fulldisplay?docid=alma"
                . $docIdUrlParm . "&vid=41SLSP_ZBS:ZBSTestView1",
            default => "https://swisscovery.slsp.ch/discovery/search?query=any,contains,"
                . $docIdUrlParm
                . "&tab=41SLSP_NETWORK&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION",
        };

        return $url;
    }

    /**
     * Is the record old enough?
     * The code below was partially copied from
     * {@link \Swissbib\RecordDriver\Helper\EbooksOnDemand::isValidForLinkA100}
     *
     * @param SolrMarc $record       a record instance
     * @param int      $limitInYears the positive limit
     * @param int      $maxYear      ignored if < 0; else used for the check too
     *
     * @return bool
     */
    protected function isOldEnough(SolrMarc $record, int $limitInYears, int $maxYear): bool
    {
        $yearArray = $record->getPublicationDates();
        $dateType = array_shift($yearArray);
        $currentYear = date("Y");
        preg_replace('/\D/', '9', $yearArray);
        $year1 = intval($yearArray[0]);
        $year2 = intval($yearArray[1]);
        $noSecondYear = 'se';
        if (stripos($noSecondYear, $dateType) !== false) {
            $year = $year1;
        } elseif ($year1 > $year2) {
            $year = $year1;
        } elseif ($year2 > $year1) {
            $year = $year2;
        } else {
            $year = $year1;
        }
        $ok = ($currentYear - $year) >= $limitInYears;
        if ($maxYear >= 0) {
            $ok = $ok && $year <= $maxYear;
        }
        return $ok;
    }
}