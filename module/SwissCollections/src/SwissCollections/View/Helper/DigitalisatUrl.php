<?php
/**
 * SwissCollections: DigitalisatUrl.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 16/7/21
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\Formatter\FieldFormatterRegistry;
use SwissCollections\Formatter\SubfieldFormatterRegistry;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RecordDriver\SolrMarc;
use SwissCollections\RenderConfig\RenderConfig;

/**
 * Get first "Digitalisat" url configured in detail-fields.csv and
 * detail-view-field-structure.yaml.
 * "Digitalisat" is expected to be located in section "Basisinformationen".
 * Also the formatter should be "link". If another formatter is configured,
 * the message
 * "** Expected formatter 'link' for 'Digitalisat'. Can't extract thumbnail url."
 * is written to the log file, because it is unknown how to get the url.
 * The url is extracted by the configured formatter option "urlSubfield".
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class DigitalisatUrl extends AbstractHelper
{

    /**
     * Get the "Digitalisat" url as the formatters do.
     *
     * @param PhpRenderer  $phpRenderer  a non empty instance
     * @param SolrMarc     $record       a non empty instance
     * @param RenderConfig $renderConfig a non empty instance
     *
     * @return bool|string
     */
    public function __invoke(PhpRenderer $phpRenderer, SolrMarc $record, RenderConfig $renderConfig): bool|string
    {
        $thumbSrc = "";
        /**
         * @var SubfieldFormatterRegistry $subfieldFormatterRegistry
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $subfieldFormatterRegistry = $phpRenderer->subfieldFormatterRegistry();
        /**
         * @var FieldFormatterRegistry $fieldFormatterRegistry
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $fieldFormatterRegistry = $phpRenderer->fieldFormatterRegistry();

        $fieldContext = new FieldRenderContext(
            $fieldFormatterRegistry,
            $record,
            $subfieldFormatterRegistry,
            $phpRenderer,
            'page.detail.field.label',
            $renderConfig->logger,
            0
        );

        $rcg = $renderConfig->get("Basisinformationen");
        $rcg->resetHiddenSubfields();
        if (!$rcg->isEmpty($record, $renderConfig)) {
            $digitalisatFields = $rcg->getField("Digitalisat");

            // only first non empty entry is used for link
            foreach ($digitalisatFields as $renderElem) {
                if (!$renderElem->isEmpty($record, $renderConfig)) {
                    $formatterConfig = $renderElem->getFormatterConfig();
                    $formatterName = $formatterConfig->getFormatterName();
                    // we only know how formatter 'link' extracts the url
                    if ($formatterName === 'link') {
                        $urlSubfield = $formatterConfig->optionalConfigEntry(
                            "urlSubfield", ""
                        );
                        if (!empty($urlSubfield)) {
                            $fields = $record->getFieldValues(
                                $renderElem, $renderConfig
                            );
                            if (!empty($fields)) {
                                foreach ($fields as $field) {
                                    $values = $renderElem->getAllRenderData(
                                        $field, $fieldContext
                                    );
                                    $thumbSrc
                                        = FieldFormatterData::valueBySubfield(
                                        $urlSubfield, $values
                                    );
                                    if (!empty($thumbSrc)) {
                                        break 2;
                                    }
                                }
                            }
                        }
                    } else {
                        $renderConfig->logger->err(
                            "** Expected formatter 'link' for 'Digitalisat'. Can't extract thumbnail url."
                        );
                    }
                }
            }

        }

        return $thumbSrc;
    }
}
