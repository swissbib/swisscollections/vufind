<?php
/**
 * SwissCollections: RenderConfigFactory.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper\Root
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper\Root;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\NotFoundExceptionInterface;
use SwissCollections\Csv\CsvReader;
use SwissCollections\RecordDriver\ViewFieldInfo;
use SwissCollections\RenderConfig\RenderConfig;
use VuFind\Config\YamlReader;
use VuFind\Log\Logger;

/**
 * RenderConfigFactory helper factory.
 *
 * @category VuFind
 * @package  View_Helpers
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class RenderConfigFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container     Service manager
     * @param string             $requestedName Service being created
     * @param null|array         $options       Extra options (optional)
     *
     * @return RenderConfig
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws \Exception
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): RenderConfig {
        if (!empty($options)) {
            throw new \Exception('Unexpected options sent to factory.');
        }
        /**
         * A csv reader instance.
         *
         * @var CsvReader $csvReader
         */
        $csvReader = $container->get(CsvReader::class);
        $csvMapping = $csvReader->get('detail-fields.csv');

        /**
         * A yaml reader instance.
         *
         * @var YamlReader $yamlReader
         */
        $yamlReader = $container->get(YamlReader::class);
        $detailViewFieldInfo = $yamlReader->get(
            'detail-view-field-structure.yaml'
        );

        $logger = $container->get(Logger::class);
        return new $requestedName(
            $csvMapping,
            new ViewFieldInfo($detailViewFieldInfo),
            $logger
        );
    }
}
