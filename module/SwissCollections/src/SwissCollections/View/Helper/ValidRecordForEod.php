<?php
/**
 * SwissCollections:: ValidRecordForEod.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use SwissCollections\RecordDriver\SolrMarc;

/**
 * Check whether a given record is valid for EOD ordering.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class ValidRecordForEod extends AbstractHelper
{

    /**
     * Check a given record.
     *
     * @param SolrMarc $record       a record instance
     * @param int      $limitInYears the positive limit
     * @param int      $maxYear      optional: the allowed maximum year
     *
     * @return bool
     */
    public function __invoke(SolrMarc $record, string $library, int $limitInYears, int $maxYear = -1): bool
    {
        $marc945 = $record->getMarcFieldsRawMap(945, null, []);
        $marc947 = $record->getMarcFieldsRawMap(947, null, []);
        $marcAVD = $record->getMarcFieldsRawMap('AVD', null, []);
        $marc852 = $record->getMarcFieldsRawMap(852, null, []);

        $notInAlma = empty($marc945) && empty($marc947) && empty($marcAVD);

        $holdingIsValid = false;
        if (!empty($marc852)) {
            foreach ($marc852 as $value) {
                if (!empty($value['b']) && in_array($library, $value['b'])) {
                    $holdingIsValid = true;
                    break;
                }
            }
        }

        $oldEnough = $this->isOldEnough($record, $limitInYears, $maxYear);

        return $notInAlma && $holdingIsValid && $oldEnough;
    }

    /**
     * Is the record old enough?
     * The code below was partially copied from
     * {@link \Swissbib\RecordDriver\Helper\EbooksOnDemand::isValidForLinkA100}
     *
     * @param SolrMarc $record       a record instance
     * @param int      $limitInYears the positive limit
     * @param int      $maxYear      ignored if < 0; else used for the check too
     *
     * @return bool
     */
    protected function isOldEnough(SolrMarc $record, int $limitInYears, int $maxYear): bool
    {
        $yearArray = $record->getPublicationDates();
        $dateType = array_shift($yearArray);
        $currentYear = date("Y");
        preg_replace('/\D/', '9', $yearArray);
        $year1 = intval($yearArray[0]);
        $year2 = intval($yearArray[1]);
        $noSecondYear = 'se';
        if (stripos($noSecondYear, $dateType) !== false) {
            $year = $year1;
        } elseif ($year1 > $year2) {
            $year = $year1;
        } elseif ($year2 > $year1) {
            $year = $year2;
        } else {
            $year = $year1;
        }
        $ok = ($currentYear - $year) >= $limitInYears;
        if ($maxYear >= 0) {
            $ok = $ok && $year <= $maxYear;
        }
        return $ok;
    }
}