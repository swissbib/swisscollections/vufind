<?php
/**
 * SwissCollections:: Record.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */


namespace SwissCollections\View\Helper;

use Swissbib\View\Helper\Record as SwissbibRecord;
use SwissCollections\Formatter\SubfieldFormatter\Simple;

/**
 * Enhanced view record driver.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\View\Helper
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Record extends SwissbibRecord
{

    /**
     * Use the given coverid if the Cover/Show service shall be used.
     * The implementation is almost a copy of
     * {@link SwissbibRecord::getThumbnail} except, that the given $cover id
     * is used instead of the calculated one.
     *
     * @param string $coverId             the cover id to use
     * @param string $size                optional 'small'
     * @param bool   $fallbackToCoverShow optional true
     *
     * @return array|mixed|string
     */
    public function getThumb(
        string $coverId, string $size = 'small', bool $fallbackToCoverShow = true
    ): mixed
    {
        // Try to build thumbnail:
        $thumb = $this->driver->tryMethod('getThumbnail', [$size]);

        // Array?  It's parameters to send to the cover generator:
        if (is_array($thumb)) {

            // set $coverId in "format", "contenttype"
            if (key_exists('format', $thumb)) {
                $thumb['format'] = $coverId;
            }
            if (key_exists('contenttype', $thumb)) {
                $thumb['contenttype'] = $coverId;
            }

            if ($fallbackToCoverShow) {
                $urlHelper = $this->getView()->plugin('url');

                return $urlHelper('cover-show') . '?' . http_build_query(
                    $thumb
                );
            }

        }

        // Default case -- return fixed string:
        return $thumb;
    }

    /**
     * Get HTML to render a title.
     *
     * @param int $maxLength Maximum length of non-highlighted title.
     *
     * @return string
     */
    public function getTitleHtml($maxLength = 180): string
    {
        // currently no solr field exists which contains all subtitles
        $titleList = $this->driver->tryMethod(
            'getMarcFieldsRawMap', [245, null, []]
        );

        $translate = $this->getView()->plugin('translate');
        $noTitle = $translate('Title not available');

        if (!empty($titleList)) {
            $titles = $titleList[0]['a'] ?? null;
            if (empty($titles)) {
                $title = $noTitle;
            } else {
                $title = $titles[0];
            }
            $subtitles = $titleList[0]['b'] ?? null;
            if (!empty($subtitles)) {
                foreach ($subtitles as $subtitle) {
                    $title .= ": " . $subtitle;
                }
            }

            $escapeHtml = $this->getView()->plugin('escapeHtml');
            $truncate = $this->getView()->plugin('truncate');
            $title = Simple::stripHtmlQuoting(
                $escapeHtml($truncate($title, $maxLength))
            );
        } else {
            $title = $noTitle;
        }

        return $title;
    }
}