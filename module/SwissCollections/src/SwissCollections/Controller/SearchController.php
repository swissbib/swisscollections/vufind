<?php
/**
 * SwissCollections SearchController
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * http://www.swissbib.org  / http://www.swissbib.ch / https://www.ub.unibas.ch
 *
 * Date: 1/2/13
 * Time: 4:09 PM
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  Controller
 * @author   Guenter Hipler  <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://www.swissbib.org
 */

namespace SwissCollections\Controller;

use Laminas\Stdlib\ResponseInterface as Response;
use Laminas\View\Model\ViewModel;
use Swissbib\Controller\SearchController as SwissbibSearchController;
use VuFind\Search\Base\Params;
use VuFind\Search\Solr\Results;

/**
 * SwissCollections SearchController
 *
 * @category Swissbib_VuFind
 * @package  Controller
 * @author   Christoph Böhm  <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org
 */
class SearchController extends SwissbibSearchController
{
    /**
     * Home action
     *
     * @return mixed
     */
    public function homeAction()
    {
        /** @ViewModel */
        $view = parent::homeAction();
        $homePageConfig = $this->getConfig('searches')['HomePage'];
        $carpetConfig = $homePageConfig->carpet;
        $carpetData = [];
        if (!empty($carpetConfig)) {
            foreach ($carpetConfig as $carpetEntry) {
                // earch carpet entry has the form: <img name>:<record id>
                $sepIndex = strpos($carpetEntry, ':');
                if ($sepIndex !== false) {
                    $carpetData[] = [
                        'img' => trim(substr($carpetEntry, 0, $sepIndex)),
                        'recordId' => trim(substr($carpetEntry, $sepIndex + 1)),
                    ];
                }
            }
        }
        $view->imageCarpet = $carpetData;
        $view->itemsPerPageInDocTypeCarousel = $homePageConfig->itemsPerPageInDocTypeCarousel;
        $minDocTypeCount = $homePageConfig->minDocTypeCount;
        $docTypeIconMap = $homePageConfig->docTypeIcon;
        $docTypes = $this->getFacetList(
            'format_hierarchy_str_mv'
        );
        $docTypeIcons = [];
        foreach ($docTypes as $entry) {
            $img = $docTypeIconMap[$entry['value']];
            if (!empty($img)
                && (empty($minDocTypeCount)
                    || $entry['count'] >= $minDocTypeCount
                )
            ) {
                $entry['img'] = $img;
                $docTypeIcons[] = $entry;
            }
        }
        $view->formatFacet = $docTypeIcons;
        return $view;
    }

    /**
     * Gets first level of hierarchical facet
     *
     * @param string $facet
     * @param string $sort
     * @param int    $limit
     *
     * @return array
     */
    public function getFacetList(
        string $facet,
        $sort = 'count',
        $limit = 100
    ): array {
        /** @var Results */
        $results = $this->getResultsManager()->get('Solr');
        /** @var Params */
        $paramsObj = $results->getParams();
        $paramsObj->addFacet($facet, null, false);
        $paramsObj->setFacetPrefix('0/');
        $facets = $results->getPartialFieldFacets(
            [$facet],
            false,
            $limit,
            $sort,
            1,
            true
        );
        return $facets[$facet]['data']['list'];
    }

    /**
     * Overwrites resultsAction to remember search after saveSearchToHistory().
     * Property searchId is no set before. That means, we can not find the
     * search in Memory by id.
     *
     * This fix seems hacky because we only use vanilla VuFind for this steps.
     *
     * If we find out läter how the searchID can be set earlier and remembered
     * correctly, this can be removed.
     *
     * @return ViewModel
     */
    public function resultsAction()
    {
        /** @var ViewModel $viewModel */
        $viewModel =  parent::resultsAction();
        if ($this->rememberSearch) {
            $results = $viewModel->getVariable('results');
            $this->rememberSearch($results);
        }
        return $viewModel;
    }
}
