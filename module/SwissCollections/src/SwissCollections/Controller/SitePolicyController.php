<?php
/**
 * SwissCollections: SitePolicyController.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Controller;

use Laminas\View\Model\ViewModel;
use Swissbib\Controller\BaseController;

/**
 * Class SitePolicyController.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class SitePolicyController extends BaseController
{

    /**
     * Implementation of home action.
     *
     * @return ViewModel
     */
    public function homeAction()
    {
        $data = [];
        $viewModel = new ViewModel($data);
        $this->layout()->breadcrumbs = false;
        $this->layout()->breadcrumbCssClasses = "breadcrumbWrapper--hidden";
        $viewModel->setTemplate(
            'site-policy/' . $this->layout()->userLang . "/home"
        );
        return $viewModel;
    }
}