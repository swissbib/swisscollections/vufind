<?php
/**
 * SwissCollections: AlphaBrowse Module Controller
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  Controller
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Controller;

use Exception;
use Laminas\Config\Config;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Model\ViewModel;
use VuFind\Controller\AlphabrowseController as VuFindAlphabrowseController;
use VuFindSearch\ParamBag;

/**
 * AlphabrowseController Class
 *
 * Controls the alphabetical browsing feature
 *
 * @category SwissCollections_VuFind
 * @package  Controller
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class AlphabrowseController extends VuFindAlphabrowseController implements
    \VuFindHttp\HttpServiceAwareInterface,
    \Laminas\Log\LoggerAwareInterface
{
    use \VuFindHttp\HttpServiceAwareTrait;
    use \VuFind\Log\LoggerAwareTrait;

    /**
     * @var Config
     */
    protected $config;

    /**
     * AlphabrowseController constructor.
     *
     * @param ServiceLocatorInterface $sm
     * @param Config                  $config
     */
    public function __construct(ServiceLocatorInterface $sm, Config $config)
    {
        parent::__construct($sm);
        $this->config = $config;
    }

    /**
     * Gathers data for the view of the AlphaBrowser and does some initialization
     *
     * @return ViewModel
     * @throws Exception
     */
    public function homeAction(): ViewModel
    {
        $groups = $this->getAlphaBrowseGroups();
        $extras = $this->loadExtrasFromConfigFile();
        $limit = $this->getLimit();

        // Process incoming parameters:
        $group = $this->getGroupFromRequest();
        $types = $this->getTypesFromGroup($group);
        $source = $this->params()->fromQuery('source', false);
        $position = $this->params()->fromQuery('page', false);
        $from = $this->params()->fromQuery('from', false);
        $backwards = $this->params()->fromQuery('backwards', false);

        // Set up any extra parameters to pass
        $extraParams = new ParamBag();
        if (isset($extras[$source])) {
            $extraParams->add('extras', $extras[$source]);
        }

        $view = $this->createViewModel();

        // If required parameters are present, load results:
        if ($source && $from !== false) {
            $result = $this->getResults(
                $position,
                $from,
                $limit,
                $backwards,
                $source
            );
            if (!$result->isSuccess()) {
                $error = $result->getBody();
                $this->logWarning(
                    'Alphabrowse failed! position: ' . $position
                            . ', from: ' . $from
                            . ', limit: ' . $limit
                            . ', backwards: ' . $backwards
                            . ', source: ' . $source
                    . ', Error: ' . $result->getStatusCode() . ' '
                    . $error
                );
                $result = [];
                if ($backwards) {
                    $this->setNextPage(
                        $result,
                        $position,
                        $backwards,
                        $limit,
                        $view
                    );
                }
            } else {
                $prevHeader = $result->getHeaders()->get("backwards");
                // If header is missing, assume true.
                $hasPrev = !$prevHeader || $prevHeader->getFieldValue();
                $result = json_decode($result->getBody(), true);

                // Only display next/previous page links when applicable:
                $this->setNextPage(
                    $result,
                    $position,
                    $backwards,
                    $limit,
                    $view
                );

                if ($hasPrev) {
                    $this->setPrevPage(
                        $result,
                        $position,
                        $backwards,
                        $limit,
                        $view
                    );
                }
            }
            $this->setResult($result, $backwards, $limit, $view);
        }

        $view->alphaBrowseTypes = $types;
        $view->alphaBrowseGroups = $groups;
        $view->from = $from;
        $view->source = $source;
        $view->group = $group;

        return $view;
    }

    /**
     * @param     $position
     * @param     $from
     * @param int $limit
     * @param     $backwards
     * @param     $source
     *
     * @return \Laminas\Http\Response
     * @throws Exception
     */
    protected function getResults(
        $position,
        $from,
        int $limit,
        $backwards,
        $source
    ): \Laminas\Http\Response {
        $query = [
            'term' => $position ?: $from,
            'rows' => $limit + 1,
            'backwards' => $backwards
        ];
        $url = $this->config->AlphaBrowse->base_url . '/' . $source;
        $this->log('debug', 'Browse indices: ' . $url . '?' . http_build_query($query));
        return $this->httpService->get(
            $url,
            $query
        );
    }

    /**
     * @param                               $result
     * @param                               $position
     * @param                               $backwards
     * @param int                           $limit
     * @param ViewModel $view
     */
    protected function setPrevPage(
        $result,
        $position,
        $backwards,
        int $limit,
        ViewModel $view
    ): void {
        if (empty($result)) {
            $view->prevpage = $position;
        } elseif ($backwards) {
            $view->prevpage = count($result) >= $limit
                ? $result[1]['fieldvalue'] : $result[0]['fieldvalue'];
        } else {
            $view->prevpage = $result[0]['fieldvalue'];
        }
    }

    /**
     * @param                               $result
     * @param                               $backwards
     * @param int                           $limit
     * @param ViewModel $view
     */
    protected function setResult(
        $result,
        $backwards,
        int $limit,
        ViewModel $view
    ): void {
        if ($backwards) {
            if (count($result) >= $limit) {
                $view->result = array_slice($result, 1, $limit);
            } else {
                $view->result = $result;
            }
        } else {
            $view->result = array_slice($result, 0, $limit);
        }
    }

    /**
     * @param                               $result
     * @param                               $position
     * @param                               $backwards
     * @param int                           $limit
     * @param ViewModel $view
     */
    protected function setNextPage(
        $result,
        $position,
        $backwards,
        int $limit,
        ViewModel $view
    ): void {
        if ($backwards) {
            $view->nextpage = $position;
        } elseif (count($result) >= $limit) {
            $view->nextpage = $result[$limit]['fieldvalue'];
        }
    }

    private function getAlphaBrowseGroups()
    {
        $groups = [];
        foreach ($this->config->AlphaBrowse_Groups as $key => $value) {
            $groups[$key] = $value;
        }
        return $groups;
    }

    private function loadExtrasFromConfigFile()
    {
        $extras = [];
        if (isset($this->config->AlphaBrowse_Extras)) {
            foreach ($this->config->AlphaBrowse_Extras as $key => $value) {
                $extras[$key] = $value;
            }
        }
        return $extras;
    }

    /**
     * @return int
     */
    protected function getLimit(): int
    {
        $limit = isset($this->config->AlphaBrowse->page_size)
        && is_numeric($this->config->AlphaBrowse->page_size)
            ? (int)$this->config->AlphaBrowse->page_size : 20;
        return $limit;
    }

    /**
     * @param       $group
     *
     * @return array
     */
    protected function getTypesFromGroup($group): array
    {
        $types = [];
        if ($group && $this->config->AlphaBrowse_Types->get($group)) {
            foreach ($this->config->AlphaBrowse_Types->get($group) as $key => $value
            ) {
                $types[$key] = $value;
            }
        }
        return $types;
    }

    /**
     * @return string
     */
    protected function getGroupFromRequest()
    {
        $group = $this->params()->fromQuery('group', false);
        if ($group && !$this->config->AlphaBrowse_Groups->get($group . '')) {
            $group = '';
        }
        return $group;
    }
}
