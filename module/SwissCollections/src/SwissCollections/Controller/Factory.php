<?php
/**
 * SwissCollections: Factory.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Controller;

use Laminas\ServiceManager\ServiceManager;
use VuFind\Controller\AbstractBaseFactory;

/**
 * Class Factory.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class Factory extends AbstractBaseFactory
{
    /**
     * Factory method to create a new {@link KeywordSearchController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return KeywordSearchController
     */
    public static function getKeywordSearchController(ServiceManager $sm)
    {
        return new KeywordSearchController($sm);
    }

    /**
     * Factory method to create a new {@link AbcSearchController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return AbcSearchController
     */
    public static function getAbcSearchController(ServiceManager $sm)
    {
        return new AbcSearchController($sm);
    }

    /**
     * Factory method to create a new {@link getTektonikController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return TektonikController
     */
    public static function getTektonikController(ServiceManager $sm)
    {
        return new TektonikController($sm);
    }

    /**
     * Factory method to create a new {@link BibliographiesController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return BibliographiesController
     */
    public static function getBibliographiesController(ServiceManager $sm)
    {
        return new BibliographiesController($sm);
    }

    /**
     * Factory method to create a new {@link BrowseController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return BrowseController
     */
    public static function getBrowseController(ServiceManager $sm)
    {
        return new BrowseController($sm);
    }

    /**
     * Factory method to create a new {@link AboutController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return AboutController
     */
    public static function getAboutController(ServiceManager $sm)
    {
        return new AboutController($sm);
    }

    /**
     * Factory method to create a new {@link ContactController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return ContactController
     */
    public static function getContactController(ServiceManager $sm)
    {
        return new ContactController($sm);
    }

    /**
     * Factory method to create a new {@link ImpressController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return ImpressController
     */
    public static function getImpressController(ServiceManager $sm)
    {
        return new ImpressController($sm);
    }

    /**
     * Factory method to create a new {@link PrivacyController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return PrivacyController
     */
    public static function getPrivacyController(ServiceManager $sm)
    {
        return new PrivacyController($sm);
    }

    /**
     * Factory method to create a new {@link PartnersController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return PartnersController
     */
    public static function getPartnersController(ServiceManager $sm)
    {
        return new PartnersController($sm);
    }

    /**
     * Factory method to create a new {@link SitePolicyController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return SitePolicyController
     */
    public static function getSitePolicyController(ServiceManager $sm)
    {
        return new SitePolicyController($sm);
    }

    /**
     * Factory method to create a new {@link DataExportController}
     *
     * @param ServiceManager $sm the service manager
     *
     * @return DataExportController
     */
    public static function getDataExportController(ServiceManager $sm)
    {
        return new DataExportController($sm);
    }
}