<?php
/**
 * SwissCollections: TektonikController.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Controller;

use Laminas\View\Model\ViewModel;
use Swissbib\Controller\BaseController;

/**
 * Class TektonikController.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class TektonikController extends BaseController
{

    /**
     * Implementation of home action.
     *
     * @return ViewModel
     */
    public function homeAction()
    {
        return $this->tektonikAction();
    }

    /**
     * Implementation of topics action.
     *
     * @return ViewModel
     */
    public function topicsAction()
    {
        return $this->createTectonicsViewModel("topics");
    }

    /**
     * Implementation of business docs action.
     *
     * @return ViewModel
     */
    public function businessDocsAction()
    {
        return $this->createTectonicsViewModel("business-docs");
    }

    /**
     * Implementation of tectonics action.
     *
     * @return ViewModel
     */
    public function tektonikAction()
    {
        return $this->createTectonicsViewModel("tektonik");
    }

    /**
     * @param string $tektonikTab
     *
     * @return ViewModel
     */
    private function createTectonicsViewModel(string $tektonikTab): ViewModel
    {
        $viewModel = $this->createViewModel(["tektonikTab" => $tektonikTab]);
        $viewModel->setTemplate('tektonik/home');
        return $viewModel;
    }
}
