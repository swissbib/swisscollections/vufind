<?php
/**
 * SwissCollections: DataExportController.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Controller;

use Laminas\Http\Client;
use Laminas\Http\Request;
use Laminas\View\Model\ViewModel;
use Swissbib\Controller\SearchController as SwissbibSearchController;
use SwissCollections\Adapter\StreamAdapter;
use Symfony\Component\HttpFoundation\HeaderUtils;
use VuFind\Search\BackendManager;
use VuFindSearch\Backend\Solr\QueryBuilder;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class DataExportController.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Controller
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class DataExportController extends SwissbibSearchController
{

    /**
     * Implementation of home action.
     *
     * @return ViewModel
     */
    public function homeAction()
    {
        $data = [];
        $viewModel = $this->createViewModel($data);
        $this->layout()->breadcrumbs = false;
        $this->layout()->breadcrumbCssClasses = "breadcrumbWrapper--hidden";
        $viewModel->setTemplate(
            'data-export/' . $this->layout()->userLang . "/home"
        );
        return $viewModel;
    }

    public function infoAction()
    {
        $data = [];
        $viewModel = $this->createViewModel($data);
        $this->layout()->breadcrumbs = false;
        $this->layout()->breadcrumbCssClasses = "breadcrumbWrapper--hidden";
        $viewModel->setTemplate(
            'data-export/' . $this->layout()->userLang . "/info"
        );
        return $viewModel;
    }

    /**
     * Implementation of home action.
     *
     * @return ViewModel
     */
    public function downloadStartInfoAction()
    {
        $recordTotal = (integer)$this->params()->fromQuery('record_total');
        $exportType = (string)$this->params()->fromQuery('export_type');

        $processTime = $this->getTimeForRecordsProcessing($recordTotal, $exportType);

        $data = [
            'recordTotal' => number_format($recordTotal, 0, '.', "'"),
            'processTime' => $processTime
        ];

        $viewModel = $this->createViewModel($data);
        $this->layout()->breadcrumbs = false;
        $this->layout()->breadcrumbCssClasses = "breadcrumbWrapper--hidden";
        $viewModel->setTemplate(
            'data-export/de/download-start'
        );
        return $viewModel;
    }

    private function getTimeForRecordsProcessing($recordTotal, $type)
    {
        // Calculate time in minutes
        if ($type === 'data_packet') {
            $totalTime = $recordTotal / 500;
        } else if ($type === 'csv') {
            $totalTime = $recordTotal / 5000;
        }

        // Convert total time to hours and minutes
        $days = floor($totalTime / 1440); // 1440 minutes in a day
        $hours = floor(((int)$totalTime % 1440) / 60);
        $minutes = round((int)$totalTime % 60);

        // Construct the time string
        $timeString = '';
        if ($days > 0) {
            $timeString .= $days . ' ' . ($days == 1 ? $this->translate('duration_day') : $this->translate('duration_days')) . ' ';
        }
        if ($hours > 0) {
            $timeString .= $hours . ' ' . ($hours == 1 ? $this->translate('duration_hour') : $this->translate('duration_hours')) . ' ';
        }
        if ($minutes > 0 || $timeString === '') {
            $timeString .= $minutes . ' ' . ($minutes == 1 ? $this->translate('duration_minute') : $this->translate('duration_minutes'));
        }

        return trim($timeString);
    }

    /**
     * Implementation of export data action.
     */
    public function downloadAction()
    {
        $exportFile = $this->params()->fromQuery('export-selection');
        $exportType = $this->params()->fromQuery('export-type');
        $exportQuery = $this->params()->fromQuery('export-data');
        $searchId = $this->params()->fromQuery('search-id');

        $url = $this->getUrl($exportFile, $exportQuery, $exportType, $searchId);

        // Set unlimited timeout
        $client = new Client($url, ['timeout' => 0]);
        $client->setMethod(Request::METHOD_GET);

        $adapter = $this->configureCurlAdapter();
        $client->setAdapter($adapter);

        return $client->send();
    }

    /**
     * @return QueryBuilder
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getSolrQueryBuilder(): QueryBuilder
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->serviceLocator->get(BackendManager::class)->get(
            'Solr'
        )->getQueryBuilder();
        return $queryBuilder;
    }

    protected function getCSVConfig()
    {
        return $this->serviceLocator->get('VuFind\Config\PluginManager')->get('config')->get('CSV');
    }

    /**
     * @param mixed $exportFile
     * @param mixed $exportQuery
     * @param mixed $exportType
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getUrl(string $exportFile, mixed $exportQuery, string $exportType, string $searchId): string
    {
        $refererUrl = $this->getRequest()->getHeaders('Referer')->getFieldValue('query');

        $search = $this->getSearchMemory()->getSearchBySearchId((int) $searchId);

        /** @var \VuFind\Search\Solr\Params $params */
        $params = $search->getParams();
        $backendParams = $params->getBackendParameters();

        $queryBuilder = $this->getSolrQueryBuilder();

        $backendParams->mergeWith($queryBuilder->build($params->getQuery()));
        $paramString = implode('&', $backendParams->request());

        $urlQuery = 'solr_query=' . ($exportFile === 'marked' ? $exportQuery : sprintf('%s', urlencode($paramString)));

        $url = $this->getCSVConfig()->base_url . ($exportType === 'csv' ? 'csv-export' : 'data-package');

        $url .= '?' . $urlQuery;

        return $exportType === 'data-packet' ? $url . '&vufind_url=' . urlencode($refererUrl) : $url;
    }

    public function configureCurlAdapter(): Client\Adapter\Curl
    {
        $adapter = new Client\Adapter\Curl();
        $headers = [];
        $outputStream = fopen('php://output', 'wb');
        $adapter->setOutputStream($outputStream);
        $adapter->setOptions(
            [
                'curl_options' => [
                    CURLOPT_HEADERFUNCTION => function ($ch, $header) use (&$headers, $adapter) {
                        if (!in_array($header, $headers)) {
                            $headers[] = $header;
                            header($header);
                        }
                        $adapter->readHeader($ch, $header);
                        return strlen($header);
                    },
                ]
            ]);
        return $adapter;
    }

    /**
     * Get the search memory
     *
     * @return \VuFind\Search\Memory
     */
    public function getSearchMemory()
    {
        return $this->serviceLocator->get(\SwissCollections\VuFind\Search\Memory::class);
    }

}
