<?php
/**
 * Hierarchy Controller
 *
 * PHP version 7
 *
 * Copyright (C) Villanova University 2010.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  Controller
 * @author   Demian Katz <demian.katz@villanova.edu>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development:plugins:controllers Wiki
 */

namespace SwissCollections\Controller;

use Laminas\Config\Config;
use Laminas\Escaper\Escaper;
use Laminas\Http\Response;
use Laminas\Log\LoggerAwareInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use VuFind\I18n\HasSorterInterface;
use VuFind\I18n\HasSorterTrait;
use VuFind\Log\LoggerAwareTrait;
use VuFind\Search\Results\PluginManager;

/**
 * Hierarchy Controller
 *
 * @category VuFind
 * @package  Controller
 * @author   Demian Katz <demian.katz@villanova.edu>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development:plugins:controllers Wiki
 */
class HierarchyController extends \VuFind\Controller\HierarchyController implements
    LoggerAwareInterface, HasSorterInterface
{
    use LoggerAwareTrait;
    use HasSorterTrait;

    /**
     * @var Config
     */
    protected Config $config;

    /**
     * @v2ar \Laminas\Escaper\Escaper
     */
    protected Escaper $escaper;
    /**
     * @var PluginManager
     */
    protected PluginManager $resultsManager;

    /**
     * Constructor
     *
     * @param ServiceLocatorInterface $sm
     * @param Config                  $config
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ServiceLocatorInterface $sm, Config $config)
    {
        parent::__construct($sm);
        $this->resultsManager = $sm->get(
            PluginManager::class
        );
        $this->config = $config;
        $this->escaper = new Escaper('utf-8');
    }

    /**
     * Returns JSON for leaf of /Hierarchy/GetTectonicsJSON
     * Requires parameter id
     *
     * @return Response
     */
    public function gettopicstectonicsjsonAction(): Response
    {
        $facetName = $this->config->TopicsTectonics->get('facet', 'topic_tectonic_str_mv');
        return $this->getTreeAsJSON($facetName);
    }

    /**
     * Returns JSON for leaf of /Hierarchy/GetTopicsTectonicsJSON
     * Requires parameter id
     *
     * @return \Laminas\Http\Response
     */
    public function gettectonicsjsonAction(): Response
    {
        $facetName = $this->config->Tectonics->get('facet', 'tectonics_str_mv');
        return $this->getTreeAsJSON($facetName);
    }

    /**
     * Gets a Hierarchy Tree
     *
     * @param string $tectonicsFacet
     *
     * @return \Laminas\Http\Response
     */
    private function getTreeAsJSON(string $tectonicsFacet): Response
    {
        $this->disableSessionWrites();  // avoid session write timing bug

        // Retrieve the record from the index
        $id = $this->params()->fromQuery('id');
        $source = $this->params()
            ->fromQuery('hierarchySource', DEFAULT_SEARCH_BACKEND);
        $results = $this->resultsManager->get($source);
        $paramsObj = $results->getParams();
        $paramsObj->addFacet($tectonicsFacet, null, false);
        $paramsObj->addFilter($tectonicsFacet . ':' . $id);

        $records = $results->getResults();

        if (!empty($records)) {
            $nodes = $this->getHierarchy(
                $records,
                $results->getResultTotal(),
                $id
            );
            if ($nodes) {
                return $this->outputJSON($nodes);
            }
        }

        // If we got this far, something went wrong:
        return $this->outputJSON(
            json_encode(['error' =>  'No record found for facet: ' . $id ]),
            503
        ); // Service Unavailable
    }

    /**
     * @param $records
     * @param $total
     * @param $facet
     *
     * @return false|string
     */
    protected function getHierarchy($records, $total, $facet): bool|string
    {
        $json = false;
        if (count($records) > 1) {
            $nodes = [];
            foreach ($records as $recordDriver) {
                $nodes[] = $this->createNode($recordDriver);
            }
            if ($total > count($records)) {
                $nodes[] = $this->createMoreNode($facet);
            }
            $json = json_encode($nodes);
        } else {
            try {
                $recordDriver = $records[0];
                $treeRenderer = $recordDriver->getHierarchyDriver()
                    ->getTreeRenderer($recordDriver);
                $json = $treeRenderer->getJSON(
                    $recordDriver->getUniqueID(),
                    $this->params()->fromQuery('context')
                );
                if ($json) {
                    $children = json_decode($json)->children;
                    if ($children) {
                        $json = json_encode($children);
                    }
                } else {
                    $json = json_encode($this->createNode($recordDriver));
                }
            } catch (\Exception $e) {
                $this->logError("Failed to get hierarchy %s: %s", [$e->getMessage(), $e->getTraceAsString()]);
            }
        }
        return $json;
    }

    /**
     * @param $recordDriver
     *
     * @return array
     */
    protected function createNode($recordDriver): array
    {
        $childRecordCount = $recordDriver->getChildRecordCount();
        return [
            'id' => preg_replace(
                '/\W/',
                '-',
                $recordDriver->getUniqueID()
            ),
            'text' => $this->escaper->escapeHtml(
                $recordDriver->getTitle()
            ),
            'li_attr' => [
                'recordid' => $recordDriver->getUniqueID()
            ],
            'a_attr' => [
                'href' => "/Record/"
                    . $recordDriver->getUniqueID(),
                'title' => $recordDriver->getTitle()
            ],
            'type' => "record",
            'children' => $childRecordCount > 0
        ];
    }

    /**
     * @param $facet
     *
     * @return array
     */
    protected function createMoreNode($facet): array
    {
        return [
            'id' => $facet,
            'text' => $this->translate("more"),
            'li_attr' => [
                'recordid' => $facet
            ],
            'a_attr' => [
                'href' =>
                    '/Search/Results?type=AllFields&filter[]=tectonics_str_mv:'
                    . $facet,
                'title' => $this->translate("more")
            ],
            'type' => "record",
            'children' => false
        ];
    }
}
