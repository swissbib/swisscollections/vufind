<?php
/**
 * Abstract "Get Facet Data" AJAX handler
 *
 * PHP version 7
 *
 * Copyright (C) Villanova University 2018.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  AJAX
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */

namespace SwissCollections\AjaxHandler;

use Laminas\Config\Config;
use Laminas\Log\LoggerAwareInterface;
use Laminas\Mvc\Controller\Plugin\Params;
use Laminas\ServiceManager\ServiceLocatorInterface;
use VuFind\I18n\Translator\TranslatorAwareInterface;
use VuFind\I18n\Translator\TranslatorAwareTrait;
use VuFind\Log\LoggerAwareTrait;

/**
 * Allows caching
 *
 * Get hierarchical facet data for jsTree for tectonics
 *
 * Parameters:
 * facetName  The facet to retrieve
 * facetSort  By default all facets are sorted by count. Two values are available
 * for alternative sorting:
 *   top = sort the top level alphabetically, rest by count
 *   all = sort all levels alphabetically
 *
 * @category SwissCollections_VuFind
 * @package  AJAX
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
abstract class GetCachedTectonicsData extends GetFacetData implements
    LoggerAwareInterface,
    TranslatorAwareInterface
{
    use LoggerAwareTrait;
    use TranslatorAwareTrait;

    protected string $jsonParent;
    /**
     * @var string
     */
    protected string $cacheDir;
    /**
     * @var int
     */
    protected int $cacheTime;

    /**
     * Constructor
     *
     * @param ServiceLocatorInterface $sm
     * @param Config                  $config
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ServiceLocatorInterface $sm, Config $config)
    {
        parent::__construct($sm, $config);
        $this->facetLimit = new Config([], false);
    }


    /**
     * @param Params $params Parameter helper from controller
     *
     * @return array [response data, HTTP status code]
     * @throws \Exception
     */
    public function handleRequest(Params $params): array
    {
        $refresh = $params->fromQuery('refresh');
        $locale = $this->getTranslatorLocale('de');
        $cacheFile = rtrim($this->cacheDir, '/')
            . '/' . $this->jsonParent . '-' . $locale . '.json';

        $useCache = $refresh !== 'true';
        if ($useCache
            && file_exists($cacheFile)
            && (
                $this->cacheTime < 0
                || filemtime($cacheFile) > (time() - $this->cacheTime)
            )
        ) {
            $this->debug("Using cached data from $cacheFile");
            $json = json_decode(file_get_contents($cacheFile), false);
            return $this->formatResponse([$this->jsonParent => $json]);
        }

        $response = parent::handleRequest($params);
        if ($cacheFile) {
            // Write file
            if (!file_exists($this->cacheDir)) {
                if (!mkdir($concurrentDirectory = $this->cacheDir)
                    && !is_dir(
                        $concurrentDirectory
                    )
                ) {
                    $this->logWarning(
                        'Directory was not created',
                        ['directory' => $concurrentDirectory]
                    );
                }
            }
            file_put_contents(
                $cacheFile,
                json_encode($response[0][$this->jsonParent])
            );
            $this->log('info', 'Wrote new tectonics cache file', ['type' => $this->jsonParent, 'cacheFile' => $cacheFile]);
        }
        return $response;
    }
}
