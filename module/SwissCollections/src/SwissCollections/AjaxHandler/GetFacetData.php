<?php
/**
 * "Get Facet Data" AJAX handler
 *
 * PHP version 7
 *
 * Copyright (C) Villanova University 2018.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  AJAX
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */

namespace SwissCollections\AjaxHandler;

use Exception;
use Laminas\Config\Config;
use Laminas\Mvc\Controller\Plugin\Params;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\Stdlib\Parameters;
use VuFind\AjaxHandler\GetFacetData as VuFindGetFacetData;
use VuFind\Search\Results\PluginManager;
use VuFind\Search\Solr\HierarchicalFacetHelper;
use VuFind\Session\Settings;

/**
 * Overwritten to allow a limit to avoid
 * 'Fatal error: Allowed memory size of 134217728 bytes exhausted'
 *
 * "Get Facet Data" AJAX handler
 *
 * Get hierarchical facet data for jsTree
 *
 * Parameters:
 * facetLimit Limit for number of facets to fetch
 * facetName  The facet to retrieve
 * facetSort  By default all facets are sorted by count. Two values are available
 * for alternative sorting:
 *   top = sort the top level alphabetically, rest by count
 *   all = sort all levels alphabetically
 *
 * @category SwissCollections_VuFind
 * @package  AJAX
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class GetFacetData extends VuFindGetFacetData
{
    protected string $jsonParent = 'facets';
    /**
     * @var Config
     */
    protected Config $facetLimit;
    /**
     * @var Config
     */
    protected Config $levels;
    /**
     * @var Config
     */
    protected Config $translatedSubFacets;

    /**
     * Constructor
     *
     * @param ServiceLocatorInterface $sm
     * @param Config                  $config
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ServiceLocatorInterface $sm, Config $config)
    {
        parent::__construct(
            $sm->get(Settings::class),
            $sm->get(HierarchicalFacetHelper::class),
            $sm->get(PluginManager::class)
        );
        $this->facetLimit = $config->AjaxFacets->get('facetLimit');
        $this->levels = $config->AjaxFacets->get('levels');
        $this->translatedSubFacets = $config->AjaxFacets->get(
            'translatedSubFacets'
        );
    }


    /**
     * Handle a request. Use Limit!!!
     *
     * @param Params $params Parameter helper from controller
     *
     * @return array [response data, HTTP status code]
     * @throws Exception
     */
    public function handleRequest(Params $params): array
    {
        $this->disableSessionWrites();  // avoid session write timing bug

        $facet = $params->fromQuery('facetName');
        $sort = $params->fromQuery('facetSort');
        $operator = $params->fromQuery('facetOperator');
        $backend = $params->fromQuery('source', DEFAULT_SEARCH_BACKEND);
        $limit = $params->fromQuery('facetLimit', $this->getFacetLimit($facet));

        $results = $this->resultsManager->get($backend);
        $paramsObj = $results->getParams();
        $paramsObj->addFacet($facet, null, $operator === 'OR');
        $paramsObj->initFromRequest(new Parameters($params->fromQuery()));

        if ($this->getLevels($facet) > 0) {
            // Get first level of facet and then the next levels
            $level = 0;
            $paramsObj->setFacetPrefix($level . '/');
            $facetList = $this->getFacetsList($results, $facet, $limit);
            $facetList = $this->getSubFacetList(
                $level,
                $facetList,
                $results,
                $facet
            );
        } else {
            $facetList = $this->getFacetsList($results, $facet, $limit);
        }

        if (empty($facetList)) {
            $facets = [];
        } else {
            $this->facetHelper->sortFacetList($facetList, $sort);
            $facets = $this->facetHelper->buildFacetArray(
                $facet,
                $facetList,
                $results->getUrlQuery(),
                false
            );
        }
        foreach ($facets as &$facet) {
            // Un-escaping on 1st level should be sufficient
            if (array_key_exists('displayText', $facet)) {
                $facet['displayText'] = str_replace('\\/', '/', $facet['displayText']);
            }
        }

        return $this->formatResponse(
            [
                $this->jsonParent => $facets,
                'more' => ($limit > 0 && $results->getResultTotal() > $limit)
            ]
        );
    }

    /**
     * @param int   $level
     * @param array $facetList
     * @param       $results
     * @param       $facet
     *
     * @return array
     */
    protected function getSubFacetList(int $level, array $facetList, $results, $facet): array
    {
        $level++;
        if ($level < $this->getLevels($facet)) {
            foreach ($facetList as $f) {
                $results->getParams()->setFacetPrefix(
                    $level . substr($f['value'], 1)
                );
                $subFacets = $this->getFacetsList($results, $facet, -1);
                $this->translateFacet($subFacets, $results, $facet);
                $subFacetList = $this->getSubFacetList($level, $subFacets, $results, $facet);
                $facetList = array_merge($facetList, $subFacetList);
            }
        }
        return $facetList;
    }

    /**
     * @param $results
     * @param $facet
     * @param $limit
     *
     * @return array
     */
    protected function getFacetsList($results, $facet, $limit): array
    {
        $facets = $results->getFullFieldFacets(
            [$facet],
            false,
            $limit,
            'count'
        );
        $facetList = [];
        if (!empty($facets[$facet]['data']['list'])) {
            $facetList = $facets[$facet]['data']['list'];
        }
        return $facetList;
    }

    /**
     * @param $facetList
     * @param $results
     * @param $facet
     *
     * @return void
     */
    protected function translateFacet(&$facetList, $results, $facet): void
    {
        if ($this->translatedSubFacets->get($facet)) {
            $translationContext = $this->translatedSubFacets->get($facet);
            foreach ($facetList as &$f) {
                $parts = $this->facetHelper->splitParts($f['value']);
                $displayText = $f["displayText"];
                // There is always an empty string at the end
                if (count($parts) > 2
                    && $parts[count($parts) - 2] === $displayText
                ) {
                    $f["displayText"] = $results->translate(
                        $translationContext . '::' . $displayText
                    );
                }
            }
        }
    }

    /**
     * @param $facet
     *
     * @return int
     */
    protected function getFacetLimit($facet): int
    {
        return $this->facetLimit->get($facet, -1);
    }

    /**
     * @param $facet
     *
     * @return int
     */
    protected function getLevels($facet): int
    {
        return $this->levels->get($facet) ? $this->levels->get($facet) : 0;
    }
}
