<?php
/**
 * SwissCollections: Factory.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordTab
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\RecordTab;

use Laminas\ServiceManager\ServiceManager;

/**
 * Factory to create record tabs.
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\RecordTab
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class Factory
{
    /**
     * Create a new cite tab.
     *
     * @param ServiceManager $sm an instance
     *
     * @return CiteTab
     */
    public static function getCite(ServiceManager $sm)
    {
        return new CiteTab();
    }

    /**
     * Create a new order tab.
     *
     * @param ServiceManager $sm an instance
     *
     * @return OrderTab
     */
    public static function getOrder(ServiceManager $sm)
    {
        return new OrderTab();
    }

    /**
     * Create a new export tab.
     *
     * @param ServiceManager $sm an instance
     *
     * @return ExportTab
     */
    public static function getExport(ServiceManager $sm)
    {
        return new ExportTab();
    }
}