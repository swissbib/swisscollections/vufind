<?php

/**
 * Solr aspect of the Search Multi-class (Results)
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  Search_Solr
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\VuFind\Search\Solr;

/**
 * Solr Search Parameters
 *
 * @category VuFind
 * @package  Search_Solr
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */
class Results extends \Swissbib\VuFind\Search\Solr\Results
{
    /**
     * Get complete facet counts for several index fields
     *
     * @param array  $facetfields  name of the Solr fields to return facets for
     * @param bool   $removeFilter Clear existing filters from selected fields (true)
     *                             or retain them (false)?
     * @param int    $limit        A limit for the number of facets returned, this
     *                             may be useful for very large amounts of facets that can break the JSON parse
     *                             method because of PHP out of memory exceptions (default = -1, no limit).
     * @param string $facetSort    A facet sort value to use (null to retain current)
     * @param int    $page         1 based. Offsets results by limit.
     * @param bool   $ored         Whether or not facet is an OR facet or not
     *
     * @return array list facet values for each index field with label and more bool
     */
    public function getPartialFieldFacets(
        $facetfields,
        $removeFilter = true,
        $limit = -1,
        $facetSort = null,
        $page = null,
        $ored = false
    ): array
    {
        $clone = clone $this;
        $params = $clone->getParams();

        // Manipulate facet settings temporarily:
        $params->resetFacetConfig();
        $params->setFacetLimit($limit);
        // Clear field-specific limits, as they can interfere with retrieval:
        $params->setFacetLimitByField([]);
        if (null !== $page && $limit != -1) {
            $offset = ($page - 1) * $limit;
            $params->setFacetOffset($offset);
            // Return limit plus one so we know there's another page
            $params->setFacetLimit($limit + 1);
        }
        if (null !== $facetSort) {
            $params->setFacetSort($facetSort);
        }
        foreach ($facetfields as $facetName) {
            $params->addFacet($facetName, null, $ored);

            // Clear existing filters for the selected field if necessary:
            if ($removeFilter) {
                $params->removeAllFilters($facetName);
            }
        }

        // Don't waste time on spellcheck:
        $params->getOptions()->spellcheckEnabled(false);

        // Don't fetch any records:
        $params->setLimit(0);

        // Disable highlighting:
        $params->getOptions()->disableHighlighting();

        // Disable sort:
        $params->setSort('', true);

        // Do search
        $result = $clone->getFacetList();

        // Reformat into a hash:
        foreach ($result as $key => $value) {
            // Detect next page and crop results if necessary
            $more = false;
            if (
                isset($page) && count($value['list']) > 0
                && count($value['list']) == $limit + 1
            ) {
                $more = true;
                array_pop($value['list']);
            }

            // special treatment of facet values, which are translated and
            // therefore not alphabetically sortable
            if ($facetSort === "index" && $this->shallManualSort($key)) {
                $this->sortFacetValues($value);
            }

            $result[$key] = ['more' => $more, 'data' => $value];
        }

        // Send back data:
        return $result;
    }

    /**
     * Check whether manual sorting is necessary.
     *
     * @param string $facetName a facet's name
     *
     * @return true if manual sorting is wanted
     */
    public function shallManualSort(string $facetName): bool
    {
        $translatedFacets
            = $this->facetsConfig->Advanced_Settings->translated_facets->toArray();
        // unfortunately no method for checking
        $translated = preg_grep("/" . $facetName . ":/", $translatedFacets);
        return $translated !== false && count($translated) === 1;
    }

    /**
     * Sort facet values.
     *
     * @param array $data Facet values with $data['list']["displayText"]
     */
    public function sortFacetValues(array &$data): void
    {
        usort(
            $data['list'],
            function ($x, $y) {
                return strcmp($x["displayText"], $y["displayText"]);
            }
        );
    }

    /**
     * Is facet sorted by index?
     *
     * @param string $facetName a facet's key
     *
     * @return true if facet is sorted by index
     */
    public function usesIndexSort(string $facetName): bool
    {
        $advSolrSortOpts = $this->facetsConfig->AvailableFacetSortOptions->Solr;
        if (!empty($advSolrSortOpts)) {
            $sortOpt = $advSolrSortOpts[$facetName];
            if (!empty($sortOpt)) {
                return str_starts_with($sortOpt, "index=");
            }
        }
        return false;
    }

    protected function buildFacetList(array $facetList, array $filter = null): array
    {
        // If there is no filter, we'll use all facets as the filter:
        if (null === $filter) {
            $filter = $this->getParams()->getFacetConfig();
        }

        // Start building the facet list:
        $result = [];

        // Loop through every field returned by the result set
        $translatedFacets = $this->getOptions()->getTranslatedFacets();
        $hierarchicalFacets
            = is_callable([$this->getOptions(), 'getHierarchicalFacets'])
            ? $this->getOptions()->getHierarchicalFacets()
            : [];
        foreach (array_keys($filter) as $field) {
            $data = $facetList[$field] ?? [];
            // Skip empty arrays:
            if (count($data) < 1) {
                continue;
            }
            // Initialize the settings for the current field
            $result[$field] = [
                'label' => $filter[$field],
                'list' => [],
            ];
            // Should we translate values for the current facet?
            $translate = in_array($field, $translatedFacets);
            $hierarchical = in_array($field, $hierarchicalFacets);
            $operator = $this->getParams()->getFacetOperator($field);
            // Loop through values:
            foreach ($data as $value => $count) {
                $displayText = $this->getParams()
                    ->getFacetValueRawDisplayText($field, $value);
                if ($hierarchical) {
                    if (!$this->hierarchicalFacetHelper) {
                        throw new \Exception(
                            get_class($this)
                            . ': hierarchical facet helper unavailable'
                        );
                    }
                    $displayText = $this->hierarchicalFacetHelper
                        ->formatDisplayText($displayText);
                }
                $translatedFacetValue = $this->getParams()->translateFacetValue($field, $displayText);
                $displayText = $translate
                    ? $this->getParams()->translateFacetValue($field, $displayText)
                    : $displayText;
                $isApplied = $this->facetIsApplied($field, $value, $translatedFacetValue);

                // Store the collected values:
                $result[$field]['list'][] = compact(
                    'value',
                    'displayText',
                    'count',
                    'operator',
                    'isApplied'
                );
            }
        }
        return $result;
    }

    private function facetIsApplied(string $field, string $value, string $translatedFacetValue): bool
    {
        return $this->getParams()->hasFilter("{$field}:" . $value)
            || $this->getParams()->hasFilter("~{$field}:" . $value)
            || $this->getParams()->hasFilter("{$field}:" . $translatedFacetValue)
            || $this->getParams()->hasFilter("~{$field}:" . $translatedFacetValue);
    }
}
