<?php

/**
 * Factory for Solr search results objects.
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  Search_Solr
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\VuFind\Search\Solr;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use VuFind\Config\PluginManager as PluginManagerAlias;
use VuFind\Record\Loader;
use VuFind\Search\Factory\UrlQueryHelperFactory;
use VuFind\Search\Params\PluginManager;
use VuFind\Search\Solr\HierarchicalFacetHelper as HierarchicalFacetHelperAlias;
use VuFind\Search\Solr\SpellingProcessor;
use VuFindSearch\Service;

/**
 * Factory for Solr search results objects.
 *
 * @category VuFind
 * @package  Search_Solr
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */
class ResultsFactory extends \VuFind\Search\Results\ResultsFactory
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container     Service manager
     * @param string             $requestedName Service being created
     * @param null|array         $options       Extra options (optional)
     *
     * @return object
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): object
    {
        $solr = $this->getSolrImpl($container, $requestedName, $options);
        $config = $container->get(PluginManagerAlias::class)
            ->get('config');
        $solr->setSpellingProcessor(
            new SpellingProcessor($config->Spelling ?? null)
        );
        $solr->setHierarchicalFacetHelper(
            $container->get(HierarchicalFacetHelperAlias::class)
        );
        return $solr;
    }

    /**
     * Get the name of a class for a given plugin name.
     *
     * modified from: VuFind\ServiceManager\AbstractPluginFactory::getClassName
     *
     * @param string $requestedName Name of service
     * @param string $defaultNamespace
     * @param string $classSuffix
     *
     * @return string               Fully qualified class name
     */
    protected function getClassName(string $requestedName, string $defaultNamespace, string $classSuffix = ''): string
    {
        // If we have a FQCN that refers to an existing class, return it as-is:
        if (str_contains($requestedName, '\\')
            && class_exists(
                $requestedName
            )
        ) {
            return $requestedName;
        }
        // First try the raw service name, then try a normalized version:
        $finalName = $defaultNamespace . '\\' . $requestedName
            . $classSuffix;
        if (!class_exists($finalName)) {
            $finalName = $defaultNamespace . '\\'
                . ucwords(strtolower($requestedName)) . $classSuffix;
        }
        return $finalName;
    }

    // from VuFind\Search\Results\ResultsFactory

    /**
     * Create a new solr result instance.
     *
     * @param ContainerInterface $container     the container
     * @param string             $requestedName the class for which to create an instance
     * @param array|null         $options       generic options
     *
     * @return mixed
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function getSolrImpl(
        ContainerInterface $container,
        string $requestedName,
        array $options = null
    ): mixed
    {
        // Replace trailing "Results" with "Params" to get the params service:
        $paramsService = preg_replace('/Results$/', 'Params', $requestedName);
        // fix namespace too
        $paramsService = preg_replace(
            '/^SwissCollections/', 'Swissbib', $paramsService
        );
        $params = $container->get(PluginManager::class)
            ->get($paramsService);
        $searchService = $container->get(Service::class);
        $recordLoader = $container->get(Loader::class);
        $class = $this->getClassName($requestedName, 'Swissbib\VuFind\Search');
        $results = new $class(
            $params,
            $searchService,
            $recordLoader,
            ...($options ?: [])
        );
        $results->setUrlQueryHelperFactory(
            $container->get(UrlQueryHelperFactory::class)
        );
        return $results;
    }
}
