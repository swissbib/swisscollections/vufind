<?php
/**
 * PluginFactory
 *
 * PHP version 7
 *
 * Copyright (C) project swissbib, University Library Basel, Switzerland
 * http://www.swissbib.org  / http://www.swissbib.ch / http://www.ub.unibas.ch
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category Swissbib_VuFind
 * @package  VuFind_Search_Results
 * @author   Guenter Hipler <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
namespace SwissCollections\VuFind\Search\Results;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Swissbib\VuFind\Search\Solr\Results;
use SwissCollections\VuFind\Search\Helper\ExtendedSolrFactoryHelper;
use VuFind\Search\Results\PluginFactory as VuFindResultsPluginFactory;

/**
 * Class PluginFactory.
 * This is a copy of the Swissbib file, with modified namespaces only.
 *
 * @category Swissbib_VuFind
 * @package  VuFind_Search_Results
 * @author   Guenter Hipler <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://vufind.org/wiki/vufind2:developer_manual Wiki
 */
class PluginFactory extends VuFindResultsPluginFactory
{
//    /**
//     * CanCreateServiceWithName
//     *
//     * @param ContainerInterface $container     ServiceContainer
//     * @param String             $requestedName Name of service
//     * @param array|null         $options       Options (unused)
//     *
//     * @return object
//     *
//     * @throws ContainerExceptionInterface
//     * @throws NotFoundExceptionInterface
//     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
//     */
//    public function canInvoke(ContainerInterface $container,
//        $requestedName, array $options = null
//    ) {
//        /**
//         * ExtendedSolrFactoryHelper
//         *
//         * @var ExtendedSolrFactoryHelper $extendedTargetHelper
//         */
//        $extendedTargetHelper
//            = $container->get('SwissCollections\ExtendedSolrFactoryHelper');
//
//        $this->defaultNamespace = $extendedTargetHelper
//            ->getNamespace($requestedName);
//
//        return parent($container, $requestedName, $options);
//    }

    /**
     * Create a service for the specified name.
     *
     * @param ContainerInterface $container     Container service
     * @param string             $requestedName Unfiltered name of service
     * @param array|null         $extras        Extra Params
     *
     * @return object
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container,
        $requestedName, array $extras = null
    ): object
    {
        /**
         * ExtendedSolrFactoryHelper
         *
         * @var ExtendedSolrFactoryHelper $extendedTargetHelper
         */
        $extendedTargetHelper
            = $container->get('SwissCollections\ExtendedSolrFactoryHelper');

        $this->defaultNamespace
            = $extendedTargetHelper->getNamespace($requestedName);

        /**
         * Swissbib specific Results type for Solr
         *
         * @var Results $sbSolrResults
         */
        $sbSolrResults
            = parent::__invoke($container, $requestedName, $extras);
        $facetConfigs = $container->get('VuFind\Config\PluginManager')
            ->get($sbSolrResults->getOptions()->getFacetsIni());

        //todo
        //perhaps not a really nice way to provide the config dependency
        //via Setter methods analyze the complete complex Facets
        //in VuFind 4 perhaps a easier way to implement our
        //query facets (MyLibraries) is possible
        $sbSolrResults->setFacetsConfig($facetConfigs);
        return $sbSolrResults;
    }
}
