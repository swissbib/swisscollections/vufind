<?php

/**
 * Search Results Object Factory Class
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category Swissbib_VuFind
 * @package  Search
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\VuFind\Search\Results;

use Laminas\ServiceManager\ServiceManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use SwissCollections\VuFind\Search\Solr\Results;
use VuFind\Search\Solr\HierarchicalFacetHelper;
use VuFind\Search\Solr\SpellingProcessor;

/**
 * Search Results Object Factory Class
 *
 * @category Swissbib_VuFind
 * @package  Search
 * @author   Siegfried Ballmann <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */
class Factory
{
    /**
     * Factory for Solr results object.
     *
     * @param ServiceManager $sm Service manager.
     *
     * @return Results
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getSolr(ServiceManager $sm): Results
    {
        $factory = new PluginFactory();

        /**
         * Create Service With Name Solr
         *
         * @var $solr Results
         */
        $solr = $factory($sm, 'Solr');

        $config = $sm->get('VuFind\Config\PluginManager')->get('config');
        $solr->setSpellingProcessor(
            new SpellingProcessor($config->Spelling ?? null)
        );
        $solr->setHierarchicalFacetHelper(
            $sm->get(HierarchicalFacetHelper::class)
        );

        return $solr;
    }
}
