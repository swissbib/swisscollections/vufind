<?php

namespace SwissCollections\VuFind\Search;

class Memory extends \VuFind\Search\Memory
{
    /**
     * Get a search by id
     *
     * @param int $id Search ID
     *
     * @return ?\VuFind\Search\Base\Results
     */
    public function getSearchBySearchId(int $searchId): ?\VuFind\Search\Base\Results
    {
        return parent::getSearchById($searchId);
    }
}
