<?php
/**
 * Citation
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * http://www.swissbib.org  / http://www.swissbib.ch / https://www.ub.unibas.ch
 *
 * Date: 9/12/13
 * Time: 11:46 AM
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category Swissbib_VuFind
 * @package  VuFind_View_Helper_Root
 * @author   Guenter Hipler <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://www.swissbib.org
 */

namespace SwissCollections\VuFind\View\Helper\Root;

use SwissCollections\RecordDriver\SolrMarc;
use VuFind\View\Helper\Root\Citation as VuFindCitation;

/**
 * Summon Search Options
 *
 * @category Swissbib_VuFind
 * @package  VuFind_View_Helper_Root
 * @author   Guenter Hipler <guenter.hipler@unibas.ch>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     http://www.swissbib.org  Main Page
 */
class Citation extends VuFindCitation
{
    /**
     * Invoke Citation.
     *
     * Format:
     * <pre>
     *   '245a' : '245b' / '245c'. - '264a', '264c'.
     *   '300a' : '300b' ; '300c'. - '340a'.
     *   Signature format of detail view.
     * </pre>
     *
     * @param SolrMarc $driver Record driver object.
     *
     * @return Citation
     *
     * @override
     */
    public function __invoke($driver): static
    {
        parent::__invoke($driver);

        $this->details['driver'] = $driver;

        $pubDates = $driver->tryMethod('getHumanReadablePublicationDates');
        $this->details['pubDate'] = $pubDates[0] ?? null;

        $this->details['marc245'] = $this->prepareMarcValue(245, $driver);
        $this->details['hasMarc245'] = !empty($this->details['marc245']['a'])
            || !empty($this->details['marc245']['b'])
            || !empty($this->details['marc245']['c']);

        $this->details['marc264'] = $this->prepareMarcValue(264, $driver);
        $this->details['hasMarc264'] = !empty($this->details['marc264']['a'])
            || !empty($this->details['marc264']['c']);

        $this->details['marc300'] = $this->prepareMarcValue(300, $driver);
        $this->details['hasMarc300'] = !empty($this->details['marc300']['a'])
            || !empty($this->details['marc300']['b'])
            || !empty($this->details['marc300']['c']);

        $this->details['marc340'] = $this->prepareMarcValue(340, $driver);
        $this->details['hasMarc340'] = !empty($this->details['marc340']['a']);

        return $this;
    }

    /**
     * Get the first value of a given marc index or null.
     *
     * @param int      $index  the marc index
     * @param SolrMarc $driver Record driver object.
     */
    protected function prepareMarcValue(int $index, SolrMarc $driver): ?array
    {
        $marcList = $driver->getMarcFieldsRawMap($index, null, []);
        if (count($marcList) > 0) {
            $marc = $marcList[0];
            $marc1 = [];
            foreach ($marc as $subfield => $values) {
                $marc1[$subfield] = implode(", ", $values);
            }
            return $marc1;
        }
        return null;
    }

    /**
     * Get Custom citation.
     *
     * This function assigns all the necessary variables and then returns a Custom
     * citation.
     **
     *
     * @return string
     */
    public function getCitationSCShort(): string
    {
        // Behave differently for books vs. journals:
        $partial = $this->getView()->plugin('partial');
        $viewData = $this->details;

        if (empty($this->details['journal'])) {
            return $partial('Citation/scshort.phtml', $viewData);
        } else {
            return $partial('Citation/scshort-article.phtml', $viewData);
        }
    }

    /**
     * Get publisher information (place: name) for inclusion in a citation.
     * Shared by APA and MLA functionality.
     *
     * @param bool $includePubPlace Ignored! Should we include the place of publication?
     *
     * @return string
     */
    protected function getPublisher($includePubPlace = true): string
    {
        if (isset($this->details['pubName'])
            && !empty($this->details['pubName'])
        ) {
            return $this->details['pubName'];
        } else {
            return '';
        }
    }

    /**
     * GetYear
     *
     * @return string
     *
     * @override
     */
    protected function getYear(): string
    {
        return !empty($this->details['pubDate']) ? $this->details['pubDate']
            : '';
    }
}
