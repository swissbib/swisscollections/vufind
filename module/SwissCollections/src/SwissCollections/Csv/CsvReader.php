<?php
/**
 * SwissCollections: CsvReader.php
 *
 * PHP version 7
 *
 * Copyright (C)  University Library Basel, Switzerland
 * https://swisscollections.ch / https://www.ub.unibas.ch
 *
 * Date: 1/12/20
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category SwissCollections_VuFind
 * @package  SwissCollections\Csv
 * @author   Christoph Böhm <cbo@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://www.swisscollections.ch Website
 */

namespace SwissCollections\Csv;

use ParseCsv\Csv;
use VuFind\Config\YamlReader;

/**
 * VuFind CSV configuration reader. This class extends vufind's YAML reader.
 * Note that every CSV file can contain a very first line which contains
 * the parent declaration. Example:
 * <pre>
 * ,parent_csv=local/swisscollections/production/config/vufind/detail-fields.csv
 * </pre>
 * The first character (";" resp. "," in examples above) may vary as the
 * content's separator. Omit this line for no parent.
 *
 * Merging of parent csv files is not supported. A csv file either contains no
 * parent and the full content or a parent line only.
 *
 * @category VuFind
 * @package  View_Helpers
 * @author   Siegfried Ballmann  <sba@outermedia.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class CsvReader extends YamlReader
{
    /**
     * Process a CSV file (and its parent, if necessary).
     *
     * @param string $file          CSV file to load (will evaluate to empty array
     *                              if file does not exist).
     * @param string $defaultParent Is ignored. There is no concept for default
     *                              files in CSVs, because there is no merge.
     *
     * @return array
     */
    protected function parseYaml($file, $defaultParent = null): array
    {
        list($result, $parent) = $this->detectParent(
            $this->loadFile($file),
            null
        );
        $csv = null;
        // use parent's content, but don't merge
        if (!empty($parent)) {
            // First try parent as absolute path, then as relative:
            $parentPath = file_exists($parent)
                ? $parent
                : dirname($file) . '/' . $parent;
            if (!file_exists($parentPath)) {
                error_log(
                    'Cannot find parent file: ' . $parent
                );
                return [];
            }
            $csv = $this->parseYaml($parent);
        } else {
            $csv = (new Csv($result))->data;
        }
        if (empty($csv)) {
            $csv = [];
        }
        return $csv;
    }

    /**
     * Read csv file content to string. Returns false in failure case.
     *
     * @param string $name the csv file name
     *
     * @return string
     */
    protected function loadFile($name)
    {
        $result = "";
        if (!empty($name) && file_exists($name)) {
            $result = file_get_contents($name);
            if ($result === false) {
                $result = "";
            }
        }
        return $result;
    }

    /**
     * Detects first parent line and returns the parent and the csv without
     * the parent line.
     *
     * @param string      $csv           the csv code
     * @param string|null $defaultParent optional default parent
     *
     * @return array array of 'csv' without optional parent line and 'parent'
     */
    protected function detectParent($csv, $defaultParent): array
    {
        (($pos = strpos($csv, "\n")) !== false) || ($pos = strpos($csv, "\r"));
        // perhaps file with parent file only and no newline
        if ($pos === false && strlen($csv) <= PHP_MAXPATHLEN) {
            $pos = strlen($csv);
        }
        $parentCsv = $defaultParent;
        if ($pos !== false) {
            $firstline = substr($csv, 0, $pos);
            if (preg_match(
                '/^.parent_csv=(.+)\\r?\\n?/i',
                $firstline,
                $sepMatch
            )
            ) {
                $parentCsv = $sepMatch[1];
                $contentPos = $pos + 1;
                if (strlen($csv) > $contentPos) {
                    $csv = substr($csv, $contentPos);
                } else {
                    $csv = "";
                }
            }
        }
        return [$csv, $parentCsv];
    }
}
