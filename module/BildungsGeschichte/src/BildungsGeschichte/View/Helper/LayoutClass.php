<?php

namespace BildungsGeschichte\View\Helper;

/**
 * Main layout CSS classes provider.
 *
 * @category BildungsGeschichte_VuFind
 * @package  BildungsGeschichte\View\Helper
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class LayoutClass extends \VuFind\View\Helper\Bootstrap3\LayoutClass
{
    /**
     * Invoke LayoutClass
     *
     * @param String $class Class
     *
     * @return String
     */
    public function __invoke($class): string
    {
        $classString = '';

        switch ($class) {
            case 'mainbody':
                $classString .= $this->sidebarOnLeft ?
                    'col-md-8 col-md-push-3 col-table-fix-md right-md-search-results-gutter' :
                    'col-md-7 col-table-fix-md left-md-search-results-gutter';
                break;
            case 'sidebar':
                $classString .= $this->sidebarOnLeft
                    ? 'sidebar col-sm-5 col-md-pull-9 col-table-fix-md hidden-print'
                    : 'sidebar col-md-5 col-table-fix-md hidden-print';
        }

        $htmlLayoutClass = $this->getView()->htmlLayoutClass;

        return isset($htmlLayoutClass) ? $classString . ' ' .
            $htmlLayoutClass : $classString;
    }
}
