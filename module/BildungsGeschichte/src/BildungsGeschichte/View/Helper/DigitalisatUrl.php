<?php

namespace BildungsGeschichte\View\Helper;

use Laminas\Form\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use SwissCollections\Formatter\FieldFormatterData;
use SwissCollections\Formatter\FieldFormatterRegistry;
use SwissCollections\Formatter\SubfieldFormatterRegistry;
use SwissCollections\RecordDriver\FieldRenderContext;
use SwissCollections\RecordDriver\SolrMarc;
use SwissCollections\RenderConfig\RenderConfig;
use SwissCollections\Trait\OrderFieldsTrait;

class DigitalisatUrl extends AbstractHelper
{
    use OrderFieldsTrait;
    /**
     * Get the "Digitalisat" url as the formatters do.
     *
     * @param PhpRenderer  $phpRenderer  a non empty instance
     * @param SolrMarc     $record       a non empty instance
     * @param RenderConfig $renderConfig a non empty instance
     *
     * @return bool|string
     */
    public function __invoke(PhpRenderer $phpRenderer, SolrMarc $record, RenderConfig $renderConfig): bool|string
    {
        $thumbSrc = "";
        /**
         * @var SubfieldFormatterRegistry $subfieldFormatterRegistry
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $subfieldFormatterRegistry = $phpRenderer->subfieldFormatterRegistry();
        /**
         * @var FieldFormatterRegistry $fieldFormatterRegistry
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $fieldFormatterRegistry = $phpRenderer->fieldFormatterRegistry();

        $fieldContext = new FieldRenderContext(
            $fieldFormatterRegistry,
            $record,
            $subfieldFormatterRegistry,
            $phpRenderer,
            'page.detail.field.label',
            $renderConfig->logger,
            0
        );

        $rcg = $renderConfig->get("Digitalisatinformationen");
        $rcg->resetHiddenSubfields();
        if (!$rcg->isEmpty($record, $renderConfig)) {
            $digitalisatFields = $rcg->getField("Digitalisat");

            // only first non empty entry is used for link
            foreach ($digitalisatFields as $renderElem) {
                if (!$renderElem->isEmpty($record, $renderConfig)) {
                    $formatterConfig = $renderElem->getFormatterConfig();
                    $formatterName = $formatterConfig->getFormatterName();
                    // we only know how formatter 'link' extracts the url
                    if ($formatterName === 'link') {
                        $urlSubfield = $formatterConfig->optionalConfigEntry(
                            "urlSubfield", ""
                        );
                        if (!empty($urlSubfield)) {
                            $fields = $record->getFieldValues(
                                $renderElem, $renderConfig
                            );
                            if (!empty($fields)) {
                                if($orderConfig = $renderElem->getFormatterConfig()->optionalConfigEntry('order', false)) {
                                    $fields = $this->sortFields($fields, $orderConfig);
                                }
                                foreach ($fields as $field) {
                                    $values = $renderElem->getAllRenderData(
                                        $field, $fieldContext
                                    );
                                    $thumbSrc
                                        = FieldFormatterData::valueBySubfield(
                                        $urlSubfield, $values
                                    );
                                    if (!empty($thumbSrc)) {
                                        break 2;
                                    }
                                }
                            }
                        }
                    } else {
                        $renderConfig->logger->err(
                            "** Expected formatter 'link' for 'Digitalisat'. Can't extract thumbnail url."
                        );
                    }
                }
            }

        }

        return $thumbSrc;
    }
}
