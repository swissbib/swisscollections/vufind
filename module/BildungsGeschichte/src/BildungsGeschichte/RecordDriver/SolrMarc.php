<?php

namespace BildungsGeschichte\RecordDriver;

use SwissCollections\RecordDriver\SolrMarc as SwissColSolrMarc;
use SwissCollections\RenderConfig\AbstractRenderConfigEntry;
use VuFind\I18n\TranslatableString;

class SolrMarc extends SwissColSolrMarc
{
    public function getTopDocumentType(): array|null
    {
        $types = [];

        if (isset($this->fields['collection_bgs_hierarchy_str_mv']) && is_array($this->fields['collection_bgs_hierarchy_str_mv'])) {
            $types = array_map(function (TranslatableString $item) {
                return $item->getDisplayString();
            }, $this->fields['collection_bgs_hierarchy_str_mv']);
        }

        return $types;
    }

    public function getDefaultDocType(): string
    {
        return 'Monographien';
    }

    public static function primaryAuthorsProvider(
        AbstractRenderConfigEntry $renderElem,
        self|SwissColSolrMarc     $solrMarc
    ): array
    {
        return [$solrMarc->getPrimaryAuthorList($renderElem)];
    }


    public function getPrimaryAuthorList(AbstractRenderConfigEntry $renderElem): array
    {
        $formatterConfig = $renderElem->getFormatterConfig();
        $entries = $formatterConfig->optionalConfigEntry('entries', false);

        $authors = $this->getMarcFields($renderElem->marcIndex);
        $authorSubFields = $this->getMarcSubFieldMap($renderElem->marcIndex, $this->personFieldMap);

        $allAuthors = [];
        foreach ($authors as $author) {
            if ($entries) {
                $hideFieldRelatorCode = $entries[0]['hideFieldRelatorCode'];
                if (!array_key_exists('relator_code', $authorSubFields) ||
                    (array_key_exists('relator_code', $author) && !in_array($author['relator_code'], $hideFieldRelatorCode))) {
                    $allAuthors[] = $author['subfields'][0]['data'];
                } else if (array_key_exists('relator_code', $authorSubFields) && !in_array($authorSubFields['relator_code'], $hideFieldRelatorCode)) {
                    $allAuthors[] = $author['subfields'][0]['data'];
                }
            }
        }

        return [$this->createAuthorsString($allAuthors)];
    }

    private function createAuthorsString(array $data): string
    {
        $name = implode('; ', $data);

        return trim($name);
    }

    public static function documentMediumProvider(
        AbstractRenderConfigEntry $renderElem,
        self                      $solrMarc
    ): array
    {

        $translateFile = $renderElem->getFormatterConfig()->optionalConfigEntry('translateFile', 'formats');
        $field = $renderElem->getFormatterConfig()->optionalConfigEntry('searchField', 'navMedia_bgs_str_mv');
        $medium = $solrMarc->fields[$field] ?? [];

        $docType[] = array_map(function ($item) use ($solrMarc, $translateFile) {
            return $solrMarc->getTranslator()
                ->translate($item, $translateFile);
        }, $medium);

        return $docType;
    }

}
