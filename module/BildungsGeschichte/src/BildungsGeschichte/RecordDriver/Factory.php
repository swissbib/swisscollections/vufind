<?php

namespace BildungsGeschichte\RecordDriver;

use Laminas\ServiceManager\ServiceManager;
use SwissCollections\RecordDriver\Factory as SwissColFactory;

class Factory extends SwissColFactory
{
    public static function getSolrMarcRecordDriver(ServiceManager $sm): SolrMarc
    {
        $driver = new SolrMarc(
            $sm->get('VuFind\Config\PluginManager')->get('config'),
            null,
            $sm->get('VuFind\Config\PluginManager')->get('searches'),
            $sm->get('Swissbib\HoldingsHelper'),
            $sm->get('Swissbib\RecordDriver\SolrDefaultAdapter'),
            $sm->get('Swissbib\Availability'),
            $sm->get('VuFind\Config\PluginManager')->get('Holdings')->AlephNetworks
                ->toArray(),
            $logger = $sm->get('VuFind\Log\Logger')
        );
        $driver->attachILS(
            $sm->get('VuFind\ILS\Connection'),
            $sm->get('VuFind\ILS\Logic\Holds'),
            $sm->get('VuFind\ILS\Logic\TitleHolds')
        );
        return $driver;
    }
}
