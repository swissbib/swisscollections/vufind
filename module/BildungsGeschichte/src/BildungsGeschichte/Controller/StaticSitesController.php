<?php

namespace BildungsGeschichte\Controller;

use Laminas\View\Model\ViewModel;
use Swissbib\Controller\BaseController;

class StaticSitesController extends BaseController
{

    public function getStaticPageAction()
    {
        $page = ltrim($this->getRequest()->getRequestUri(), '/');
        $viewModel = $this->setStaticPageModel();
        $templatePath = $this->getTemplatePath($page);
        $viewModel->setTemplate($templatePath);
        return $viewModel;
    }

    private function getTemplatePath($pageType)
    {
        return "static-pages/{$pageType}/{$this->layout()->userLang}/page";
    }

    private function setStaticPageModel(): ViewModel
    {
        $data = [];
        $viewModel = new ViewModel($data);
        $this->layout()->breadcrumbs = false;
        $this->layout()->breadcrumbCssClasses = "breadcrumbWrapper--hidden";
        return $viewModel;
    }
}