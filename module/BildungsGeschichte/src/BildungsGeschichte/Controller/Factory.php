<?php

namespace BildungsGeschichte\Controller;

use Laminas\ServiceManager\ServiceManager;
use VuFind\Controller\AbstractBaseFactory;

class Factory extends AbstractBaseFactory
{
    public static function getStaticPageController(ServiceManager $sm)
    {
        return new StaticSitesController($sm);
    }
}