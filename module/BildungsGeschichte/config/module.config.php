<?php

return [
    'controllers' => [
        'factories' => [
            'static' => 'BildungsGeschichte\Controller\Factory::getStaticPageController',
        ]
    ],
    'view_helpers' => [
        'factories' => [
            'BildungsGeschichte\View\Helper\LayoutClass' => 'Laminas\ServiceManager\Factory\InvokableFactory',
        ],
        'aliases' => [
            'layoutClass' => 'BildungsGeschichte\View\Helper\LayoutClass',
        ],
    ],
    'router' => [
        'routes' => [
            'about' => [
                'type' => 'Laminas\Router\Http\Segment',
                'options' => [
                    'route' => '/about',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ]
                ],
            ],
            'association' => [
                'type' => 'Laminas\\Router\\Http\\Literal',
                'options' => [
                    'route' => '/association',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ],
                ],
            ],
            'collections' => [
                'type' => 'Laminas\\Router\\Http\\Literal',
                'options' => [
                    'route' => '/collections',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ],
                ],
            ],
            'searchInfo' => [
                'type' => 'Laminas\\Router\\Http\\Literal',
                'options' => [
                    'route' => '/searchInfo',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ],
                ],
            ],
            'contact' => [
                'type' => 'Laminas\\Router\\Http\\Literal',
                'options' => [
                    'route' => '/contact',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ],
                ],
            ],
            'rights' => [
                'type' => 'Laminas\\Router\\Http\\Literal',
                'options' => [
                    'route' => '/rights',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ],
                ],
            ],
            'privacy' => [
                'type' => 'Laminas\\Router\\Http\\Literal',
                'options' => [
                    'route' => '/privacy',
                    'defaults' => [
                        'controller' => 'static',
                        'action' => 'getStaticPage',
                    ],
                ],
            ],
        ],
    ],
    'vufind' => [
        'plugin_managers' => [
            'recorddriver' => [
                'factories' => [
                    'BildungsGeschichte\RecordDriver\SolrMarc' => 'BildungsGeschichte\RecordDriver\Factory::getSolrMarcRecordDriver',
                ],
                'aliases' => [
                    'solrmarc' => 'BildungsGeschichte\RecordDriver\SolrMarc',
                ],
            ],
        ],
    ]
];