# seachspecs.yaml for swissbib
# see extensive documentation in head of file /config/vufind/searchspecs.yaml
# ###########################################################################

# GlobalExtraParams allows extra parameters to be (conditionally) added to the Solr
# search request.
#
# Example showcasing all the available conditions:
#
GlobalExtraParams:
  - param: fq
    value: 'view_str_mv:bgs'
    conditions:
      - SearchTypeIn:
          - AllFields

# These searches use Dismax when possible:
AllFields:
  DismaxHandler: edismax
  DismaxFields:
    - title_short^1000                    # main title, i.e. 245 $a
    - title_alt^200
    - title^200
    - title_sub^200
    - title_old^200
    - title_new^200
    - author^750                          # all entries main name forms
    - author_additional^100               # all entries additional fields
    - author_additional_gnd_txt_mv^100    # variants from GND
    - title_additional_gnd_txt_mv^100
    - publplace_additional_gnd_txt_mv^100
    - series^200
    - topic^500                           # all verbal topics, i.e. 6xx
    - related_gnd_txt_mv^500              # GND related terms 5xx
    - addfields_txt_mv^50                 # additional fields
    - publplace_txt_mv^25                 # publication country and place, i.e. 752ad
    - publplace_dsv11_txt_mv^25
    - fulltext                            # imported fulltext, i.e. TOC/Abstracts
    - callnumber_txt_mv^50                # callnumbers (works only when quoted) with low boost
    - ctrlnum^1000
    - series_ctrlnum_isn_mv
    - publishDate
    - isbn
    - cancisbn_isn_mv
    - variant_isbn_isn_mv
    - issn
    - incoissn_isn_mv
    - ismn_isn_mv
    - cancismn_isn_mv
    - doi_isn_mv
    - date_txt_mv
    - urn_isn_mv
    - other_id_isn_mv
    - publisher_id_isn_mv
    - coordinate_strl_mv
    - from_gnd_isn_mv
    - about_gnd_isn_mv
    - classif_local
    - localcode_txt_mv              # local codes splitted at spaces
    - id_txt
    - callno_bgs_strl_mv
    - format_additional_bgs_txt_mv
  #add the new fields to AllFieldsAutosuggest as well
  DismaxParams:
    - [pf, 'title_short^1000 callnumber_txt_mv^100 localcode_txt_mv^10']
    - [ps, '2']
    - [bf, 'recip(abs(ms(NOW/DAY,freshness)),3.16e-10,100,100)']
  FilterQuery: 'view_str_mv:bgs'

#same as AllFileds, but without pf for title_short
#reason is that with pf a query like algebra OR algebra* will boost
#results that have twice algebra in the title
AllFieldsAutosuggest:
  DismaxHandler: edismax
  DismaxFields:
    - title_short^1000                    # main title, i.e. 245 $a
    - title_alt^200
    - title^200
    - title_sub^200
    - title_old^200
    - title_new^200
    - author^750                          # all entries main name forms
    - author_additional^100               # all entries additional fields
    - author_additional_gnd_txt_mv^100    # variants from GND
    - title_additional_gnd_txt_mv^100
    - publplace_additional_gnd_txt_mv^100
    - series^50
    - topic^100                           # all verbal topics, i.e. 6xx
    - related_gnd_txt_mv^50
    - addfields_txt_mv^50                 # additional fields
    - publplace_txt_mv^25                 # publication country and place, i.e. 752ad
    - publplace_dsv11_txt_mv^25
    - fulltext                            # imported fulltext, i.e. TOC/Abstracts
    - callnumber_txt_mv^50                # callnumbers (works only when quoted) with low boost
    - ctrlnum^1000
    - series_ctrlnum_isn_mv
    - publishDate
    - isbn
    - cancisbn_isn_mv
    - variant_isbn_isn_mv
    - issn
    - incoissn_isn_mv
    - ismn_isn_mv
    - cancismn_isn_mv
    - doi_isn_mv
    - date_txt_mv
    - urn_isn_mv
    - other_id_isn_mv
    - publisher_id_isn_mv
    - coordinate_strl_mv
    - from_gnd_isn_mv
    - about_gnd_isn_mv
    - classif_local
    - localcode_txt_mv              # local codes splitted at spaces
    - id_txt
    - format_additional_bgs_txt_mv
  DismaxParams:
    - [pf, 'callnumber_txt_mv^100 localcode_txt_mv^10']
    - [ps, '2']
    - [bf, 'recip(abs(ms(NOW/DAY,freshness)),3.16e-10,100,100)']
  FilterQuery: 'view_str_mv:bgs'

Author:
  DismaxHandler: edismax
  DismaxFields:
    - author^100
    - author_additional^20
    - author_additional_dsv11_txt_mv^20
    - author_additional_gnd_txt_mv^20
  DismaxParams:
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

#author gnd, or rero id
AuthorById:
  DismaxHandler: edismax
  DismaxFields:
    - from_gnd_isn_mv
    - from_rero_isn_mv
  DismaxParams:
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

Fulltext:
  DismaxHandler: edismax
  DismaxFields:
    - fulltext
  DismaxParams:
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

# config identical to AllFields, apart from field fulltext
AllFieldsNoFulltext:
  DismaxHandler: edismax
  DismaxFields:
    - title_short^1000                    # main title, i.e. 245 $a
    - title_alt^200
    - title^200
    - title_sub^200
    - title_old^200
    - title_new^200
    - author^750                          # all entries main name forms
    - author_additional^100               # all entries additional fields
    - author_additional_gnd_txt_mv^100    # variants from GND
    - title_additional_gnd_txt_mv^100
    - publplace_additional_gnd_txt_mv^100
    - series^200
    - topic^500                           # all verbal topics, i.e. 6xx
    - related_gnd_txt_mv^500              # GND related terms 5xx
    - addfields_txt_mv^50                 # additional fields
    - publplace_txt_mv^25                 # publication country and place, i.e. 752ad
    - publplace_dsv11_txt_mv^25
    - callnumber_txt_mv^50                # callnumbers (works only when quoted) with low boost
    - ctrlnum^1000
    - series_ctrlnum_isn_mv
    - publishDate
    - isbn
    - cancisbn_isn_mv
    - variant_isbn_isn_mv
    - issn
    - incoissn_isn_mv
    - ismn_isn_mv
    - cancismn_isn_mv
    - doi_isn_mv
    - date_txt_mv
    - urn_isn_mv
    - other_id_isn_mv
    - publisher_id_isn_mv
    - coordinate_strl_mv
    - from_gnd_isn_mv
    - about_gnd_isn_mv
    - classif_local
    - localcode_txt_mv              # local codes splitted at spaces
    - id_txt
    - callno_bgs_strl_mv
  #add the new fields to AllFieldsAutosuggest as well
  DismaxParams:
    - [pf, 'title_short^1000 callnumber_txt_mv^100 localcode_txt_mv^10']
    - [ps, '2']
    - [bf, 'recip(abs(ms(NOW/DAY,freshness)),3.16e-10,100,100)']
  FilterQuery: 'view_str_mv:bgs'

ISN:
  DismaxHandler: edismax
  DismaxFields:
    - isbn
    - variant_isbn_isn_mv
    - cancisbn_isn_mv
    - issn
    - incoissn_isn_mv
    - ismn_isn_mv
    - doi_isn_mv
    - urn_isn_mv
    - callno_bgs_strl_mv
  DismaxParams:
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

Identifier:
  QueryFields:
    isbn:
      - [onephrase, ~]
    variant_isbn_isn_mv:
      - [onephrase, ~]
    cancisbn_isn_mv:
      - [onephrase, ~]
    issn:
      - [onephrase, ~]
    incoissn_isn_mv:
      - [onephrase, ~]
    ismn_isn_mv:
      - [onephrase, ~]
    doi_isn_mv:
      - [onephrase, ~]
    urn_isn_mv:
      - [onephrase, ~]
    callno_bgs_strl_mv:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

Subject:
  DismaxHandler: edismax
  DismaxFields:
    - topic^150
    - related_gnd_txt_mv
  DismaxParams:
    - [bf, 'recip(abs(ms(NOW/DAY,freshness)),3.16e-10,100,100)']
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

Subject_OR:
  DismaxHandler: dismax
  DismaxFields:
    - topic^150
  QueryFields:
    topic:
      - [onephrase, 350]
      - [or, 500]
  DismaxParams:
    - [mm, '50%']
  FilterQuery: 'view_str_mv:bgs'

JournalTitle:
  DismaxFields:
    - title_short^1000
    - title_alt^200
    - title_sub^200
    - title_old^200
    - title_new^200
  QueryFields:
    title_short:
      - [onephrase, 10000]
    title_sub:
      - [onephrase, 200]
    title_alt:
      - [onephrase, 200]
    title_old:
      - [onephrase, 200]
    title_new:
      - [onephrase, 200]
  FilterQuery: 'format_str_mv:(CR0305* OR CR0306* OR CR0307*) AND view_str_mv:bgs'
  DismaxParams:
    - [pf, 'title_short^100000']
    - [ps, '2']
    - [mm, '100%']

FRBR:
  QueryFields:
    groupid_isn_mv:
      - [onephrase, 500]
  FilterQuery: 'view_str_mv:bgs'

WLC:
  DismaxFields:
    - localcode
  QueryFields:
    localcode:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

WNEL:
  DismaxFields:
    - itemnote_isn_mv
  QueryFields:
    itemnote_isn_mv:
      - [or, ~]
  FilterQuery: 'view_str_mv:bgs'

Itemnote:
  DismaxFields:
    - itemnote_txt_mv
  QueryFields:
    itemnote_isn_mv:
      - [or, ~]
  FilterQuery: 'view_str_mv:bgs'

WFC:
  DismaxFields:
    - classif_912_BSBE
  QueryFields:
    classif_912:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

WLD:
  DismaxFields:
    - origcountry_isn_mv
  QueryFields:
    origcountry_isn_mv:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

Title:
  DismaxHandler: edismax
  DismaxFields:
    - title_short^1000            # main title, i.e. 245 $a
    - title_alt^200
    - title_additional_dsv11_txt_mv^200
    - title_additional_gnd_txt_mv^200
    - title_sub^200
    - title_old^200
    - title_new^200
    - series^100
  DismaxParams:
    - [bf, 'recip(abs(ms(NOW/DAY,freshness)),3.16e-10,100,100)']
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

Series:
  DismaxHandler: edismax
  DismaxFields:
    - series^100
  DismaxParams:
    - [bf, 'recip(abs(ms(NOW/DAY,freshness)),3.16e-10,100,100)']
    - [mm, '100%']
  FilterQuery: 'view_str_mv:bgs'

# These are advanced searches that never use Dismax:
id:
  QueryFields:
    id:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

# Fields for exact matches originating from alphabetic browse
ids:
  QueryFields:
    id:
      - [or, ~]
  FilterQuery: 'view_str_mv:bgs'

CallNumber:
  # We use two similar munges here -- one for exact matches, which will get
  # a very high boost factor, and one for left-anchored wildcard searches,
  # which will return a larger number of hits at a lower boost.
  # adapted for swissbib usage
  CustomMunge:
    callnumber_exact:
      # Strip whitespace and quotes:
      - [preg_replace, '/[ "]/', '']
      # Escape colons (unescape first to avoid double-escapes):
      - [preg_replace, '/(\\:)/', ':']
      - [preg_replace, '/:/', '\:']
      # Strip pre-existing trailing asterisks:
      - [preg_replace, '/\*+$/', '']
    callnumber_fuzzy:
      # Strip whitespace and quotes:
      - [preg_replace, '/[ "]/', '']
      # Escape colons (unescape first to avoid double-escapes):
      - [preg_replace, '/(\\:)/', ':']
      - [preg_replace, '/:/', '\:']
      # Escape parentheses to be able to search call numbers with
      # parenthesis :
      - [preg_replace, '/\(/', '\\(']
      - [preg_replace, '/\)/', '\\)']
      # Strip pre-existing trailing asterisks, then add a new one:
      - [preg_replace, '/\*+$/', '']
      - [append, "*"]
  QueryFields:
    callnumber:
      - [callnumber_exact, 1000]
      - [callnumber_fuzzy, ~]
  FilterQuery: 'view_str_mv:bgs'

year:
  DismaxFields:
    - publishDate^100
  QueryFields:
    publishDate:
      - [and, 100]
      - [or, ~]
  FilterQuery: 'view_str_mv:bgs'

language:
  QueryFields:
    language:
      - [and, ~]
  FilterQuery: 'view_str_mv:bgs'

HostID:
  DismaxFields:
    - hostotherID_str_mv
    - hostissn_isn_mv
    - hostisbn_isn_mv
  QueryFields:
    hostotherID_str_mv:
      - [and, 100]
      - [or, ~]
    hostissn_isn_mv:
      - [and, 100]
      - [or, ~]
    hostisbn_isn_mv:
      - [and, 100]
      - [or, ~]
  FilterQuery: 'view_str_mv:bgs'

oclc_num:
  CustomMunge:
    oclc_num:
      - [preg_replace, '/[^0-9]/', '']
      # trim leading zeroes:
      - [preg_replace, '/^0*/', '']
  QueryFields:
    oclc_num:
      - [oclc_num, ~]
  FilterQuery: 'view_str_mv:bgs'

ctrlnum:
  CustomMunge:
    ctrlnum:
      - [preg_replace, '/[\(\)]/', '']
  QueryFields:
    ctrlnum:
      - [ctrlnum, ~]
  FilterQuery: 'view_str_mv:bgs'

GNDS:
  DismaxFields:
    - subpers_gnd
    - subtitle_gnd
    - subtime_gnd
    - subtop_gnd
    - subgeo_gnd
    - subform_gnd
  QueryFields:
    subpers_gnd:
      - [and, 100]
      - [or, ~]
    subtitle_gnd:
      - [and, 100]
      - [or, ~]
    subtime_gnd:
      - [and, 100]
      - [or, ~]
    subtop_gnd:
      - [and, 100]
      - [or, ~]
    subgeo_gnd:
      - [and, 100]
      - [or, ~]
    subform_gnd:
      - [and, 100]
      - [or, ~]
  FilterQuery: 'view_str_mv:bgs'

PUBL:
  DismaxFields:
    - publisher_txt_mv
  QueryFields:
    publisher_txt_mv:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

PUBPL:
  DismaxFields:
    - pubplace_txt_mv
  QueryFields:
    pubplace_txt_mv:
      - [onephrase, ~]
  FilterQuery: 'view_str_mv:bgs'

PublPlace:
  DismaxFields:
    - publplace_txt_mv
    - publplace_additional_gnd_txt_mv
  QueryFields:
    publplace_txt_mv:
      - [and, ~]
    publplace_additional_gnd_txt_mv:
      - [and, ~]
  FilterQuery: 'view_str_mv:bgs'

Form:
  DismaxFields:
    - formImage_txt_mv
    - sublocal_uzb-ZG_txt_mv
    - musicGenre_txt_mv
  QueryFields:
    formImage_txt_mv:
      - [and, ~]
    sublocal_uzb-ZG_txt_mv:
      - [and, ~]
    musicGenre_txt_mv:
      - [and, ~]
  FilterQuery: 'view_str_mv:bgs'

Terms:
  DismaxFields:
    - terms_txt_mv
  QueryFields:
    terms_txt_mv:
      - [and, ~]
  FilterQuery: 'view_str_mv:bgs'