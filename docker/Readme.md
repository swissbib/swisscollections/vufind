# Installation
## Docker

You need to have access to solr test cluster which is used by default.

Available under https://solrtest.swisscollections.unibas.ch/solr/#/ from inside unibas network


In root directory : 
```
docker-compose -f docker/development/docker-compose-custom-node.yml build
touch local/private_config_values/private_config_base_content.conf
touch local/private_config_values/private_config_swisscollections_test_database.conf
touch local/private_config_values/private_config_swisscollections_prod_database.conf
touch local/private_config_values/private_config_classic_switchapitest.conf
touch local/private_config_values/private_config_base_libadmin_swisscollections.conf
export DOCKER_HOST_IP=$(ip route | grep docker0 | awk '{print $9}')
# optional: convert less to css (but css is checked in)
# but it is the needed the first time to have the node modules installed
docker-compose -f docker/docker-compose.yml up -d node
./in-node.sh npm install
./in-node.sh npm run build
docker-compose -f docker/docker-compose.yml down
# only once: composer install to build vendor/
docker-compose -f docker/development/docker-compose-php.yml up
# start the complete local dev stack
docker-compose -f docker/docker-compose.yml up -d 
docker exec -it swisscollections-php /bin/sh /usr/local/vufind/cli/sync.sh
```

See VuFind on http://127.0.0.1

Don't forget to add your user to docker group (https://docs.docker.com/engine/install/linux-postinstall/)


## Run locally (experience from Lionel)

For an unknown reason it is impossible to run cisco vpn, make a tunnel to solr server and run docker in parallel. There are some problems in the network configuration

Solution :

First you need to add you IP address (from home) in the nginx proxy (sb-upo1) so that you can access solrtest.swissbib.ch without VPN

Then

This needs to be done only once (or when there are js updates or similar) :
```
docker-compose -f docker/development/docker-compose-custom-node.yml build
touch local/private_config_values/private_config_base_content.conf
touch local/private_config_values/private_config_swisscollections_test_database.conf
touch local/private_config_values/private_config_base_libadmin_swisscollections.conf
export DOCKER_HOST_IP=$(ip route | grep docker0 | awk '{print $9}')
# optional: convert less to css (but css is checked in)
docker-compose -f docker/docker-compose.yml up -d node
./in-node.sh npm install
./in-node.sh npm run build
docker-compose -f docker/docker-compose.yml down
# only once: composer install to build vendor/
docker-compose -f docker/development/docker-compose-php.yml up
```

This needs to be run everytime you start the stack :

1. stop cisco vpn
1. stop apache `sudo service apache2 stop`

```
# start the complete local dev stack
export DOCKER_HOST_IP=$(ip route | grep docker0 | awk '{print $9}')
docker-compose -f docker/docker-compose.yml up
```
## Library names (and labels)
If you want library names : first copy the file `private_config_base_libadmin_swisscollections.conf` with the credentials from the private Repository `deployRep` to the `local/private_config_values/` folder.

Then rebuild the container (see below). And then : 

```
docker exec -it swisscollections-php mkdir -p  data/cache/
docker exec -it swisscollections-php /bin/sh /usr/local/vufind/cli/sync.sh
```

## Rebuild image

To rebuild the php image you need to remove the image first with `docker image rm -f docker_swisscollections-php`

Rebuild image if needed :
```
docker-compose -f docker/docker-compose.yml build
```

## In case of Problems
Helfen könnten hier folgende libraries:

```
sudo apt-get install build-essential libsqlite3-dev sqlite3 bzip2 libbz2-dev zlib1g-dev libssl-dev openssl libgdbm-dev libgdbm-compat-dev liblzma-dev libreadline-dev libncursesw5-dev libffi-dev uuid-dev
```

(vor allem zlib & co.)


## Xdebug configuration

Phpstorm listens to the docker container.

Important points : 
- in docker-xdebug.ini, there is a port xdebug.remote_port=XXXX and a key xdebug.idekey=INTELLIJ
- in phpstorm settings Languages & Frameworks -> PHP -> Debug : check that the port is the same
- in the same settings, check that it is similar to the screenshot (xdebug-settings.png)

Create a run configuration of type PHP Remote Debug and a corresponding server. See screenshot (php-remote-debugger.png).

Use the same key as in docker-xdebug.ini and map the local file (in my case /usr/local/vufind/httpd) to the container files (/usr/local/vufind)

After that, launch the container (via docker-compose in the console) and click on the "bug" button near the disabled "run" button near php-remote-debug configurations. Make sure that phpstorm doesn't listen to PHP Debug Connections (telephone icon).

It might be needed to rebuild the image if this doesn't work

### Xdebug : some notes from March 2023

See https://www.jetbrains.com/help/phpstorm/configuring-xdebug.html

Much of what is above is obsolete.

Main points : 
- configuer in phpstorm a php cli on docker (see screenshot php-cli-interpreter.png)
- adjust path mappings in the corresponding PHP server (see screenshot server-path-mappings.png)
- click the small bug near the run button to listen to external connections




## Customization 
- Theme: `themes/swisscollections`
- Module: `module/SwissCollections`
- Configuration: `local/swisscollections/swisscollections`

Configuration is based on `local/swisscollections/development`
