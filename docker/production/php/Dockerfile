# Multi-stage Docker Image Build
# First composer dependencies
# 2nd node dependencies
# Finally VuFind build

#
# PHP Dependencies Installation
#
FROM composer:2 AS composer

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

#
# NodeNpm
#
FROM node:17 AS nodenpm

RUN mkdir -p /app/public

COPY package.json package-lock.json tsconfig.json webpack.config.js Gruntfile.js /app/
COPY themes /app/themes

WORKDIR /app

RUN npm -v

RUN npm install
RUN npm run test
RUN npm run build

#
# VuFind
#
FROM php:8.1-fpm-alpine
#Use the default production configuration
#RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
VOLUME [ "/usr/local/vufind" ]
COPY --from=composer /app/vendor/ /usr/local/vufind/vendor/
ENV VUFIND_CACHE_DIR=/var/cache/vufind \
  VUFIND_APPLICATION_PATH=/usr/local/vufind

RUN mkdir -p ${VUFIND_CACHE_DIR} \
  && chown www-data:www-data ${VUFIND_APPLICATION_PATH} ${VUFIND_CACHE_DIR} -R \
  && apk add --no-cache --update --virtual .build-deps icu-dev freetype-dev libjpeg-turbo-dev libpng-dev libxslt-dev zlib-dev bzip2-dev libzip-dev oniguruma-dev autoconf \
  && apk add --no-cache --update libpng libjpeg-turbo freetype icu-libs libxslt libzip libxml2-dev \
  && apk add --no-cache --update libcurl curl-dev \
  && docker-php-ext-configure gd --with-freetype --with-jpeg \
  && docker-php-ext-install -j$(nproc) mbstring intl mysqli pdo_mysql pdo gd xsl xml curl zip bz2  \
  && apk del .build-deps \
  && echo 'memory_limit = 512M' >> ${PHP_INI_DIR}/conf.d/docker-php-memlimit.ini \
  && echo 'pm = static' >> /usr/local/etc/php-fpm.d/zz-docker.conf \
  && echo 'pm.max_children = 20' >> /usr/local/etc/php-fpm.d/zz-docker.conf \
  && echo 'pm.start_servers = 5' >> /usr/local/etc/php-fpm.d/zz-docker.conf \
  && echo 'pm.min_spare_servers = 5' >> /usr/local/etc/php-fpm.d/zz-docker.conf \
  && echo 'pm.max_spare_servers = 10' >> /usr/local/etc/php-fpm.d/zz-docker.conf;

COPY ./data ${VUFIND_APPLICATION_PATH}/data
COPY ./cli ${VUFIND_APPLICATION_PATH}/cli
COPY ./local ${VUFIND_APPLICATION_PATH}/local
COPY ./config ${VUFIND_APPLICATION_PATH}/config
COPY ./languages ${VUFIND_APPLICATION_PATH}/languages
COPY ./module ${VUFIND_APPLICATION_PATH}/module
COPY ./public ${VUFIND_APPLICATION_PATH}/public
COPY ./util ${VUFIND_APPLICATION_PATH}/util
COPY --from=nodenpm /app/themes ${VUFIND_APPLICATION_PATH}/themes
COPY --from=nodenpm /app/node_modules ${VUFIND_APPLICATION_PATH}/node_modules

# Required by Swissbib Logger. Rather log to stdout!
COPY --chown=www-data:www-data ./log ${VUFIND_APPLICATION_PATH}/log

WORKDIR ${VUFIND_APPLICATION_PATH}

CMD [ "php-fpm" ]
