<?php

use Laminas\ServiceManager\Factory\InvokableFactory;
use SwissCollections\Formatter\FieldFormatterRegistry;
use SwissCollections\Formatter\FieldGroupFormatterRegistry;
use SwissCollections\Formatter\SubfieldFormatterRegistry;
use SwissCollections\RecordDriver\DocTypeCategories;
use SwissCollections\RecordDriver\ResultListViewFieldInfo;
use SwissCollections\RenderConfig\RenderConfig;
use SwissCollections\View\Helper\DigitalisatUrl;
use SwissCollections\View\Helper\FieldValue;
use SwissCollections\View\Helper\GenerateEodLink;
use SwissCollections\View\Helper\HeaderTitle;
use SwissCollections\View\Helper\HideSearchInput;
use SwissCollections\View\Helper\Orderable;
use SwissCollections\View\Helper\Root\AlphaBrowse;
use SwissCollections\View\Helper\Root\AlphaBrowseFactory;
use SwissCollections\View\Helper\Root\Browse;
use SwissCollections\View\Helper\Root\BrowseFactory;
use SwissCollections\View\Helper\Root\DocTypeCategoriesFactory;
use SwissCollections\View\Helper\Root\FieldFormatterRegistryFactory;
use SwissCollections\View\Helper\Root\FieldGroupFormatterRegistryFactory;
use SwissCollections\View\Helper\Root\RenderConfigFactory;
use SwissCollections\View\Helper\Root\ResultListViewConfigFactory;
use SwissCollections\View\Helper\Root\SubfieldFormatterRegistryFactory;
use SwissCollections\View\Helper\Record;
use SwissCollections\View\Helper\SwisscoveryOrderLink;
use SwissCollections\View\Helper\ValidRecordForEod;
use VuFind\View\Helper\Root\RecordFactory;

return [
    'extends' => 'sbvfrdsingle',
    'js' => [
        'export-results.js',
        'swisscollections/detail-helper.js',
        'swisscollections/results-helper.js',
    ],
    'less' => [
        'active' => false,
        'compiled.less'
    ],
    'helpers' => [
        'factories' => [
            Browse::class => BrowseFactory::class,
            AlphaBrowse::class => AlphaBrowseFactory::class,
            ResultListViewFieldInfo::class => ResultListViewConfigFactory::class,
            SubfieldFormatterRegistry::class => SubfieldFormatterRegistryFactory::class,
            FieldFormatterRegistry::class => FieldFormatterRegistryFactory::class,
            FieldGroupFormatterRegistry::class => FieldGroupFormatterRegistryFactory::class,
            DocTypeCategories::class => DocTypeCategoriesFactory::class,
            RenderConfig::class => RenderConfigFactory::class,
            Record::class => RecordFactory::class,
            HeaderTitle::class => InvokableFactory::class,
            'SwissCollections\View\Helper\LayoutClass' => 'Laminas\ServiceManager\Factory\InvokableFactory',
            'SwissCollections\VuFind\View\Helper\Root\Citation' => 'VuFind\View\Helper\Root\CitationFactory',
            FieldValue::class => InvokableFactory::class,
            GenerateEodLink::class => InvokableFactory::class,
            ValidRecordForEod::class => InvokableFactory::class,
            SwisscoveryOrderLink::class => InvokableFactory::class,
            HideSearchInput::class => InvokableFactory::class,
            Orderable::class => InvokableFactory::class,
            DigitalisatUrl::class => InvokableFactory::class,
        ],
        'aliases' => [
            'browse' => Browse::class,
            'alphabrowse' => AlphaBrowse::class,
            'resultListViewConfig' => ResultListViewFieldInfo::class,
            'subfieldFormatterRegistry' => SubfieldFormatterRegistry::class,
            'fieldFormatterRegistry' => FieldFormatterRegistry::class,
            'fieldGroupFormatterRegistry' => FieldGroupFormatterRegistry::class,
            'docTypeCategories' => DocTypeCategories::class,
            'renderConfig' => RenderConfig::class,
            'record' => Record::class,
            'headTitle' => HeaderTitle::class,
            'layoutClass' => 'SwissCollections\View\Helper\LayoutClass',
            'citation' => 'SwissCollections\VuFind\View\Helper\Root\Citation',
            'fieldValue' => FieldValue::class,
            'generateEodLink' => GenerateEodLink::class,
            'validRecordForEod' => ValidRecordForEod::class,
            'swisscoveryOrderLink' => SwisscoveryOrderLink::class,
            'hideSearchInput' => HideSearchInput::class,
            'showOrderTab' => Orderable::class,
            'digitalisatUrl' => DigitalisatUrl::class,
        ]
    ]
];
