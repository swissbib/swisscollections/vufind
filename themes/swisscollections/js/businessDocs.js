/*global VuFind */

/*exported initBusinessDocsTree */
function buildBusinessDocsNodes (data) {
  var json = [];

  $(data).each(function businessDocsNodesEach () {
    var url = this.href;
    var $html = $('<div/>');
    var $item = $('<span/>')
      .addClass('main text' + (this.isApplied ? ' applied' : ''))
      .attr('role', 'menuitem')
      .attr('title', this.displayText)
      .text(this.displayText);
    $html.append($item);

    var children = null;
    if (typeof this.children !== 'undefined' && this.children.length > 0) {
      children = buildBusinessDocsNodes(this.children);
    } else {
      children = [];
    }
    json.push({
      'text': $html.html(),
      'children': children,
      'applied': this.isApplied,
      'state': {
        'opened': this.hasAppliedChildren
      },
      'li_attr': this.isApplied ? { 'class': 'active' } : {},
      'a_attr': {
        'href': VuFind.path + '/Search/Results' + url.replace(/&amp;/g, '&'),
      },
      'data': {
        'value': this.value,
      }
    });
  });

  return json;
}

function buildBusinessDocsTree (treeNode, businessDocsData) {
  // Enable keyboard navigation also when a screen reader is active
  // treeNode.bind('select_node.jstree', VuFind.businessDocs.showLoadingOverlay);

  var results = buildBusinessDocsNodes(businessDocsData);
  treeNode.find('.fa-spinner').parent().remove();
  treeNode.jstree({
    'core': {
      'data': results,
      'check_callback': true,
    },
    "state" : { "key" : "businessDocs" },
    "plugins" : [ "state", "sort" ],
    "sort": sortTree
  });
}

function initBusinessDocsTree (treeNode) {
  var loaded = treeNode.data('loaded');
  if (loaded) {
    return;
  }
  treeNode.data('loaded', true);

  var source = treeNode.data('source');
  var businessDocs = treeNode.data('businessdocs');
  var operator = treeNode.data('operator');
  var sort = treeNode.data('sort');
  var query = window.location.href.split('?')[1];
  query = query ? '?' + query : '';

  treeNode.prepend('<div><Hierarchyi class="fa fa-spinner fa-spin" aria-hidden="true"></Hierarchyi><div>');

  $.getJSON(VuFind.path + '/AJAX/JSON' + query,
    {
      method: "getBusinessDocsData",
      source: source,
      facetName: businessDocs,
      facetSort: sort,
      facetOperator: operator
    },
    function getBusinessDocsData (response/*, textStatus*/) {
      buildBusinessDocsTree(treeNode, response.data.businessDocs);
      initSelectionBehaviour();
    }
  );
}