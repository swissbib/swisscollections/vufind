if (!window.swisscollections) {
    window.swisscollections = {};
}

swisscollections.initOverflowDetailPage = function (fieldContainer) {
    const openHeight = swisscollections.openHeightDetailPage(fieldContainer);
    const currentHeight = fieldContainer.getBoundingClientRect().height;
    const hierarchyTree = $(fieldContainer).find("#hierarchyTree");
    const isHierarchy = hierarchyTree.length > 0;
    const shallOpenHierarchy = new URLSearchParams(window.location.search).get("openHierarchy") === "true";
    const userWatchesTree = isHierarchy && shallOpenHierarchy;

    // if too high, collapse tree if tree isn't focused
    if ((openHeight - currentHeight) >= 30) {
        if ( userWatchesTree ) {
            swisscollections.openClosedOverflowInDetailPage(fieldContainer);
        } else {
            const overflowOverlayList = fieldContainer.querySelectorAll(".overflow-overlay");
            // for groups use the very last one (fields have only 1)
            const overflowOverlay = overflowOverlayList[overflowOverlayList.length - 1];
            overflowOverlay.classList.add("overflow-overlay--show");
        }
    } else {
        fieldContainer.classList.remove("folder-candidate");
    }
};

swisscollections.showOpenOnOverflowInDetailPage = function () {
    var fieldContainers = document.querySelectorAll('.detail-content .folder-candidate');
    var i;

    // remove nested folder candidates ...
    const subFieldContainers = [];
    for (i = 0; i < fieldContainers.length; i++) {
        const subFolderCandidates = fieldContainers[i].querySelectorAll('.folder-candidate');
        if (subFolderCandidates.length > 0) {
            for (var j = 0; j < subFolderCandidates.length; j++) {
                if (subFolderCandidates[j] !== fieldContainers[i]) {
                    subFieldContainers.push(subFolderCandidates[j]);
                }
            }
        }
    }
    for (i = 0; i < subFieldContainers.length; i++) {
        subFieldContainers[i].classList.remove("folder-candidate");
    }

    fieldContainers = document.querySelectorAll('.detail-content .folder-candidate');
    for (i = 0; i < fieldContainers.length; i++) {
        // asynchronously loaded foldable components init on their own
        if (!fieldContainers[i].classList.contains("folder-candidate--lazy")) {
            swisscollections.initOverflowDetailPage(fieldContainers[i]);
        }
    }
};

swisscollections.openClosedOverflowInDetailPage = function (fieldContainer) {
    swisscollections.openClosedOverflowInDetailPageImpl(fieldContainer);
    swisscollections.setMaximizeState();
};

swisscollections.openHeightDetailPage = function (fieldContainer) {
    const foldableContainer = fieldContainer.querySelector(".foldable");
    const foldableContainerHeight = foldableContainer.getBoundingClientRect().height;
    var paddingBottom = 0;
    const paddingBottomStyle = window.getComputedStyle(fieldContainer).paddingBottom;
    if (paddingBottomStyle) {
        paddingBottom = parseInt(paddingBottomStyle, 10);
    }
    return foldableContainerHeight + paddingBottom;
};

swisscollections.closedHeightDetailPage = function (button) {
    const overflowElement = button.parentElement.previousElementSibling;
    if (overflowElement.classList.contains('overflow-overlay')) {
        // group
        const paddingBottom = 0;
        const groupContainer = overflowElement.previousElementSibling;
        const bbox = groupContainer.firstElementChild.getBoundingClientRect();
        return {overflowElement: groupContainer, height: (bbox.height + paddingBottom)};
    } else {
        // field
        const paddingBottom = parseInt(window.getComputedStyle(overflowElement)['padding-bottom']);
        const fieldContainer = overflowElement.querySelector('.overflow-overlay').previousElementSibling;
        const bbox = fieldContainer.getBoundingClientRect();
        return {overflowElement: overflowElement, height: (bbox.height + paddingBottom)};
    }
};

swisscollections.openClosedOverflowInDetailPageImpl = function (fieldContainer) {
    const folderContainer = fieldContainer.querySelector(".folder");
    const overflowOverlayList = fieldContainer.querySelectorAll(".overflow-overlay");
    // for groups use the very last one (fields have only 1)
    const overflowOverlay = overflowOverlayList[overflowOverlayList.length - 1];
    const collapseValuesList = fieldContainer.querySelectorAll(".collapse-values");
    // for groups use the very last one (fields have only 1)
    const collapseValues = collapseValuesList[collapseValuesList.length - 1];
    const openHeight = swisscollections.openHeightDetailPage(fieldContainer);

    fieldContainer.classList.add("folder-candidate--open");
    folderContainer.addEventListener("transitionend", function () {
        folderContainer.style.maxHeight = "none";
    }, {once: true});
    folderContainer.style.maxHeight = openHeight + "px";

    collapseValues.classList.add("collapse-values--show");
    overflowOverlay.classList.remove("overflow-overlay--show");
};

swisscollections.closeOpenOverflowInDetailPage = function (fieldContainer) {
    swisscollections.closeOpenOverflowInDetailPageImpl(fieldContainer);
    swisscollections.setMaximizeState();
    // compare two occurrences of "transition: max-height 1s;" in css!
    setTimeout(function () {
        swisscollections.highlightSidebar(document.querySelector(".sidebar"));
    }, 1100);
};

swisscollections.closeOpenOverflowInDetailPageImpl = function (fieldContainer) {
    const folderContainer = fieldContainer.querySelector(".folder");
    const overflowOverlayList = fieldContainer.querySelectorAll(".overflow-overlay");
    // for groups use the very last one (fields have only 1)
    const overflowOverlay = overflowOverlayList[overflowOverlayList.length - 1];
    const collapseValuesList = fieldContainer.querySelectorAll(".collapse-values");
    // for groups use the very last one (fields have only 1)
    const collapseValues = collapseValuesList[collapseValuesList.length - 1];
    const openHeight = swisscollections.openHeightDetailPage(fieldContainer);

    fieldContainer.classList.remove("folder-candidate--open");

    // no transition for max-height:none to concrete value
    // so can't use "transitionend" event listener
    folderContainer.style.maxHeight = openHeight + "px";
    setTimeout(function () {
        folderContainer.style = "";
    }, 100);

    collapseValues.classList.remove("collapse-values--show");
    overflowOverlay.classList.add("overflow-overlay--show");
};

swisscollections.toggleAllFields = function () {
    var closedFieldContainers = document.querySelectorAll('.detail-content .folder-candidate:not(.folder-candidate--open)');
    
    if (closedFieldContainers.length > 0) {
        // found closed field overlays, open all
        for (var i = 0; i < closedFieldContainers.length; i++) {
            swisscollections.openClosedOverflowInDetailPageImpl(closedFieldContainers[i]);
        }
        swisscollections.setMaximizeState();
    } else {
        // found no closed overlays, try to close open ones
        swisscollections.closeAllFields();
    }
};

swisscollections.closeAllFields = function () {
    var openFieldContainers = document.querySelectorAll('.detail-content .folder-candidate--open');
    if (openFieldContainers.length > 0) {
        for (var i = 0; i < openFieldContainers.length; i++) {
            swisscollections.closeOpenOverflowInDetailPageImpl(openFieldContainers[i]);
        }
        swisscollections.setMaximizeState();
        // no adjustment of highlighted group in sidebar needed, because
        // the toggle button is at the top which doesn't influence the highlighting
    }
};

swisscollections.setMaximizeState = function () {
    const maximizeTab = document.getElementById("maximize");
    if (maximizeTab) {
        var state = "open";
        const hasOpen = document.querySelector('.detail-content .folder-candidate--open');
        const hasClosed = document.querySelector('.detail-content .folder-candidate:not(.folder-candidate--open)');
        if (!hasClosed) {
            if (hasOpen) {
                state = "close";
            } else {
                state = "none";
            }
        }
        var text;
        switch (state) {
            case "open":
                text = VuFind.translate('page.detail.tabs.maximize_fields');
                break;
            case "close":
                text = VuFind.translate('page.detail.tabs.minimize_fields');
                break;
            default:
        }
        if (text) {
            maximizeTab.firstElementChild.innerText = text;
        } else {
            maximizeTab.style.display = "none";
        }
    }
};

swisscollections.positionSidebar = function (sidebar, contentGrid) {
    const sidebarBbox = sidebar.getBoundingClientRect();
    const classes = sidebar.parentElement.classList;
    if (sidebarBbox.height <= window.innerHeight) {
        // window high enough to show sidebar completely
        const contentGridBbox = contentGrid.getBoundingClientRect();
        // start checks close to the end of the scroll area
        if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight - window.innerHeight) {
            if (contentGridBbox.bottom <= sidebarBbox.height) {
                classes.remove("sidebar-container--sticky");
                classes.add("sidebar--bottom");
            } else {
                classes.remove("sidebar--bottom");
                classes.add("sidebar-container--sticky");
            }
        } else {
            classes.remove("sidebar--bottom");
            classes.add("sidebar-container--sticky");
        }
    } else {
        // window not high enough to show sidebar completely
        classes.remove("sidebar-container--sticky");
        classes.remove("sidebar--bottom");
    }
};

swisscollections.sidebarHighlight = null;

swisscollections.highlightSidebar = function (sidebar) {
    const groups = document.querySelectorAll(".detail-content .field-group");
    var newGroup;
    var g;
    for (var i = 0; i < groups.length; i++) {
        g = groups.item(i);
        if (g.getBoundingClientRect().bottom >= 0) {
            newGroup = g;
            break;
        }
    }
    if (newGroup !== swisscollections.sidebarHighlight) {
        if (swisscollections.sidebarHighlight) {
            // remove highlight
            const sidebarEntry = sidebar.querySelector('*[data-gid="'
                + swisscollections.sidebarHighlight.dataset.gid + '"]');
            sidebarEntry.classList.remove("anchor--highlighted");
        }
        if (newGroup) {
            // add highlight to newGroup
            const sidebarEntry = sidebar.querySelector('*[data-gid="' + newGroup.dataset.gid + '"]');
            sidebarEntry.classList.add("anchor--highlighted");
        }
        swisscollections.sidebarHighlight = newGroup;
    }
};

swisscollections.setWindowSizeHandler = function () {
    const sucheinstiege = document.querySelector('.detail-content .field-group-Sucheinstiege');
    var horizBar;
    if (sucheinstiege) {
        horizBar = sucheinstiege.querySelector('.horiz-bar');
    }
    const sidebar = document.querySelector(".sidebar");
    const contentGrid = document.querySelector(".detail-content-grid");

    // adjust horizontal grey bar on window resize changes
    if (sucheinstiege && horizBar && sidebar && contentGrid) {
        window.onresize = function () {
            const bbox = sucheinstiege.getBoundingClientRect();
            horizBar.style.left = (-bbox.left) + "px";
            horizBar.style.right = (-bbox.right) + "px";
            swisscollections.positionSidebar(sidebar, contentGrid);
            // max-height is set to open fields, which may change if device orientation changes
            swisscollections.closeAllFields();
            swisscollections.highlightSidebar(sidebar);
        };
    }

    if (sidebar && contentGrid) {
        window.addEventListener("scroll", function () {
            swisscollections.positionSidebar(sidebar, contentGrid);
            swisscollections.highlightSidebar(sidebar);
        });
        swisscollections.highlightSidebar(sidebar);
    }
};

// "sticky" js solution
swisscollections.controlSidebarPosition = function () {
    const sidebar = document.querySelector(".sidebar-container");
    swisscollections.sidebarInitialY = sidebar.getBoundingClientRect().top;

    window.addEventListener("scroll", function () {
        const scrollY = window.scrollY;
        if (scrollY >= swisscollections.sidebarInitialY) {
            sidebar.style.top = (scrollY - swisscollections.sidebarInitialY) + "px";
        } else {
            sidebar.style.top = "0px";
        }
    });
};

swisscollections.tabContentHeight = function (tab) {
    var margin = 0;
    const firstTabElementStyle = window.getComputedStyle(tab.firstElementChild);
    if (firstTabElementStyle.marginTop) {
        margin = parseInt(firstTabElementStyle.marginTop, 10);
    }
    return tab.getBoundingClientRect().height + margin;
};

swisscollections.showTab = function (tabName) {
    document.getElementById(tabName).parentElement.classList.add("active");
    var tab = document.getElementById(tabName + "-tab");
    tab.style.display = "block";
    tab.parentElement.addEventListener("transitionend", function () {
        tab.parentElement.style.maxHeight = "none";
    }, {once: true});
    const height = swisscollections.tabContentHeight(tab);
    tab.parentElement.style.maxHeight = height + "px";
};

swisscollections.closeTab = function (tabName, callback) {
    document.getElementById(tabName).parentElement.classList.remove("active");
    const tab = document.getElementById(tabName + "-tab");
    const height = swisscollections.tabContentHeight(tab);
    tab.parentElement.style.maxHeight = height + "px";
    setTimeout(function () {
        tab.parentElement.addEventListener("transitionend", function () {
                tab.style.display = "none";
                if (callback) {
                    callback();
                }
            }, {once: true}
        );
        tab.parentElement.style.maxHeight = "0px";
    }, 100);
};

swisscollections.openTab = function (tabName) {
    const tabButtonContainer = document.querySelector('.recordTabs');
    const visibleTabButton = tabButtonContainer.querySelector('.active > .nav-tab');
    if (visibleTabButton) {
        if (visibleTabButton.id !== tabName) {
            swisscollections.closeTab(visibleTabButton.id, function () {
                swisscollections.showTab(tabName);
            });
        } else {
            swisscollections.closeTab(tabName);
        }
    } else {
        swisscollections.showTab(tabName);
    }
};

$(document).ready(function () {
    swisscollections.showOpenOnOverflowInDetailPage();
    // swisscollections.controlSidebarPosition();
    swisscollections.setWindowSizeHandler();
    swisscollections.setMaximizeState();
});