if (!window.swisscollections) {
    window.swisscollections = {};
}

swisscollections.toggleFacetsInResultsView = function () {
    const openClass = "sidebar-open";
    const sidebar = document.querySelector(".sidebar");
    const isOpen = sidebar.classList.contains("sidebar-visible");
    // iPhone shows horizontal scrollbar although overflow-x is hidden
    // so handle display ("sidebar-visible")
    if (!isOpen) {
        sidebar.classList.toggle("sidebar-visible");
        setTimeout(function () {
            sidebar.classList.toggle(openClass);
            document.querySelector(".modal-overlay").classList.toggle(openClass);
            document.body.classList.toggle("overlayed");
        }, 200);
    } else {
        sidebar.classList.toggle(openClass);
        document.querySelector(".modal-overlay").classList.toggle(openClass);
        document.body.classList.toggle("overlayed");
        setTimeout(function () {
            sidebar.classList.toggle("sidebar-visible");
        }, 1000);
    }
};

swisscollections.showOpenOnOverflowInResultPage = function () {
    var fieldContainers = document.querySelectorAll('.resultView .folder-candidate');
    for (var i = 0; i < fieldContainers.length; i++) {
        // asynchronously loaded foldable components init on their own
        if (!fieldContainers[i].classList.contains("folder-candidate--lazy")) {
            swisscollections.initOverflowDetailPage(fieldContainers[i]);
        }
    }
};

$(document).ready(function () {
    swisscollections.showOpenOnOverflowInResultPage();
});