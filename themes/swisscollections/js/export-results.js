/*global VuFind */
function getCookie(name) {
  const value = `; ${ document.cookie }`;
  const parts = value.split(`; ${ name }=`);
  if (parts.length === 2) {
    return parts.pop().split(';').shift();
  }
  return false;
}

function toggleDisableExportButton(flag) {
  const exportBtn = document.getElementById('export_btn');
  if (flag) {
    exportBtn.disabled = true;
    exportBtn.setAttribute('title', VuFind.translate('export_btn_tooltip_disabled'));
  } else {
    exportBtn.disabled = false;
    exportBtn.setAttribute('title', '');
  }
}

function setRemoveUrlQuery(isSelected, urlString, paramName, paramValue) {
  const url = new URL(urlString);
  if (url.searchParams && isSelected) {
    url.searchParams.set(paramName, paramValue);
  } else {
    url.searchParams.delete(paramName);
  }
  return url;
}

function setWindowUrlQuery(isSelected, paramName, paramValue = 'true') {
  const url = setRemoveUrlQuery(isSelected, window.location.href, paramName, paramValue);
  window.history.replaceState(null, null, url);
}

function toggleExportResults(exportResults, shouldHide) {
  exportResults.forEach(result => {
    result.classList.toggle('hidden', shouldHide);
  });
}

function setTagRefQuery(aTags, isSelected, paramName, paramValue = 'true') {
  aTags.forEach(link => {
    const url = setRemoveUrlQuery(isSelected, link.href, paramName, paramValue);
    link.href = url.toString();
  });
}

function getRadioButtonValue(radioGroup) {
  const exportSelectionRadio = document.querySelector('input[name="' + radioGroup + '"]:checked');
  return exportSelectionRadio ? exportSelectionRadio.value : null;
}

function getSelectedIds() {
  const cookieData = getCookie('vufind_cart');
  if (cookieData) {
    const pairs = cookieData.split('%09');
    const ids = pairs.map(pair => pair.split('ASolr|')[1]);
    return 'q%3Did%3A' + ids.join('%2BOR%2Bid%3A');
  } else {
    console.log('Cookie not found');
  }
}

function setDownloadUrl(exportResultsBtn, paramName, paramValue) {
  let downloadPath = window.location.origin + exportResultsBtn.getAttribute('href');
  let downloadUrl = setRemoveUrlQuery(true, downloadPath, paramName, paramValue);
  let newUrl = downloadUrl.pathname + downloadUrl.search;
  exportResultsBtn.setAttribute('href', newUrl);
}

function getExportData(e) {
  e.preventDefault();
  let export_button = e.target;
  export_button.disabled = true;

  const exportResultsBtn = document.getElementById('export_results');
  const exportTypeValue = getRadioButtonValue('export_type');
  const exportFileValue = getRadioButtonValue('export_selection');
  const selectedData = exportFileValue === 'selection' ? getSelectedIds() : '';

  if (exportTypeValue && (exportFileValue === 'selection' && selectedData) || (exportFileValue === 'all')) {
    if (selectedData) {
      setDownloadUrl(exportResultsBtn, 'export-data', selectedData);
    }
    exportResultsBtn.click();
  } else {
    console.error('export_type and export_selection and ids of results values are required.');
  }
  export_button.disabled = false;
}

function removeListenerSubmitButton() {
  const exportResultsBtn = document.getElementsByClassName('export-results__button-submit');
  // eslint-disable-next-line func-names
  exportResultsBtn.item(0).removeEventListener('click', getExportData, true);
}

function setListenerSubmitButton() {
  const exportResultsBtn = document.getElementsByClassName('export-results__button-submit');
  // eslint-disable-next-line func-names
  exportResultsBtn.item(0).addEventListener('click', getExportData, true);
}

function setFormActionUrl(openExportForm, paramName, paramValue) {
  let actionUrl = window.location.origin + openExportForm.getAttribute('action');
  let url = setRemoveUrlQuery(true, actionUrl, paramName, paramValue);
  openExportForm.setAttribute('action', url.pathname + url.search);
}

function setLightBoxShow(openExportForm, exportDataPacketRadio, exportCSVRadio) {
  let actionUrl = window.location.origin + openExportForm.getAttribute('action');
  const url = new URL(actionUrl);
  const recordTotal = parseInt(url.searchParams.get('record_total'));

  if (exportDataPacketRadio && recordTotal >= 500 || exportCSVRadio && recordTotal >= 5000) {
    removeListenerSubmitButton();
  } else {
    setListenerSubmitButton();
  }
}

function checkExportBtnShouldDisable() {
  if (!getCookie('vufind_cart')) {
    toggleDisableExportButton(true);
  } else {
    toggleDisableExportButton(false);
  }
}

// eslint-disable-next-line func-names
document.addEventListener('DOMContentLoaded', function () {

  // eslint-disable-next-line func-names
  $(document).on('click', '.cart-add', function() {
    checkExportBtnShouldDisable();
  });

  // eslint-disable-next-line func-names
  $(document).on('click', '.cart-remove', function() {
    checkExportBtnShouldDisable();
  });

  // eslint-disable-next-line func-names
  $('#modal').on('shown.bs.modal', function () {
    const layout = document.getElementById('modal').querySelector('.col-md-8');
    layout.setAttribute('class', 'col-md-12');
  });

  function extractedExportFormElements() {
    const exportCSVRadio = document.getElementById('export_csv');
    const exportDataPacketRadio = document.getElementById('export_packet');
    const openExportFormBtn = document.getElementById('export-results-form');
    const openExportForm = document.getElementById('export-results-form-submit');
    const exportResultsBtn = document.getElementById('export_results');
    const exportAllRadio = document.getElementById('export_all');
    const exportMarkedRadio = document.getElementById('export_marked');
    const prevPage = document.getElementsByClassName('prev-page');
    const nextPage = document.getElementsByClassName('next-page');

    return {
      exportCSVRadio,
      exportDataPacketRadio,
      openExportFormBtn,
      exportResultsBtn,
      exportAllRadio,
      exportMarkedRadio,
      prevPage,
      nextPage,
      openExportForm,
    };
  }

  function registerShowResultCheckboxEventHandlers() {
    const {
      exportCSVRadio,
      exportDataPacketRadio,
      openExportFormBtn,
      exportResultsBtn,
      exportAllRadio,
      exportMarkedRadio,
      prevPage,
      nextPage,
      openExportForm
    } = extractedExportFormElements();
    const exportResults = document.querySelectorAll('.export-results__result');
    const prevPageSet = new Set(prevPage);
    const nextPageSet = new Set(nextPage);
    const paginationLinks = new Set([...prevPageSet, ...nextPageSet]);
    
    if (exportCSVRadio) {
      // eslint-disable-next-line func-names
      exportCSVRadio.addEventListener('click', function () {
        setWindowUrlQuery(exportCSVRadio.checked, 'export-type', 'csv');
        setTagRefQuery(paginationLinks, true, 'export-type', 'csv');
        setFormActionUrl(openExportForm, 'export-type', 'csv');
        setDownloadUrl(exportResultsBtn, 'export-type', 'csv');
        if (exportAllRadio.checked) {
          setLightBoxShow(openExportForm, exportDataPacketRadio.checked, exportCSVRadio.checked);
        }
      });
    }

    if (exportDataPacketRadio) {
      // eslint-disable-next-line func-names
      exportDataPacketRadio.addEventListener('click', function () {
        setWindowUrlQuery(exportDataPacketRadio.checked, 'export-type', 'data-packet');
        setTagRefQuery(paginationLinks, true, 'export-type', 'data-packet');
        setFormActionUrl(openExportForm, 'export-type', 'data-packet');
        setDownloadUrl(exportResultsBtn, 'export-type', 'data-packet');
        if (exportAllRadio.checked) {
          setLightBoxShow(openExportForm, exportDataPacketRadio.checked, exportCSVRadio.checked);
        }
      });
    }

    if (openExportFormBtn) {
      // eslint-disable-next-line func-names
      openExportFormBtn.addEventListener('click', function (event) {
        const url = new URL(window.location.href);
        const exportType = url.searchParams.get('export-type');
        const exportSelection = url.searchParams.get('export-selection');
        if (!exportType && !exportSelection) {
          exportCSVRadio.click();
          exportAllRadio.click();
        }
        const isSelected = event.target.matches('.collapsed');
        setWindowUrlQuery(isSelected, 'export-form');
        setTagRefQuery(paginationLinks, isSelected, 'export-form');
      });
    }

    if (exportMarkedRadio) {
      // eslint-disable-next-line func-names
      exportMarkedRadio.addEventListener('click', function () {
        checkExportBtnShouldDisable();
        setWindowUrlQuery(exportMarkedRadio.checked, 'export-selection', 'marked');
        setTagRefQuery(paginationLinks, true, 'export-selection', 'marked');
        setDownloadUrl(exportResultsBtn, 'export-selection', 'marked');
        setListenerSubmitButton();
        if (exportMarkedRadio.checked) {
          toggleExportResults(exportResults, false);
        }
      });
    }

    if (exportAllRadio) {
      // eslint-disable-next-line func-names
      exportAllRadio.addEventListener('click', function () {
        toggleDisableExportButton(false);
        setWindowUrlQuery(exportAllRadio.checked, 'export-selection', 'all');
        setTagRefQuery(paginationLinks, true, 'export-selection', 'all');
        setDownloadUrl(exportResultsBtn, 'export-selection', 'all');
        if (exportAllRadio.checked) {
          setLightBoxShow(openExportForm, exportDataPacketRadio.checked, exportCSVRadio.checked);
        }
        if (exportAllRadio.checked) {
          toggleExportResults(exportResults, true);
        }
      });
    }
    
  }

  function setExportFormOptions() {
    const url = new URL(window.location.href);
    const formShow = url.searchParams.get('export-form');
    const exportType = url.searchParams.get('export-type');
    const exportSelection = url.searchParams.get('export-selection');

    const {
      exportCSVRadio,
      exportDataPacketRadio,
      openExportFormBtn,
      exportAllRadio,
      exportMarkedRadio
    } = extractedExportFormElements();

    if (!formShow && !exportType && !exportSelection) {
      window.Cookies.remove('vufind_cart');
    }

    if (formShow) {
      openExportFormBtn.click();
    }
    if (exportType === 'csv') {
      exportCSVRadio.click();
    } else if (exportType === 'data-packet') {
      exportDataPacketRadio.click();
    }
    if (exportSelection === 'all') {
      exportAllRadio.click();
    } else if (exportSelection === 'marked') {
      exportMarkedRadio.click();
    }
  }

  registerShowResultCheckboxEventHandlers();
  setExportFormOptions();
});