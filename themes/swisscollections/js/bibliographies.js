/*global VuFind */

/*exported initBibliographiesTree */
function buildBibliographiesNodes (data) {
  var json = [];

  $(data).each(function bibliographiesNodesEach () {
    var url = this.href;
    var $html = $('<div/>');
    var $item = $('<span/>')
      .addClass('main text' + (this.isApplied ? ' applied' : ''))
      .attr('role', 'menuitem')
      .attr('title', this.displayText)
      .text(this.displayText);
    $html.append($item);

    var children = null;
    if (typeof this.children !== 'undefined' && this.children.length > 0) {
      children = buildBibliographiesNodes(this.children);
    } else {
      children = [];
    }
    json.push({
      'text': $html.html(),
      'children': children,
      'applied': this.isApplied,
      'state': {
        'opened': this.hasAppliedChildren
      },
      'li_attr': this.isApplied ? { 'class': 'active' } : {},
      'a_attr': {
        'href': VuFind.path + '/Search/Results' + unescapeXml(url)
      },
      'data': {
        'value': this.value,
      }
    });
  });

  return json;
}

function buildBibliographiesTree (treeNode, bibliographiesData) {
  // Enable keyboard navigation also when a screen reader is active
  // treeNode.bind('select_node.jstree', VuFind.bibliographies.showLoadingOverlay);

  var results = buildBibliographiesNodes(bibliographiesData);
  treeNode.find('.fa-spinner').parent().remove();
  treeNode.jstree({
    'core': {
      'data': results,
      'check_callback': true,
    },
    "state" : { "key" : "bibliographies" },
    "plugins" : [ "state", "sort" ],
    "sort": sortTree
  });
}

function initBibliographiesTree (treeNode) {
  var loaded = treeNode.data('loaded');
  if (loaded) {
    return;
  }
  treeNode.data('loaded', true);

  var source = treeNode.data('source');
  var bibliographies = treeNode.data('bibliographies');
  var operator = treeNode.data('operator');
  var sort = treeNode.data('sort');
  var query = window.location.href.split('?')[1];
  query = query ? '?' + query : '';

  treeNode.prepend('<div><i class="fa fa-spinner fa-spin" aria-hidden="true"></i><div>');

  $.getJSON(VuFind.path + '/AJAX/JSON' + query,
    {
      method: "getBibliographiesData",
      source: source,
      facetName: bibliographies,
      facetSort: sort,
      facetOperator: operator
    },
    function getBibliographiesData (response/*, textStatus*/) {
      buildBibliographiesTree(treeNode, response.data.bibliographies);
      initSelectionBehaviour();
    }
  );
}