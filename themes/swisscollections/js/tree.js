// monkey patch jstree to use session storage
$.vakata.storage = {
  set: function (key, val) { return window.sessionStorage.setItem(key, val);},
  get: function (key) { return window.sessionStorage.getItem(key);},
  del: function (key) { return window.sessionStorage.removeItem(key);}
};

function unescapeXml (xml) {
  if (xml === undefined) {
    return xml;
  }
  return xml.replace(/&apos;/gi, '\'')
    .replace(/&gt;/gi, '>')
    .replace(/&lt;/gi, '<')
    .replace(/&quot;/gi, '"')
    .replace(/&amp;/gi, '&');
}
function initSelectionBehaviour() {
  $(".jstree").on("select_node.jstree deselect_node.jstree", function openLink(e, data) {
    if (data.event) {
      if(data.event.ctrlKey )
      {
        var tree = $(".jstree").jstree();
        data.selected.filter(id => id!==data.node.id).forEach(n => tree.deselect_node(n));
        window.open(data.node.a_attr.href, '_blank');
      }
      else {
        window.open(data.node.a_attr.href, '_self');
      }
    }
  });
}
function sortTree (a, b) {
  var textA = this._model.data[a].text;
  var textB = this._model.data[b].text;
  if (typeof textA === 'string' && typeof textB === 'string') {
    return textA.localeCompare(textB, 'de');
  }
  return 0;
}