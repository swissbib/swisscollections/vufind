/*global VuFind */

function buildTopicsTectonicsNodes (data) {
  var json = [];

  $(data).each(function topicsTectonicsNodesEach () {
    var children;
    var url = this.href;
    if (typeof this.children !== 'undefined' && this.children.length > 0) {
      children = buildTopicsTectonicsNodes(this.children);
    } else if (this.displayText.endsWith("_")) {
      children = false;
      this.displayText = this.displayText.substring(0, this.displayText.length - 1);
      // this.value = this.value.substring(0, this.value.length - 2) + "\/";
    } else {
      children = true;
      if (this.count == 1) {
        url += '&jumpto=1';
      }
    }
    var $html = $('<div/>');
    var $item = $('<span/>')
      .addClass('main text' + (this.isApplied ? ' applied' : ''))
      .attr('role', 'menuitem')
      .attr('title', this.displayText)
      .text(this.displayText);
    $html.append($item);

    json.push({
      'text': $html.html(),
      'children': children,
      'applied': this.isApplied,
      'state': {
        'opened': this.hasAppliedChildren
      },
      'li_attr': this.isApplied ? { 'class': 'active' } : {},
      'a_attr': {
        'href': VuFind.path + '/Search/Results' + unescapeXml(url)
      },
      'data': {
        'value': this.value,
      }
    });
  });

  return json;
}

function buildTopicsTectonicsTree (treeNode, topicsTectonicsData) {

  var results = buildTopicsTectonicsNodes(topicsTectonicsData);
  treeNode.find('.fa-spinner').parent().remove();
  treeNode.jstree({
    'core': {
      'data': results,
      'check_callback': true
    },
    'state' : { "key" : "tectonics" },
    'plugins' : [ "state"],
  });
}

function initTopicsTectonicsTree (treeNode) {
  var loaded = treeNode.data('loaded');
  if (loaded) {
    return;
  }
  treeNode.data('loaded', true);

  var source = treeNode.data('source');
  var topicsTectonics = treeNode.data('topicstectonics');
  var operator = treeNode.data('operator');
  var sort = treeNode.data('sort');
  var query = window.location.href.split('?')[1];
  query = query ? '?' + query : '';

  treeNode.prepend('<div><i class="fa fa-spinner fa-spin" aria-hidden="true"></i><div>');

  $.getJSON(VuFind.path + '/AJAX/JSON' + query,
    {
      method: "getTopicsTectonicsData",
      source: source,
      facetName: topicsTectonics,
      facetSort: sort,
      facetOperator: operator
    },
    function getTopicsTectonicsData (response/*, textStatus*/) {
      buildTopicsTectonicsTree(treeNode, response.data.topicsTectonics);
      $(".jstree").jstree().sort = () => { return void 0; };
      $(".jstree").jstree().settings.core.data.url = function getJson(node) {
        return "/Hierarchy/GetTopicsTectonicsJSON?id=" + encodeURIComponent(node.data.value);
      };
      initSelectionBehaviour();
    }
  );
}
