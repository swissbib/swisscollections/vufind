import Configuration from "./Configuration";
import Item from "./Item";
import SectionResult from "./SectionResult";
import Section from "./Section";

/**
 * Utility class that converts the results received from some search backend into VuFindAutoCompleteItem objects.
 */
export default class SearchResultConverter {

    /**
     * @private
     */
    private static RESULT_INDEX: number = 0;

    /**
     * Converts the given results as they have been received from the search backend into an array of structured items
     * as expected by the underlying auto-suggest interface.
     *
     * @param {Configuration} configuration
     * @param section
     * @param {SearchResult} result
     * @returns {SectionResult}
     */
    public convert(configuration: Configuration, section: Section, result: SearchResult): SectionResult {

        const searchTerm = this.removeBracketsTrim(section.query)
        const regex = this.regexEscape(searchTerm)

        const sectionResult: SectionResult = { items: [], total: 0 };
        const data: SearchResultData = this.getResult(result);
        sectionResult.total = data.total;

        for (let index: number = 0; index < data.suggestions.length; ++index) {

            const removedBrackets = this.removeBracketsTrim(data.suggestions[index])
            let highlightText = removedBrackets.replace(regex, '<b>$1</b>')

            const item: Item = {
                text: highlightText,
                value: data.suggestions[index],
            };
            item.href = configuration.getRecordLink(data.suggestions[index], section)

            sectionResult.items[index] = item;
        }
        return sectionResult;
    }

    private getResult(result: SearchResult): SearchResultData {
        return result.data;
    }

    // remove <<..>> and trim
    private removeBracketsTrim(content: string): string {
        return content.replace(/>>/g, ' ').replace(/<</g, ' ').replace(/^\s+/g, "").replace(/\s+$/g, "");
    }

    // escape term for regex - https://github.com/sindresorhus/escape-string-regexp/blob/master/index.js
    private regexEscape(searchTerm: string): RegExp {
        const escapedTerm = searchTerm.replace(/[|\\{}()\[\]\^$+*?.]/g, '\\$&')
        return new RegExp('(' + escapedTerm + ')', 'ig')
    }
}

declare interface SearchResultData { total: number; suggestions: string[]; }

/**
 * Type definition for search results received for the per-section auto-suggest.
 */
export interface SearchResult {

    /**
     * The data received.
     */
    data: SearchResultData;

    /**
     * The response status.
     */
    status: string;
}