
import TemplateBase from "../common/Templates";

/**
 * Internal class to keep template snippets in one place.
 */
export default class Templates extends TemplateBase {

    /**
     * Generates
     * @param {SectionHeader} args
     * @returns {string}
     */
    public sectionHeader(args: SectionHeader): string {
        return `<hr class="ac-section-divider"><header class="ac-section-header"><span class="section-label">${args.label}</span></header>`;
    }
}

/**
 * Internal type declaration for the Templates.sectionHeader() method.
 */
interface SectionHeader {

    label: string;
}
