<?php $this->headTitle($this->translate('page.footer.search'));?>

<div class="staticContent">
    <div class="row row-no-gutters">
        <div class="col-md-5 col-sm-4">
        </div>
        <div class="col-md-7 col-sm-8 left-md-search-results-gutter">
            <h1><?= $this->transEsc('page.footer.search') ?></h1>
            <h2>Guide to searching the portal</h2>

            <p>The <i>History of Education Switzerland</i> portal allows users to conduct <i>simple</i> searches using individual keywords or <i>advanced</i> searches using various linked search terms across all or selected collections. Searches can be refined using <i>facets</i> or <i>search operators</i>. Based on a search result, <i>search entries</i> enable a more detailed search for relevant terms.</p>

            <h3>Simple search</h3>
            <p>The simplest way to use the portal is to enter one or more search terms in the search field, which will trigger a search across all the collections.</p>

            <p>If you enter several search terms, the default search is for documents that contain all the search terms entered. By using search operators such as AND, OR and NOT, the linking of search terms can be flexibly adapted (further information can be found in the section below).</p>

            <h3>Advanced search</h3>
            <p>In the advanced search, you have the option of limiting the search to specific metadata areas.</p>

            <p>The following options are available:</p>
            <ul class="list">
                <li>Search only in the <b>full text</b></li>
                <li>Search in <b>all fields (without full text)</b></li>
                <li>Search in the <b>title</b></li>
                <li>Search in the <b>title of work</b></li>
                <li>Search by <b>publishing place</b></li>
                <li>Search by <b>subject</b></li>
                <li>Search by <b>author/contributor</b></li>
                <li>Search by <b>identifier</b> (e.g. DOI, call number, document ID, etc.)</li></ul>

            <p>It is possible to combine several search groups (see the help section of the <a class="link-grey" href="<?php echo $this->url('search-advanced'); ?>">advanced search</a>). Optionally, the search can be further narrowed down by language, text type, collection and time period. Search operators (see below) can also be used in the advanced search.</p>

            <h3>Search entries</h3>
            <p>Shown on the detailed view of a search result, search entries offer relevant contextual references. They allow you to search further in the entire collection using thematically or collection-specific relevant terms.</p>

            <h2>Narrowing down the search results</h2>

            <h3>Facets</h3>
            <p>Facets allow you to refine your search query and serve as a navigation aid through the entire collection if your search is empty.</p>

            <p>The following facets are available:</p>
            <ul class="list">
                <li><b>Collection</b>: Depicts the internal structure of a collection according to the source.</li>
                <li><b>Text type</b>: Identifies the type or genre of a document.</li>
                <li><b>Medium</b>: Describes the format or physical representation of a document.</li>
                <li><b>Place</b>: Thematic reference point of a document (not identical with the place of publication).</li>
                <li><b>Language</b>: Particularly relevant for text sources. Certain sources, such as photographs or children's drawings, have not been assigned a language.</li>
                <li><b>Year</b>: Depending on the source, this is usually the year of origin or publication, but occasionally also a thematic reference year or the date of digitisation.</li></ul>

            <p>We aim to link the holdings included in <i>History of Education Switzerland</i> with these facets as comprehensively and logically as possible. However, certain restrictions may apply depending on the holdings and the available metadata. Each document is clearly linked to the facets <i>Collection</i>, <i>Text type</i> and <i>Medium</i>. In the case of the facets <i>Place</i> and <i>Language</i>, on the other hand, both multiple links and missing links are possible.</p>

            <p>The place facet was designed with thematic considerations in mind, which is why the place of publication or publication was not taken into account for text sources. The language facet, on the other hand, is primarily aimed at text sources. A few text holdings that cannot be clearly categorised (e.g. bilingual journals) are not linked to this facet. Similarly, certain sources, such as photographs or children's drawings, are not linked to it; thus, narrowing the search with the language facet excludes these types of documents from the search.</p>

            <p>As a rule, all documents are assigned to the facet <i>Year</i>. However, an objectively ‘correct’ assignment is not possible in every case, especially with ambiguous or (partly due to the source) incomplete metadata. Missing or vague information (e.g. ‘18uu’) is taken from the source unchanged, unless more precise information is available.</p>

            <h3>Search operators</h3>
            <p>Search operators can be used in both the simple and advanced search to allow for more flexible use of search terms.</p>

            <p>A selection of the most important search operators:</p>
            <ul class="list">
                <li><b>Phrase search with quotation marks</b>: e.g. <i><b>"</b>Höhere Töchterschule Zürich<b>"</b></i><br>
                Searches for the exact word order of the terms in double quotation marks.</li>
                <li><b>Question mark as a placeholder for any character</b>: e.g. <i>kindergar<b>?</b>en</i><br>
                The question mark can be used as a placeholder for any single character. In the example, documents for the terms “Kindergarten” and “Kindergarden” are found, but also for “Kindergarken”, for example.</li>
                <li><b>Asterisk as a placeholder for zero or more characters</b>: e.g. <i>kinderg<b>*</b></i><br>
                The asterisk is a placeholder for any number of characters. In the example, the following terms are included: Kindergarten, Kindergärten, Kindergärtnerinnenseminar, Kindergeburtstag, etc.</li>
                <li><b>Search for similar terms</b>: e.g. <i>Berufslehre<b>~</b></i><br>
                Searches for similarly spelled words (e.g. “Berufsleben”, “Berufsleute”, etc.).</li>
                <li><b>Area search</b>: e.g. <i><b>"</b>Mathematik Prüfung<b>"~5</b></i><br>
                Searches for terms in quotation marks that are a certain distance apart. In the example, the words “exam” and “mathematics” are separated by a maximum of five words.</li>
                <li><b>Weighting</b>: e.g. <i>Höhere Töchterschule<b>^5</b> Zürich</i><br>
                In this example, the term “Töchterschule” is given a higher weighting when calculating the relevance of the search results.</li>
                <li><b>Boolean operators (AND, +, NOT, -, OR)</b>: <i>e.g. Rousseau <b>-</b>Jean-Jacques <b>-</b>J.-J. <b>-</b>J.</i><br>
                Searches for documents on “Rousseau” that do not contain “Jean-Jacques” or “J.-J.” or “J. J.”. Consequently, works by “Joh. Jakob Rousseau” or “H. Rousseau” will be found, as well as documents related to the famous philosopher in which his first name or initials are not mentioned. Links are possible with <b>AND</b> or <b>+</b>, <b>NOT</b> or <b>-</b> and <b>OR</b>. The search query “Rousseau NOT Jean-Jacques NOT J.-J. NOT J.” thus leads to the same result as the example above.</li></ul>

            <p>Further information on the search operators can be found in the help section of the <a class="link-grey" href="<?php echo $this->url('search-advanced'); ?>">advanced search</a>.</p>

            <h2>Full-text recognition</h2>

            <p><i>History of Education Switzerland</i> does not use its own OCR full-text recognition, but relies on the full texts provided by the respective data sources. The fact that the search for “Kindergar?” yields more results than the search for “Kindergarten OR Kindergarden” indicates that in some cases the OCR also recognises other words, such as “Kindergarken”. This is often due to the limitations of current text recognition technology, especially for source texts with special fonts, layouts or non-optimal source material.</p>

        </div>
    </div>
</div>
