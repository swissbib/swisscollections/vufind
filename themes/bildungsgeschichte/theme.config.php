<?php

use BildungsGeschichte\View\Helper\DigitalisatUrl;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'extends' => 'swisscollections',
    'less' => [
        'active' => false,
        'compiled.less'
    ],
    'helpers' => [
        'factories' => [
            'BildungsGeschichte\View\Helper\LayoutClass' => 'Laminas\ServiceManager\Factory\InvokableFactory',
            DigitalisatUrl::class => InvokableFactory::class,
            ],
        'aliases' => [
            'layoutClass' => 'BildungsGeschichte\View\Helper\LayoutClass',
            'digitalisatUrl' => DigitalisatUrl::class,
        ]
    ]
];
