#!/bin/bash
#
# sync with libadmin and clear cache
BASEDIR=$(dirname $0)
INDEX="$BASEDIR/../public/index.php"
if [ -z "$VUFIND_LOCAL_DIR" ]; # if $VUFIND_LOCAL_DIR empty or unset, use default localdir
   then export VUFIND_LOCAL_DIR=${BASEDIR}/../local;
fi

export VUFIND_CACHE=$VUFIND_LOCAL_DIR/cache
export VUFIND_LOCAL_MODULES=Swissbib,SwissCollections

php $INDEX libadmin/sync $@

php $INDEX libadmin/syncGeoJson $@
#symbolic link so that geojson.json is reachable for libraries_map.js
ln -sf ../data/cache/geojson.json $BASEDIR/../public/geojson.json

#please do not delete a directory with options -rf as root based on a relative directory! GH
echo "Trying to remove local cache"
# no removal of hierarchy cache
rm -rf $VUFIND_CACHE/searchspecs/*
rm -rf $VUFIND_CACHE/objects/*
rm -rf $VUFIND_CACHE/languages/*
